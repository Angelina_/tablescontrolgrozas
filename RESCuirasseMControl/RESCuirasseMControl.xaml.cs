﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace RESCuirasseMControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRESCuirasseM : UserControl
    {
        public event EventHandler<RESCuirasseMModel> OnDeleteRecord = (object sender, RESCuirasseMModel data) => { };
        public event EventHandler OnClearRecords;
        public event EventHandler<RESCuirasseMModel> OnDoubleClick = (object sender, RESCuirasseMModel data) => { };

        public UserControlRESCuirasseM()
        {
            InitializeComponent();

            DgvRESCuirasseM.DataContext = new GlobalRESCuirasseM();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    RESCuirasseMModel resCuirasseMModel = new RESCuirasseMModel
                    {
                        Id = ((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, resCuirasseMModel);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, null);
            }
            catch { }
        }

        private void DgvRESCuirasseM_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvRESCuirasseM_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem == null) return;

            if (((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem).Id > -1)
            {
                if (((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem).Id != PropSelectedIdRESCuirasseM.IdRESCuirasseM)
                {
                    PropSelectedIdRESCuirasseM.IdRESCuirasseM = ((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem).Id;
                }
            }
            else
            {
                PropSelectedIdRESCuirasseM.IdRESCuirasseM = 0;
            }
        }

        private void DgvRESCuirasseM_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if ((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Двойной клик по строке
                    OnDoubleClick(this, (RESCuirasseMModel)DgvRESCuirasseM.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
