﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RESCuirasseMControl
{
    public partial class UserControlRESCuirasseM : UserControl
    {
        #region RESCuirasseM
        private List<RESCuirasseMModel> listRESCuirasseMModel = new List<RESCuirasseMModel> { };
        public List<RESCuirasseMModel> ListRESCuirasseMModel
        {
            get { return listRESCuirasseMModel; }
            set
            {
                if (listRESCuirasseMModel != null && listRESCuirasseMModel.Equals(value)) return;
                listRESCuirasseMModel = value;
                UpdateRESCuirasseM();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateRESCuirasseM()
        {
            try
            {
                if (ListRESCuirasseMModel == null)
                    return;

                ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.Clear();

                for (int i = 0; i < ListRESCuirasseMModel.Count; i++)
                {
                    ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.Add(ListRESCuirasseMModel[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.ToList().FindIndex(x => x.Id == PropSelectedIdRESCuirasseM.IdRESCuirasseM);
                if (ind != -1)
                {
                    DgvRESCuirasseM.SelectedIndex = ind;
                }
                else
                {
                    DgvRESCuirasseM.SelectedIndex = 0;
                }

            }
            catch { }
        }
        #endregion

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRESCuirasseM.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRESCuirasseM.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRESCuirasseM.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.RemoveAt(index);
                    }
                }

                List<RESCuirasseMModel> list = new List<RESCuirasseMModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    RESCuirasseMModel strRESCuirasseM = new RESCuirasseMModel
                    {
                        Id = -2,
                        Id_Groza = -2,
                        Band = -2,
                        Type = 255,
                        Coordinates = new CoordCuirasseM() { Altitude = -2, Latitude = -2, Longitude = -2 }
                    };
                    list.Add(strRESCuirasseM);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRESCuirasseM)DgvRESCuirasseM.DataContext).CollectionRESCuirasseM.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((RESCuirasseMModel)DgvRESCuirasseM.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
