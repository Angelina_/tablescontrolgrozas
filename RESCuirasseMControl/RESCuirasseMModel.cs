﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace RESCuirasseMControl
{
    public class RESCuirasseMModel
    {
        public int Id { get; set; }
        public int Id_Groza { get; set; }
        public double Frequency { get; set; }
        public float Band { get; set; }
        public byte Type { get; set; }
        public CoordCuirasseM Coordinates { get; set; } = new CoordCuirasseM();
    }

    //public class CoordCuirasseM
    //{
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }
    //    public float Altitude { get; set; }
    //}
}
