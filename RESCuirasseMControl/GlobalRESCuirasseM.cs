﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace RESCuirasseMControl
{
    public class GlobalRESCuirasseM : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<RESCuirasseMModel> CollectionRESCuirasseM { get; set; }

        public GlobalRESCuirasseM()
        {
            CollectionRESCuirasseM = new ObservableCollection<RESCuirasseMModel> { };

            #region Test
            //CollectionRESCuirasseM = new ObservableCollection<RESCuirasseMModel>
            //{
            //    new RESCuirasseMModel
            //    {
            //        Id = 1,
            //        Id_Groza = 11,
            //        Frequency = 135.83F,
            //        Band = 25.5F,
            //        Type = 0,
            //        Coordinates = new CoordCuirasseM() { Altitude = 200, Latitude = -53.123445, Longitude = 27.456459 }
            //    },
            //    new RESCuirasseMModel
            //    {
            //        Id = 2,
            //        Id_Groza = 22,
            //        Frequency = 435.83F,
            //        Band = 4.6F,
            //        Type = 4,
            //        Coordinates = new CoordCuirasseM() { Altitude = 400, Latitude = 55.65432444, Longitude = 29.45245245 }
            //    },
            //    new RESCuirasseMModel
            //    {
            //        Id = 3,
            //        Id_Groza = 33,
            //        Frequency = 789.34F,
            //        Band = 78.8F,
            //        Type = 2,
            //        Coordinates = new CoordCuirasseM() { Altitude = 230, Latitude = -57.33455755, Longitude = -30.7854356 }
            //    }
            //};
            #endregion

        }
    }
}
