﻿using GrozaSModelsDBLib;
using System;
using System.ComponentModel;
using System.Windows;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void UcJammerStation_OnIsWindowPropertyOpen(object sender, JammerStationControl.JammerStationProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }
        private void UcFreqRangesRecon_OnIsWindowPropertyOpen(object sender, FreqRangesSControl.FreqRangesSProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }

        private void UcFreqKnown_OnIsWindowPropertyOpen(object sender, FreqRangesSControl.FreqRangesSProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }

        private void UcFreqForbidden_OnIsWindowPropertyOpen(object sender, FreqRangesSControl.FreqRangesSProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }

        private void UcSectorsRecon_OnIsWindowPropertyOpen(object sender, SectorsReconControl.SectorsReconProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }

        private void UcOwnUAV_OnIsWindowPropertyOpen(object sender, OwnUAVControl.OwnUAVProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }
    }
}
