﻿using GrozaSModelsDBLib;
using InheritorsEventArgs;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using TableEvents;
using ClientDataBase;
using System.Collections.Generic;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        private void OnUpTable_TableJammerStation(object sender, TableEventArgs<TableJammerStation> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lJammerStation = e.Table;
                ucJammerStation.UpdateJammerStation(lJammerStation);
            });
        }

        private void UcJammerStation_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id > 0)
            {
                LoadTablesByFilter(e.Id);
            }
        }

        private void UcJammerStation_OnDoubleClickStation(object sender, SelectedRowEvents e)
        {
            int id = e.Id;
        }

        private async void LoadTablesByFilter(int id)
        {
            try
            {
                lFreqKnown = await (clientDB.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(id);
                ucFreqKnown.UpdateFreqRangesS(lFreqKnown);

                lFreqRangesRecon = await (clientDB.Tables[NameTable.TableFreqRangesRecon] as IDependentAsp).LoadByFilterAsync<FreqRanges>(id);
                ucFreqRangesRecon.UpdateFreqRangesS(lFreqRangesRecon);

                lFreqForbidden = await (clientDB.Tables[NameTable.TableFreqForbidden] as IDependentAsp).LoadByFilterAsync<FreqRanges>(id);
                ucFreqForbidden.UpdateFreqRangesS(lFreqForbidden);

                lSectorsRecon = await (clientDB.Tables[NameTable.TableSectorsRecon] as IDependentAsp).LoadByFilterAsync<TableSectorsRecon>(id);
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
        }

        private void OnUpTable_TableFreqRangesRecon(object sender, TableEventArgs<TableFreqRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqRangesRecon = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList();
                ucFreqRangesRecon.UpdateFreqRangesS(lFreqRangesRecon);
            });
        }

        private void OnUpTable_TableFreqKnown(object sender, TableEventArgs<TableFreqKnown> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqKnown = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList();
                ucFreqKnown.UpdateFreqRangesS(lFreqKnown);
            });
        }

        private void OnUpTable_TableFreqForbidden(object sender, TableEventArgs<TableFreqForbidden> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqForbidden = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList();
                ucFreqForbidden.UpdateFreqRangesS(lFreqForbidden);
            });
        }

        private void OnUpTable_TableSectorsRecon(object sender, TableEventArgs<TableSectorsRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSectorsRecon = (from t in e.Table let a = t as TableSectorsRecon select a).Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList();
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
            });
        }

        private void OnUpTable_TableOwnUAV(object sender, TableEventArgs<TableOwnUAV> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lOwnUAV = e.Table;
                ucOwnUAV.UpdateOwnUAV(lOwnUAV);
            });
        }

        private void UcOwnUAV_OnSendRecord(object sender, TableOwnUAV e)
        {
            TableOwnUAV table = e;
           
        }

        private async void LoadTables()
        {
            try
            {
                lJammerStation = await clientDB.Tables[NameTable.TableJammerStation].LoadAsync<TableJammerStation>();
                ucJammerStation.UpdateJammerStation(lJammerStation);

                lSectorsRecon = await clientDB.Tables[NameTable.TableSectorsRecon].LoadAsync<TableSectorsRecon>();
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon.Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList());

                lFreqKnown = await clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<FreqRanges>();
                ucFreqKnown.UpdateFreqRangesS(lFreqKnown.Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList());

                lFreqRangesRecon = await clientDB.Tables[NameTable.TableFreqRangesRecon].LoadAsync<FreqRanges>();
                ucFreqRangesRecon.UpdateFreqRangesS(lFreqRangesRecon.Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList());

                lFreqForbidden = await clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<FreqRanges>();
                ucFreqForbidden.UpdateFreqRangesS(lFreqForbidden.Where(x => x.NumberASP == PropNumJammerStation.OwnNumJammerStation).ToList());

                lOwnUAV = await clientDB.Tables[NameTable.TableOwnUAV].LoadAsync<TableOwnUAV>();
                ucOwnUAV.UpdateOwnUAV(lOwnUAV);

            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
