﻿using System;
using ClientDataBase;
using InheritorsEventArgs;
using GrozaSModelsDBLib;

namespace ProjectTablesControlTEST
{
    using TableEvents;

    public partial class MainWindow
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;

            (clientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable += OnUpTable_TableJammerStation;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += OnUpTable_TableFreqKnown;
            (clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnUpTable_TableFreqRangesRecon;
            (clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += OnUpTable_TableFreqForbidden;
            (clientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += OnUpTable_TableSectorsRecon;
            (clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += OnUpTable_TableOwnUAV;

        }

      
    }
}
