﻿using RadarRodnikControl;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using ValuesCorrectLib;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using GlobusControl;
using TempResControl;
using ValuesConverter;

namespace ProjectTablesControlTEST
{
    using GrozaSModelsDBLib;

    using TableEvents;

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public List<RodnikModel> ListRodnik { get; set; }

        private byte viewCoord = 2;
        public byte ViewCoord
        {
            get { return viewCoord; }
            set
            {
                viewCoord = value;
                OnPropertyChanged();
            }
        }



        public MainWindow()
        {
            InitializeComponent();

            DbConnection_Click(this, null);

            InitTables();
            //SetLanguageTables(basicProperties.Local.General.Language);
            //TranslatorTables.LoadDictionary(basicProperties.Local.General.Language);


            //LoadRodnik();

            //ucGlobus.PeriodicMessage = 1;
            //ucGlobus.WorkingMode = 1;
            Task.Run(() => { GenerateIri(); });
        }

        private async Task GenerateIri()
        {
            var rand = new Random();
            while (true)
            {
                var list = new List<ResModel>();
                for (int i = 1; i < 30; i++)
                {
                    list.Add(new ResModel(i, rand.Next(100, 6000), rand.Next(5, 20), "Unknown", rand.Next(0,360), -1, default, DateTime.Now, false, Led.Green));
                }

                Dispatcher.BeginInvoke(new Action(() => { ucTempRes.UpdateSignalsUAV(list); }));
                
                await Task.Delay(500);
            }
        }

        private void ucRodnik_OnDeleteRecord(object sender, RadarRodnikControl.RodnikModel e)
        {
            int ind = ucRodnik.ListRodnikModel.ToList().FindIndex(x => x.Id == e.Id);
            //ListRodnik.RemoveAt(ind);

            ListRodnik = LoadRodnikTEST();
            ucRodnik.ListRodnikModel = ListRodnik;
        }

        private void ucRodnik_OnClearRecords(object sender, EventArgs e)
        {
            ListRodnik = LoadRodnikTEST();
            ListRodnik.Clear();
            ucRodnik.ListRodnikModel = ListRodnik;
        }

        public void LoadRodnik()
        {
            ucRodnik.ListRodnikModel = new System.Collections.Generic.List<RodnikModel>
            {
                new RodnikModel
                {
                    Id = 1,
                    Azimuth = 35.83F,
                    Range = 125.4F,
                    Class = "AAA",
                    Velocity = 500.3F,
                    Aspect = 24.7F,
                    Time = DateTime.Now,
                    Mode = "BBB",
                    Flag = "CCC"
                },
                new RodnikModel
                {
                    Id = 2,
                    Azimuth = 44.43F,
                    Range = 58.8F,
                    Class = "MMM",
                    Velocity = 456.3F,
                    Aspect = 33.33F,
                    Time = DateTime.Now,
                    Mode = "VVV",
                    Flag = "XXX"
                },
                new RodnikModel
                {
                    Id = 3,
                    Azimuth = 54.43F,
                    Range = 55.8F,
                    Class = "YYY",
                    Velocity = 222.2F,
                    Aspect = 22.23F,
                    Time = DateTime.Now,
                    Mode = "PPP",
                    Flag = "RRR"
                }
            };

        }

        public List<RodnikModel> LoadRodnikTEST()
        {
            ListRodnik = new List<RodnikModel>
            {
                new RodnikModel
                {
                    Id = 3,
                    Azimuth = 54.43F,
                    Range = 55.8F,
                    Class = "YYY",
                    Velocity = 222.2F,
                    Aspect = 22.23F,
                    Time = DateTime.Now,
                    Mode = "PPP",
                    Flag = "RRR"
                }
            };

            return ListRodnik;
        }

       
        private void ucZorkiR_OnClearRecords(object sender, EventArgs e)
        {

        }

        private void ucZorkiR_OnDeleteRecord(object sender, RadarZorkiRControl.ZorkiRModel e)
        {

        }

        private void ucOwnUAV_OnSelectedRow(object sender, TableEvents.SelectedRowEvents e)
        {

        }

        private void ucGlobus_OnDeleteRecord(object sender, GlobusControl.GlobusModel e)
        {

        }

        private void ucGlobus_OnClearRecords(object sender, EventArgs e)
        {

        }

        private void ucGlobus_OnRequestCurrentMode(object sender, EventArgs e)
        {
            
            ucGlobus.WorkingMode = 0;
        }

        private void ucGlobus_OnActivate(object sender, EventArgs e)
        {
            ucGlobus.PeriodicMessage = 1;

            List<GlobusModel> listGlobusModel = new List<GlobusModel>();
            ucGlobus.ListGlobusModel = listGlobusModel;
           
        }

        private void ucGlobus_OnDeactivate(object sender, EventArgs e)
        {
            ucGlobus.WorkingMode = 1;
        }

        private void ucGlobus_OnDoubleClick(object sender, GlobusModel e)
        {
            ucGlobus.PeriodicMessage = 0;
        }

        private void UcOperatorGun_OnOnAddRecord(object sender, TableEvent e)
        {
           

            List<TableOperatorGun> listOperatorGun = new List<TableOperatorGun>();
            listOperatorGun.Add(e.Record as TableOperatorGun);
            ucOperatorGun.ListOperatorGun = listOperatorGun;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            PropViewCoords.ViewCoords = 3;


            this.ucOperatorGun.UpdateCoordinates(1, new Coord(-34.88787879, 34.6577896678, -1), 250);
        }
    }
}
