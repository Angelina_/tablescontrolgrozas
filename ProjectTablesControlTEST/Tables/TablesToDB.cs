﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        // Станции помех
        public List<TableJammerStation> lJammerStation = new List<TableJammerStation>();
        // Известные частоты (ИЧ)
        public List<FreqRanges> lFreqKnown = new List<FreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<FreqRanges> lFreqRangesRecon = new List<FreqRanges>();
        // Запрещенные частоты (ЗЧ)
        public List<FreqRanges> lFreqForbidden = new List<FreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsRecon> lSectorsRecon = new List<TableSectorsRecon>();
        // Свои БПЛА 
        public List<TableOwnUAV> lOwnUAV = new List<TableOwnUAV>();
      

        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Add(e.Record);
            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (clientDB != null)
            {
                clientDB.Tables[nameTable].Clear();
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }
    }
}
