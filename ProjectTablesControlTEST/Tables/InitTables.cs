﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        public void InitTables()
        {
            // Таблица Станции помех
            ucJammerStation.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucJammerStation.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucJammerStation.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucJammerStation.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucJammerStation.OnSelectedRow += new EventHandler<SelectedRowEvents>(UcJammerStation_OnSelectedRow);
            ucJammerStation.OnDoubleClickStation += new EventHandler<SelectedRowEvents>(UcJammerStation_OnDoubleClickStation);
            ucJammerStation.OnIsWindowPropertyOpen += new EventHandler<JammerStationControl.JammerStationProperty>(UcJammerStation_OnIsWindowPropertyOpen);

            // Таблица Известные частоты (ИЧ)
            ucFreqKnown.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqKnown.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqKnown.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqKnown.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqKnown.OnIsWindowPropertyOpen += new EventHandler<FreqRangesSControl.FreqRangesSProperty>(UcFreqKnown_OnIsWindowPropertyOpen);

            // Таблица Диапазоны радиоразведки (ДРР)
            ucFreqRangesRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqRangesRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqRangesRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqRangesRecon.OnIsWindowPropertyOpen += new EventHandler<FreqRangesSControl.FreqRangesSProperty>(UcFreqRangesRecon_OnIsWindowPropertyOpen);

            // Таблица Запрещенные частоты (ЗЧ)
            ucFreqForbidden.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqForbidden.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqForbidden.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqForbidden.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqForbidden.OnIsWindowPropertyOpen += new EventHandler<FreqRangesSControl.FreqRangesSProperty>(UcFreqForbidden_OnIsWindowPropertyOpen);

            // Таблица Сектора радиоразведки (СРР)
            ucSectorsRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSectorsRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSectorsRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucSectorsRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSectorsRecon.OnIsWindowPropertyOpen += new EventHandler<SectorsReconControl.SectorsReconProperty>(UcSectorsRecon_OnIsWindowPropertyOpen);

            // Таблица Свои БПЛА 
            ucOwnUAV.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucOwnUAV.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucOwnUAV.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucOwnUAV.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucOwnUAV.OnIsWindowPropertyOpen += new EventHandler<OwnUAVControl.OwnUAVProperty>(UcOwnUAV_OnIsWindowPropertyOpen);
            ucOwnUAV.OnSendRecord += new EventHandler<TableOwnUAV>(UcOwnUAV_OnSendRecord);
        }

       
    }
}
