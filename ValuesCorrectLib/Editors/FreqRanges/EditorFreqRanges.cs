﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class FreqRangesFminEditor : PropertyEditor
    {
        public FreqRangesFminEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqRangesSControl;component/PGEditors/PropertyGridEditorFreqRanges.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqRangesFminEditorKey"];
        }
    }

    public class FreqRangesFmaxEditor : PropertyEditor
    {
        public FreqRangesFmaxEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqRangesSControl;component/PGEditors/PropertyGridEditorFreqRanges.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqRangesFmaxEditorKey"];
        }
    }

    public class FreqRangesNoteEditor : PropertyEditor
    {
        public FreqRangesNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqRangesSControl;component/PGEditors/PropertyGridEditorFreqRanges.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqRangesNoteEditorKey"];
        }
    }
}
