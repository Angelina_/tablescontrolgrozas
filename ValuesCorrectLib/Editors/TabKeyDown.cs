﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ValuesCorrectLib
{
    public static class TabKeyDown
    {
        public static DependencyProperty CheckTabProperty = DependencyProperty.RegisterAttached("ChekTab", typeof(bool), typeof(TabKeyDown), new FrameworkPropertyMetadata(false, CheckTabChanged));


        private static void CheckTabChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as Control;
            if (control == null) return;

            var wasBound = (bool)e.OldValue;
            var needToBind = (bool)e.NewValue;

            if (wasBound)
            {
                control.PreviewKeyDown -= PreviewKeyDown;
            }

            if (needToBind)
            {
                control.PreviewKeyDown += PreviewKeyDown;
            }

        }

        private static void PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var control = sender as Control;
                var parent = control.Parent;
                if (parent is Grid)
                {
                    foreach (var child in (parent as Grid).Children)
                    {
                        if (child.Equals(sender)) continue;

                        if (!(child is TextBox)) continue;
                        if ((child as TextBox).IsEnabled && (child as TextBox).Focusable && !(child as TextBox).IsReadOnly)
                        {
                            (child as UIElement).Focus();
                            e.Handled = true;
                            return;
                        }
                    }
                }
                e.Handled = true;
            }
        }

        public static void SetCheckTab(Control target, bool value)
        {
            target.SetValue(CheckTabProperty, value);
        }

        public static bool GetCheckTab(Control target)
        {
            return (bool)target.GetValue(CheckTabProperty);
        }
    }
}
