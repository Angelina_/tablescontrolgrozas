﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class ReceivingPointCoordinatesEditor : PropertyEditor
    {
        public ReceivingPointCoordinatesEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ReceivingPointControl;component/Themes/PropertyGridEditorReceivingPoint.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ReceivingPointCoordinatesEditorKey"];
        }
    }

    public class ReceivingPointAntennaEditor : PropertyEditor
    {
        public ReceivingPointAntennaEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ReceivingPointControl;component/Themes/PropertyGridEditorReceivingPoint.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ReceivingPointAntennaEditorKey"];
        }
    }

    public class ReceivingPointNoteEditor : PropertyEditor
    {
        public ReceivingPointNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ReceivingPointControl;component/Themes/PropertyGridEditorReceivingPoint.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ReceivingPointNoteEditorKey"];
        }
    }
}
