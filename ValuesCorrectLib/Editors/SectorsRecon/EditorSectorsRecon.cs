﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class SectorsReconQminEditor : PropertyEditor
    {
        public SectorsReconQminEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SectorsReconControl;component/PGEditors/PropertyGridEditorSectorsRecon.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["SectorsReconQminEditorKey"];
        }
    }

    public class SectorsReconQmaxEditor : PropertyEditor
    {
        public SectorsReconQmaxEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SectorsReconControl;component/PGEditors/PropertyGridEditorSectorsRecon.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["SectorsReconQmaxEditorKey"];
        }
    }

    public class SectorsReconNoteEditor : PropertyEditor
    {
        public SectorsReconNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SectorsReconControl;component/PGEditors/PropertyGridEditorSectorsRecon.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["SectorsReconNoteEditorKey"];
        }
    }
}
