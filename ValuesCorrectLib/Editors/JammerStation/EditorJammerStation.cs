﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    #region Задание координат в градусах (DD)
    public class CoordinatesDDJammerStationEditor : PropertyEditor
    {
        public CoordinatesDDJammerStationEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/JammerStationControl;component/PGEditors/PropertyGridEditorJammerStationDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDJammerStationEditorKey"];
        }
    }

    public class RoleDDJammerStationEditor : PropertyEditor
    {
        public RoleDDJammerStationEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/JammerStationControl;component/PGEditors/PropertyGridEditorJammerStationDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RoleDDJammerStationEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMM)
    public class CoordinatesDDMMJammerStationEditor : PropertyEditor
    {
        public CoordinatesDDMMJammerStationEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/JammerStationControl;component/PGEditors/PropertyGridEditorJammerStationDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMJammerStationEditorKey"];
        }
    }

    public class RoleDDMMJammerStationEditor : PropertyEditor
    {
        public RoleDDMMJammerStationEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/JammerStationControl;component/PGEditors/PropertyGridEditorJammerStationDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RoleDDMMJammerStationEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMMSS)
    public class CoordinatesDDMMSSJammerStationEditor : PropertyEditor
    {
        public CoordinatesDDMMSSJammerStationEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/JammerStationControl;component/PGEditors/PropertyGridEditorJammerStationDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMSSJammerStationEditorKey"];
        }
    }

    public class RoleDDMMSSJammerStationEditor : PropertyEditor
    {
        public RoleDDMMSSJammerStationEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/JammerStationControl;component/PGEditors/PropertyGridEditorJammerStationDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RoleDDMMSSJammerStationEditorKey"];
        }
    }
    #endregion
}
