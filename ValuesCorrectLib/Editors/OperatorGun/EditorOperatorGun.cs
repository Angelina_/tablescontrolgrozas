﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    

    public class TypeConnectGunEditor : PropertyEditor
    {
        public TypeConnectGunEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
                               {
                                   Source = new Uri("/OperatorGunControl;component/PGEditors/PropertyGridEditorOperatorGun.xaml",
                                       UriKind.RelativeOrAbsolute)
                               };
            InlineTemplate = resource["TypeConnectGunEditorKey"];
        }
    }
}
