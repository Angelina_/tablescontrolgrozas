﻿using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using TableEvents;
//using ItemFreqCtrl.Models;

namespace ValuesCorrectLib
{
    public class EditorAddFrequencies
    {
        private static RelayCommand executeCommandAdd;

        public static RelayCommand ExecuteCommandAdd
        {
            get
            {
                return executeCommandAdd ??
                    (executeCommandAdd = new RelayCommand(AddItem, CanExecute));
            }
            set
            {
                if (executeCommandAdd == value) return;
                executeCommandAdd = value;
            }
        }

        private static RelayCommand executeCommandDelete;

        public static RelayCommand ExecuteCommandDelete
        {
            get
            {
                return executeCommandDelete ??
                    (executeCommandDelete = new RelayCommand(DeleteItem, CanExecute));
            }
            set
            {
                if (executeCommandDelete == value) return;
                executeCommandDelete = value;
            }
        }

        public static void AddItem(object e)
        {
            (e as ObservableCollection<TableOwnUAVFreq>).Add(new TableOwnUAVFreq() { FrequencyMHz = 2440.5, BandMHz = 10, Time = DateTime.Now });
            return;
        }

        public static void DeleteItem(object e)
        {
            if(PropFrequencies.SelectedIndFrequencies > -1)
                (e as ObservableCollection<TableOwnUAVFreq>).RemoveAt(PropFrequencies.SelectedIndFrequencies);
            
            return;
        }

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}
