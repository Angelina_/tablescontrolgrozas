﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class OwnUAVSerialNumberEditor : PropertyEditor
    {
        public OwnUAVSerialNumberEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OwnUAVControl;component/PGEditors/PropertyGridEditorOwnUAV.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["OwnUAVSerialNumberEditorKey"];
        }
    }

    public class OwnUAVNameEditor : PropertyEditor
    {
        public OwnUAVNameEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OwnUAVControl;component/PGEditors/PropertyGridEditorOwnUAV.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["OwnUAVNameEditorKey"];
        }
    }

    public class OwnUAVNoteEditor : PropertyEditor
    {
        public OwnUAVNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OwnUAVControl;component/PGEditors/PropertyGridEditorOwnUAV.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["OwnUAVNoteEditorKey"];
        }
    }

    public class OwnUAVFrequenciesEditor : PropertyEditor
    {
        public OwnUAVFrequenciesEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OwnUAVControl;component/PGEditors/PropertyGridEditorOwnUAV.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["OwnUAVFrequenciesEditorKey"];
        }

       
    }
}
