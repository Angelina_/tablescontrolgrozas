﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ValuesCorrectLib
{
    public class CorrectParams
    {
        /// <summary>
        /// Проверка на правильность ввода значений:  
        /// если Fmin меньше 25 или больше 6000, Fmin = 25;
        /// если Fmax меньше 25 или больше 6000, Fmax = 6000;
        /// </summary>
        /// <param name="suppressFHSS">Fmin, Fmax</param>
        public static void IsCorrectMinMax(FreqRanges freqRanges)
        {
            if (freqRanges.FreqMinKHz < 100.0F || freqRanges.FreqMinKHz > 6000.0F) { freqRanges.FreqMinKHz = 100.0F; }
            if (freqRanges.FreqMaxKHz < 100.0F || freqRanges.FreqMaxKHz > 6000.0F) { freqRanges.FreqMaxKHz = 6000.0F; }
        }

        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="iFreqMin"> начальное значение частоты </param>
        /// <param name="iFreqMax"> конечное значение частоты </param>
        /// <returns> true - успешно, false - нет </returns>
        //private static bool IsCorrectFreqMinMax(int iFreqMin, int iFreqMax)
        public static bool IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
        {
            bool bCorrect = true;

            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        /// <summary>
        /// Проверка на правильность ввода значений:  
        /// если Аmin меньше 0 или больше 360, Аmin = 0;
        /// если Аmax меньше 0 или больше 360, Аmax = 360
        /// </summary>
        /// <param name="sr">Fmin, Fmax, Amin, Amax</param>
        public static void IsCorrectMinMax(TableSectorsRecon sr)
        {
            if (sr.AngleMin < 0 || sr.AngleMin > 360) { sr.AngleMin = 0; }
            if (sr.AngleMax < 0 || sr.AngleMax > 360) { sr.AngleMax = 360; }
        }

        /// <summary>
        /// Проверка на правильность ввода значений:  
        /// если Аmin меньше 0 или больше 360, Аmin = 0;
        /// если Аmax меньше 0 или больше 360, Аmax = 360
        /// </summary>
        /// <param name="sr">Fmin, Fmax, Amin, Amax</param>
        public static void IsCorrectCoord(double latitude, double longitude)
        {
            if (latitude < -90 || latitude > 90) { latitude = 0; }
            if (longitude < -180 || longitude > 180) { longitude = 0; }
        }

        /// <summary>
        /// Проверка на корректность ввода значений сектора
        /// </summary>
        /// <param name="iAngleMin"> начальное значение сектора </param>
        /// <param name="iAngleMax"> конечное значение сектора </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsCorrectAngleMinMax(short iAngleMin, short iAngleMax)
        {
            bool bCorrect = true;

            if (iAngleMin >= iAngleMax)
            {
                MessageBox.Show(SMessages.mesValuesAngleMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        public static bool IsCorrectSerialNumber(string serialNumber)
        {
            return serialNumber.Length < 14 ? false : true;
        }

        public static bool IsCorrectFreq(double dFreq)
        {
            bool bCorrect = true;

            if (dFreq < 100 || dFreq > 6000)
            {
                MessageBox.Show(SMessages.mesFreqBelongBand, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        /// <summary>
        /// Проверка на возможность добавления записей в коллекцию
        /// </summary>
        /// <param name="collectionTemp"> текущий список записей, в который добавить </param>
        /// <param name="collectionFrom"> что добавить </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsAddRecordsToCollection(ObservableCollection<FreqRanges> collectionTemp, ObservableCollection<FreqRanges> collectionFrom)
        {
            bool bAdd = false;

            try
            {
                List<FreqRanges> listTemp = new List<FreqRanges>();
                listTemp.AddRange(collectionTemp);

                List<FreqRanges> listFrom = new List<FreqRanges>();
                listFrom.AddRange(collectionFrom);

                for (int i = 0; i < listFrom.Count; i++)
                {
                    if (IsCorrectFreqMinMax(listFrom[i].FreqMinKHz, listFrom[i].FreqMaxKHz))
                    {
                        if (IsCorrectJoinRanges(listTemp, listFrom[i]))
                        {
                            bAdd = true;
                        }
                    }
                }
            }
            catch { bAdd = false; }

            return bAdd;
        }

        /// <summary>
        /// Проверка на возможность объединения диапазонов РР и РП 
        /// при добавлении или изменении записи
        /// </summary>
        /// <param name="listSectorsRanges"> текущие значения </param>
        /// <param name="sectorsRanges"> значения, которые надо проверить </param>
        /// <returns> true - успешно, false - нет </returns>
        private static bool IsCorrectJoinRanges(List<FreqRanges> listSectorsRanges, FreqRanges sectorsRanges)
        {
            for (int i = 0; i < listSectorsRanges.Count; i++)
            {
                int cmp1 = listSectorsRanges[i].FreqMinKHz.CompareTo(sectorsRanges.FreqMinKHz);
                int cmp2 = listSectorsRanges[i].FreqMaxKHz.CompareTo(sectorsRanges.FreqMinKHz);
                int cmp3 = listSectorsRanges[i].FreqMinKHz.CompareTo(sectorsRanges.FreqMaxKHz);
                int cmp4 = listSectorsRanges[i].FreqMaxKHz.CompareTo(sectorsRanges.FreqMaxKHz);

                // с = a.CompareTo(b), если результат сравнения
                // < 0 --> a < b,
                // = 0 --> a = b,
                // > 0 --> a > b.
                //int cmp5 = listSectorsRanges[i].AngleMin.CompareTo(sectorsRanges.AngleMin);
                //int cmp6 = listSectorsRanges[i].AngleMax.CompareTo(sectorsRanges.AngleMin);
                //int cmp7 = listSectorsRanges[i].AngleMin.CompareTo(sectorsRanges.AngleMax);
                //int cmp8 = listSectorsRanges[i].AngleMax.CompareTo(sectorsRanges.AngleMax);

                if ((cmp1 < 0 || cmp1 == 0) && cmp3 < 0 && cmp2 > 0 && (cmp4 < 0 || cmp4 == 0))
                {
                    //for (int j = 0; j < listSectorsRanges.Count; j++)
                    //{
                    //    if (listSectorsRanges[i].AngleMin == sectorsRanges.AngleMin &&
                    //        listSectorsRanges[i].AngleMax == sectorsRanges.AngleMax &&
                    //        cmp1 == 0 && cmp4 == 0)
                    //    {
                    //        MessageBox.Show(SMessages.mesSector + listSectorsRanges[i].AngleMin.ToString() + " - " +
                    //            listSectorsRanges[i].AngleMax.ToString() + SMessages.mesAndRange +
                    //            listSectorsRanges[i].FreqMinKHz.ToString() + " - " +
                    //            listSectorsRanges[i].FreqMaxKHz.ToString() + SMessages.mesContainSetSector,
                    //            SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //        return false;
                    //    }
                    //}

                    //// если значение угла мин. < значения угла макс. 
                    //if (cmp1 == 0 && cmp4 == 0 && sectorsRanges.AngleMin < sectorsRanges.AngleMax)
                    //{
                    //    for (int j = 0; j < listSectorsRanges.Count; j++)
                    //    {
                    //        if ((cmp5 < 0 || cmp5 == 0) && (cmp8 > 0 || cmp8 == 0))
                    //        {
                    //            MessageBox.Show(SMessages.mesSector + listSectorsRanges[i].AngleMin.ToString() + " - " +
                    //                listSectorsRanges[i].AngleMax.ToString() + SMessages.mesAndRange +
                    //                listSectorsRanges[i].FreqMinKHz.ToString() + " - " +
                    //                listSectorsRanges[i].FreqMaxKHz.ToString() + SMessages.mesContainSetSector,
                    //                SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //            return false;
                    //        }
                    //    }
                    //}
                    //// если значение угла мин. < значения угла макс. 
                    //else if (cmp1 == 0 && cmp4 == 0 && sectorsRanges.AngleMin > sectorsRanges.AngleMax)
                    //{
                    //    return true;
                    //}




                        //MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                        //   SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                  
                }

                else if ((cmp1 > 0 || cmp1 == 0) && cmp3 < 0 && cmp2 > 0 && (cmp4 > 0 || cmp4 == 0))
                {
                    //MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                    //     SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                else if ((cmp1 < 0 || cmp1 == 0) && cmp3 < 0 && cmp2 > 0 && (cmp4 > 0 || cmp4 == 0))
                {
                    //MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                    //     SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                else if ((cmp1 > 0 || cmp1 == 0) && cmp3 < 0 && cmp2 > 0 && (cmp4 < 0 || cmp4 == 0))
                {
                    //MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                    //     SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }
    }
}
