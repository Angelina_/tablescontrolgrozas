﻿namespace ValuesCorrectLib
{
    #region диапазоны литер
    public class RangesLetters
    {
        public const double FREQ_START_LETTER_1 = 30000.0F;
        public const double FREQ_START_LETTER_2 = 50000.0F;
        public const double FREQ_START_LETTER_3 = 90000.0F;
        public const double FREQ_START_LETTER_4 = 160000.0F;
        public const double FREQ_START_LETTER_5 = 290000.0F;
        public const double FREQ_START_LETTER_6 = 512000.0F;
        public const double FREQ_START_LETTER_7 = 860000.0F;
        public const double FREQ_START_LETTER_8 = 1215000.0F;
        public const double FREQ_START_LETTER_9 = 2000000.0F;
        public const double FREQ_START_LETTER_10 = 3000000.0F;
        public const double FREQ_STOP_LETTER_10 = 6000000.0F;
    }
    #endregion
}
