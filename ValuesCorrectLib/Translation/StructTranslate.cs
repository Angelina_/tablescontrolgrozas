﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    #region Сообщения об ошибках
    public struct SMessageError
    {
        public static string mesErr;
        public static string mesErrValBandwidth;
        public static string mesErrEnterValues;

        public static void InitSMessageError()
        {
            mesErr = "Ошибка!";
            mesErrValBandwidth = "Значение ширины полосы задается от 0 до 1000!";
            mesErrEnterValues = "Введите значения!";
        }
    }
    #endregion

    #region Сообщения
    public struct SMessages
    {
        public static string mesMessage;
        public static string mesValuesMaxMin;
        public static string mesValuesAngleMaxMin;
        public static string mesFreqBelongBand;

        public static void InitSMessages()
        {
            mesMessage = "Сообщение!";
            mesValuesMaxMin = "Значение поля 'F мин.' должно быть меньше значения поля 'F макс.'!";
            mesValuesAngleMaxMin = "Значение поля 'θ мин.' должно быть меньше значения поля 'θ макс.'!";
            mesFreqBelongBand = "Частота не принадлежит диапазону 100 МГц - 6000 МГц!";
        }
    }
    #endregion

    #region Значения
    public struct SMeaning
    {
        public static string meaningAddRecord;
        public static string meaningChangeRecord;

        public static string meaningHz;
        public static string meaningkHz;
        public static string meaningMHz;

        public static string meaningmks;
        public static string meaningms;
        
        public static string meaningCoord;
        public static string meaningFreqUAV;

        public static string meaningNoMode;
        public static string meaningScanning;

        public static void InitSMeaning()
        {
            meaningAddRecord = "Добавить запись";
            meaningChangeRecord = "Изменить запись";

            meaningHz = "Гц";
            meaningkHz = "кГц";
            meaningMHz = "МГц";

            meaningmks = "мкс";
            meaningms = "мс";

            meaningCoord = "Координаты";
            meaningFreqUAV = "Частоты БПЛА";

            meaningNoMode = "Без режима";
            meaningScanning = "Сканирование";
        }
    }
    #endregion

    #region Заголовки таблиц для отчетов (текстовых файлов .doc, .xls, .txt)
    public struct SHeaders
    {
        public static string headerLatLon;
        public static string headerAlt;
        public static string headerFreqMin;
        public static string headerFreqMax;
        public static string headerNote;
        public static string headerDeltaF;
        public static string headerNum;
        public static string headerFreq;
        public static string headerCount;
    

        public static void InitSHeader()
        {
            headerLatLon = "Шир.,°  Долг.,°";
            headerAlt = "Выс., м";
            headerFreqMin = "F мин., кГц";
            headerFreqMax = "F макс., кГц";
            headerNote = "Примечание";
            headerDeltaF = "Δf, кГц";
            headerNum = "№";
            headerFreq = "F, кГц";
            headerCount = "Кол-во";
       
        }
    }
    #endregion
}
