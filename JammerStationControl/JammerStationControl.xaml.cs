﻿using GrozaSModelsDBLib;
using System;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace JammerStationControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlJammerStation : UserControl, ISelectedRowEvents
    {
        public JammerStationProperty JammerStationWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        public event EventHandler<SelectedRowEvents> OnSelectedRow = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<SelectedRowEvents> OnDoubleClickStation = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<SelectedRowEvents> OnGetCoords = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<SelectedRowEvents> OnGetTime = (object sender, SelectedRowEvents data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<JammerStationProperty> OnIsWindowPropertyOpen = (object sender, JammerStationProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion


        #region Properties
        public NameTable NameTable { get; } = NameTable.TableJammerStation;
        #endregion


        public UserControlJammerStation()
        {
            InitializeComponent();

            // TEST --------------------------------------------------------------
            //PropViewCoords.ViewCoords = ViewCoord;
            //ViewCoord = (byte)3;
            //PropViewCoords.ViewCoords = 2;
            // -------------------------------------------------------------- TEST 

            DgvJammerStation.DataContext = new GlobalJammerStation();

        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                JammerStationWindow = new JammerStationProperty(((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation);

                OnIsWindowPropertyOpen(this, JammerStationWindow);

                //////////////////////////////////////////////////////////////////////////
                if (!PropIsRecJammerStation.IsRecAdd && !PropIsRecJammerStation.IsRecChange)
                {
                    JammerStationWindow.Show();
                    PropIsRecJammerStation.IsRecAdd = true;
                    JammerStationWindow.OnAddRecordPG += new EventHandler<MyTableJammerStation>(JammerStationWindow_OnAddRecordPG);
                }
                //////////////////////////////////////////////////////////////////////////

                //if (JammerStationWindow.ShowDialog() == true)
                //{
                //    // Событие добавления одной записи
                //    OnAddRecord(this, new TableEvent(JammerStationWindow.JammerStation));
                //}
            }
            catch { }
        }

        private void JammerStationWindow_OnAddRecordPG(object sender, MyTableJammerStation e)
        {
            PropIsRecJammerStation.IsRecAdd = false;

            // Событие добавления одной записи
            OnAddRecord(this, new TableEvent(MyTableJammerStationToTableJammerStation(e)));

        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((MyTableJammerStation)DgvJammerStation.SelectedItem != null)
                {
                    if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id > 0)
                    {
                        var selected = (MyTableJammerStation)DgvJammerStation.SelectedItem;

                        JammerStationWindow = new JammerStationProperty(((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation, selected.Clone());

                        OnIsWindowPropertyOpen(this, JammerStationWindow);

                        //////////////////////////////////////////////////////////////////////////
                        if (!PropIsRecJammerStation.IsRecAdd && !PropIsRecJammerStation.IsRecChange)
                        {
                            JammerStationWindow.Show();
                            PropIsRecJammerStation.IsRecChange = true;
                            JammerStationWindow.OnChangeRecordPG += new EventHandler<MyTableJammerStation>(JammerStationWindow_OnChangeRecordPG);
                        }
                        //////////////////////////////////////////////////////////////////////////

                        //if (JammerStationWindow.ShowDialog() == true)
                        //{
                        //    // Событие изменения одной записи
                        //    OnChangeRecord(this, new TableEvent(JammerStationWindow.JammerStation));
                        //}
                    }
                }
            }
            catch { }
        }

        private void JammerStationWindow_OnChangeRecordPG(object sender, MyTableJammerStation e)
        {
            PropIsRecJammerStation.IsRecChange = false;

            // Событие изменения одной записи
            OnChangeRecord(this, new TableEvent(MyTableJammerStationToTableJammerStation(e)));
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((MyTableJammerStation)DgvJammerStation.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableJammerStation tableJammerStation = new TableJammerStation
                    {
                        Id = ((MyTableJammerStation)DgvJammerStation.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableJammerStation));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableJammerStation);
            }
            catch { }
        }

        private void DgvJammerStation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((MyTableJammerStation)DgvJammerStation.SelectedItem == null) return;

            if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id > -1)
            {
                if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id != PropNumJammerStation.SelectedNumJammerStation)
                {
                    PropNumJammerStation.SelectedNumJammerStation = ((MyTableJammerStation)DgvJammerStation.SelectedItem).Id;
                    PropNumJammerStation.IsSelectedNumJammerStation = true;

                    OnSelectedRow(this, new SelectedRowEvents(PropNumJammerStation.SelectedNumJammerStation));
                }
            }
            else
            {
                PropNumJammerStation.SelectedNumJammerStation = 0;
                PropNumJammerStation.IsSelectedNumJammerStation = false;
            }
        }

        private void DgvJammerStation_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvJammerStation_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if ((MyTableJammerStation)DgvJammerStation.SelectedItem == null) return;

            if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id > -1)
            {
                OnDoubleClickStation(this, new SelectedRowEvents(((MyTableJammerStation)DgvJammerStation.SelectedItem).Id));
            }
        }

        private void GetCoordsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if ((MyTableJammerStation)DgvJammerStation.SelectedItem == null) return;

            if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id > -1)
            {
                OnGetCoords(this, new SelectedRowEvents(((MyTableJammerStation)DgvJammerStation.SelectedItem).Id));
            }
        }

        private void GetTimeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if ((MyTableJammerStation)DgvJammerStation.SelectedItem == null) return;

            if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id > -1)
            {
                OnGetTime(this, new SelectedRowEvents(((MyTableJammerStation)DgvJammerStation.SelectedItem).Id));
            }
        }

        //private void JammerStation_CheckOwn(object sender, SelectionChangedEventArgs e)
        //{
        //    if ((MyTableJammerStation)DgvJammerStation..SelectedItem == null) return;

        //    if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id > -1)
        //    {
        //        if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id != PropNumJammerStation.SelectedNumJammerStation)
        //        {
        //            PropNumJammerStation.SelectedNumJammerStation = ((MyTableJammerStation)DgvJammerStation.SelectedItem).Id;
        //            PropNumJammerStation.IsSelectedNumJammerStation = true;

        //            OnSelectedRow(this, new SelectedRowEvents(PropNumJammerStation.SelectedNumJammerStation));
        //        }
        //    }
        //    else
        //    {
        //        PropNumJammerStation.SelectedNumJammerStation = 0;
        //        PropNumJammerStation.IsSelectedNumJammerStation = false;
        //    }
        //}
    }
}
