﻿using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using TableEvents;
using ValuesCorrectLib;

namespace JammerStationControl
{
    /// <summary>
    /// Логика взаимодействия для JammerStationProperty.xaml
    /// </summary>
    public partial class JammerStationProperty : Window
    {
        public event EventHandler<MyTableJammerStation> OnAddRecordPG = (object sender, MyTableJammerStation data) => { };
        public event EventHandler<MyTableJammerStation> OnChangeRecordPG = (object sender, MyTableJammerStation data) => { };

        private ObservableCollection<MyTableJammerStation> collectionTemp;
        public MyTableJammerStation JammerStation { get; private set; }
        public JammerStationProperty(ObservableCollection<MyTableJammerStation> collectionJammerStation)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionJammerStation;
                JammerStation = new MyTableJammerStation();

                propertyGrid.SelectedObject = JammerStation;
                propertyGrid.Properties[nameof(MyTableJammerStation.Coordinates)].IsBrowsable = false;
                propertyGrid.Properties[nameof(MyTableJammerStation.Id)].IsReadOnly = false;

                //switch (PropViewCoords.ViewCoords)
                //{
                //    case 1: // format "DD.dddddd"
                //        propertyGrid.Properties[nameof(MyTableJammerStation.CoordinatesDDMMSS)].IsBrowsable = false;
                //        break;

                //    case 2: // format "DD MM.mmmm"
                //    case 3: // format "DD MM SS.ss"
                //        propertyGrid.Properties[nameof(MyTableJammerStation.Coordinates)].IsBrowsable = false;
                //        break;

                //    default:
                //        break;
                //}
                
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;

                //ChangeCategories();
            }
            catch { }
        }

        public JammerStationProperty(ObservableCollection<MyTableJammerStation> collectionJammerStation, MyTableJammerStation tableJammerStation)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionJammerStation;
                JammerStation = tableJammerStation;


                //////////////////////////
                JammerStation.CoordinatesDDMMSS.LatSign = JammerStation.Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                JammerStation.CoordinatesDDMMSS.LonSign = JammerStation.Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                JammerStation.CoordinatesDDMMSS.Altitude = JammerStation.Coordinates.Altitude;

                if (JammerStation.Coordinates.Latitude != -1)
                {
                    JammerStation.Coordinates.Latitude = Math.Round(JammerStation.Coordinates.Latitude, 6);
                    JammerStation.Coordinates.Latitude = JammerStation.Coordinates.Latitude < 0 ? JammerStation.Coordinates.Latitude * -1 : JammerStation.Coordinates.Latitude;
                }
                if (JammerStation.Coordinates.Longitude != -1)
                {
                    JammerStation.Coordinates.Longitude = Math.Round(JammerStation.Coordinates.Longitude, 6);
                    JammerStation.Coordinates.Longitude = JammerStation.Coordinates.Longitude < 0 ? JammerStation.Coordinates.Longitude * -1 : JammerStation.Coordinates.Longitude;
                }

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"

                        JammerStation.CoordinatesDDMMSS.Latitude = JammerStation.Coordinates.Latitude;
                        JammerStation.CoordinatesDDMMSS.Longitude = JammerStation.Coordinates.Longitude;
                        break;

                    case 2: // format "DD MM.mmmm"

                        JammerStation.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(JammerStation.Coordinates.Latitude);
                        JammerStation.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((JammerStation.Coordinates.Latitude - Math.Truncate(JammerStation.Coordinates.Latitude)) * 60, 4);

                        JammerStation.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(JammerStation.Coordinates.Longitude);
                        JammerStation.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((JammerStation.Coordinates.Longitude - Math.Truncate(JammerStation.Coordinates.Longitude)) * 60, 4);
                        break;

                    case 3: // format "DD MM SS.ss"

                        double latD = Math.Truncate(JammerStation.Coordinates.Latitude);
                        double latM = Math.Truncate((JammerStation.Coordinates.Latitude - latD) * 60);
                        double latS = Math.Round((((JammerStation.Coordinates.Latitude - latD) * 60) - latM) * 60, 2);

                        JammerStation.CoordinatesDDMMSS.LatDegrees = (int)latD;
                        JammerStation.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)latM;
                        JammerStation.CoordinatesDDMMSS.LatSeconds = latS;

                        double lonD = Math.Truncate(JammerStation.Coordinates.Longitude);
                        double lonM = Math.Truncate((JammerStation.Coordinates.Longitude - lonD) * 60);
                        double lonS = Math.Round((((JammerStation.Coordinates.Longitude - lonD) * 60) - lonM) * 60, 2);

                        JammerStation.CoordinatesDDMMSS.LonDegrees = (int)lonD;
                        JammerStation.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)lonM;
                        JammerStation.CoordinatesDDMMSS.LonSeconds = lonS;
                        break;

                    default:
                        break;
                }

                propertyGrid.SelectedObject = JammerStation;

                propertyGrid.Properties[nameof(MyTableJammerStation.Coordinates)].IsBrowsable = false;
                propertyGrid.Properties[nameof(MyTableJammerStation.Id)].IsReadOnly = true;
                
                ///////////////////////





                //if (JammerStation.Coordinates.Latitude != -1)
                //{
                //    JammerStation.Coordinates.Latitude = JammerStation.Coordinates.Latitude < 0 ? JammerStation.Coordinates.Latitude * -1 : JammerStation.Coordinates.Latitude;
                //    JammerStation.Coordinates.Latitude = Math.Round(JammerStation.Coordinates.Latitude, 6);
                //}
                //if (JammerStation.Coordinates.Longitude != -1)
                //{
                //    JammerStation.Coordinates.Longitude = JammerStation.Coordinates.Longitude < 0 ? JammerStation.Coordinates.Longitude * -1 : JammerStation.Coordinates.Longitude;
                //    JammerStation.Coordinates.Longitude = Math.Round(JammerStation.Coordinates.Longitude, 6);
                //}
                //propertyGrid.SelectedObject = JammerStation;
                //propertyGrid.Properties[nameof(MyTableJammerStation.Id)].IsReadOnly = true;

                //switch (PropViewCoords.ViewCoords)
                //{
                //    case 1: // format "DD.dddddd"
                //        propertyGrid.Properties[nameof(MyTableJammerStation.CoordinatesDDMMSS)].IsBrowsable = false;
                //        break;

                //    case 2: // format "DD MM.mmmm"
                //    case 3: // format "DD MM SS.ss"
                //        propertyGrid.Properties[nameof(MyTableJammerStation.Coordinates)].IsBrowsable = false;
                //        break;

                //    default:
                //        break;
                //}

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;

            }
            catch { }
        }

        public JammerStationProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((MyTableJammerStation)propertyGrid.SelectedObject) != null)
            {
                if (PropIsRecJammerStation.IsRecAdd)
                {
                    OnAddRecordPG?.Invoke(sender, (MyTableJammerStation)propertyGrid.SelectedObject);
                }

                if (PropIsRecJammerStation.IsRecChange)
                {
                    OnChangeRecordPG?.Invoke(sender, (MyTableJammerStation)propertyGrid.SelectedObject);
                }

                Close();
                //DialogResult = true;
            }
        }


        public MyTableJammerStation IsAddClick(MyTableJammerStation JammerStationWindow)
        {
            if (JammerStationWindow.CoordinatesDDMMSS.Latitude < -90 || JammerStationWindow.CoordinatesDDMMSS.Latitude > 90) { JammerStationWindow.CoordinatesDDMMSS.Latitude = 0; }
            if (JammerStationWindow.CoordinatesDDMMSS.Longitude < -180 || JammerStationWindow.CoordinatesDDMMSS.Longitude > 180) { JammerStationWindow.CoordinatesDDMMSS.Longitude = 0; }

            //CorrectParams.IsCorrectMinMax(JammerStationWindow);
            //if (CorrectParams.IsCorrectFreqMinMax(JammerStationWindow.FreqMinKHz, FreqRanges.FreqMaxKHz))
            //{
            //    JammerStationWindow.IsActive = true;
            //    return JammerStationWindow;
            //}
            return JammerStationWindow;
            //return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            Close();
            PropIsRecJammerStation.IsRecAdd = false;
            PropIsRecJammerStation.IsRecChange = false;

            //DialogResult = false;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void InitEditors()
        {
            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    propertyGrid.Editors.Add(new CoordinatesDDJammerStationEditor(nameof(JammerStation.CoordinatesDDMMSS), typeof(MyTableJammerStation)));
                    propertyGrid.Editors.Add(new RoleDDJammerStationEditor(nameof(JammerStation.Role), typeof(MyTableJammerStation)));
                    break;

                case 2: // format "DD MM.mmmm"
                    propertyGrid.Editors.Add(new CoordinatesDDMMJammerStationEditor(nameof(JammerStation.CoordinatesDDMMSS), typeof(MyTableJammerStation)));
                    propertyGrid.Editors.Add(new RoleDDMMJammerStationEditor(nameof(JammerStation.Role), typeof(MyTableJammerStation)));
                    break;

                case 3: // format "DD MM SS.ss"
                    propertyGrid.Editors.Add(new CoordinatesDDMMSSJammerStationEditor(nameof(JammerStation.CoordinatesDDMMSS), typeof(MyTableJammerStation)));
                    propertyGrid.Editors.Add(new RoleDDMMSSJammerStationEditor(nameof(JammerStation.Role), typeof(MyTableJammerStation)));
                    break;

                default:
                    break;
            }
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((MyTableJammerStation)propertyGrid.SelectedObject) != null)
                {
                    if (PropIsRecJammerStation.IsRecAdd)
                    {
                        OnAddRecordPG?.Invoke(sender, (MyTableJammerStation)propertyGrid.SelectedObject);
                    }

                    if (PropIsRecJammerStation.IsRecChange)
                    {
                        OnChangeRecordPG?.Invoke(sender, (MyTableJammerStation)propertyGrid.SelectedObject);
                    }

                    Close();
                }
            }
        }

        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                            UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.SRB.xaml",
                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.FR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.FR.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllGrozaSProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllGrozaSProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropIsRecJammerStation.IsRecAdd = false;
            PropIsRecJammerStation.IsRecChange = false;
        }
    }
}
