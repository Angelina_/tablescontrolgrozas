﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace JammerStationControl
{
    public partial class UserControlJammerStation : UserControl
    {
       // public List<TableJammerStation> TempListJammerStation { get; set; }
        public List<MyTableJammerStation> TempListJammerStation { get; set; }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listJammerStation"></param>
        //public void UpdateJammerStation(List<TableJammerStation> listJammerStation)
        //{
        //    try
        //    {
        //        if (listJammerStation == null)
        //            return;

        //        TempListJammerStation = listJammerStation;


        //        ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Clear();

        //        for (int i = 0; i < listJammerStation.Count; i++)
        //        {
        //            ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Add(listJammerStation[i]);
        //        }

        //        AddEmptyRows();

        //        int ind = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id == PropNumJammerStation.SelectedNumJammerStation);
        //        if (ind != -1)
        //        {
        //            DgvJammerStation.SelectedIndex = ind;
        //        }
        //        else
        //        {
        //            DgvJammerStation.SelectedIndex = 0;
        //        }

        //    }
        //    catch { }
        //}

        public void UpdateJammerStation(List<TableJammerStation> listJammerStation)
        {
            try
            {
                if (listJammerStation == null)
                    return;

                //TempListJammerStation = listJammerStation;


                ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Clear();

                List<MyTableJammerStation> list = TableJammerStationToMyTableJammerStation(listJammerStation);

                TempListJammerStation = list;

                for (int i = 0; i < listJammerStation.Count; i++)
                {
                    ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Add(list[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id == PropNumJammerStation.SelectedNumJammerStation);
                if (ind != -1)
                {
                    DgvJammerStation.SelectedIndex = ind;
                }
                else
                {
                    DgvJammerStation.SelectedIndex = 0;
                }

                var own = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.FirstOrDefault(x => x.Role == StationRole.Own);
                PropNumJammerStation.OwnNumJammerStation = own != null ? own.Id : -1;
             
            }
            catch { }
        }

        /// <summary>
        /// Преобразование TableJammerStation к MyTableJammerStation
        /// </summary>
        /// <param name="listJammerStation"></param>
        /// <returns> List<MyTableJammerStation> </returns>
        private List<MyTableJammerStation> TableJammerStationToMyTableJammerStation(List<TableJammerStation> listJammerStation)
        {
            List<MyTableJammerStation> list = new List<MyTableJammerStation>();
            for (int i = 0; i < listJammerStation.Count; i++)
            {
                MyTableJammerStation table = new MyTableJammerStation();

                double minutesLat = (listJammerStation[i].Coordinates.Latitude - Math.Truncate(listJammerStation[i].Coordinates.Latitude)) * 60;
                double minutesLon = (listJammerStation[i].Coordinates.Longitude - Math.Truncate(listJammerStation[i].Coordinates.Longitude)) * 60;

                table.Id = listJammerStation[i].Id;
                table.Role = listJammerStation[i].Role;
                table.CallSign = listJammerStation[i].CallSign;
                table.Coordinates.Altitude = listJammerStation[i].Coordinates.Altitude;
                table.Coordinates.Latitude = listJammerStation[i].Coordinates.Latitude;
                table.Coordinates.Longitude = listJammerStation[i].Coordinates.Longitude;
                table.CoordinatesDDMMSS.LatSign = listJammerStation[i].Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                table.CoordinatesDDMMSS.LonSign = listJammerStation[i].Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                table.CoordinatesDDMMSS.Latitude = listJammerStation[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Latitude = listJammerStation[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Altitude = listJammerStation[i].Coordinates.Altitude;
                table.CoordinatesDDMMSS.Latitude = listJammerStation[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Longitude = listJammerStation[i].Coordinates.Longitude;
                table.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(listJammerStation[i].Coordinates.Latitude);
                table.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(listJammerStation[i].Coordinates.Longitude);
                table.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((listJammerStation[i].Coordinates.Latitude - Math.Truncate(listJammerStation[i].Coordinates.Latitude)) * 60, 4);
                table.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((listJammerStation[i].Coordinates.Longitude - Math.Truncate(listJammerStation[i].Coordinates.Longitude)) * 60, 4);
                table.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((listJammerStation[i].Coordinates.Latitude - Math.Truncate(listJammerStation[i].Coordinates.Latitude)) * 60);
                table.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((listJammerStation[i].Coordinates.Longitude - Math.Truncate(listJammerStation[i].Coordinates.Longitude)) * 60);
                table.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                table.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);
                table.CoordinatesDDMMSS.Altitude = listJammerStation[i].Coordinates.Altitude;
                table.DeltaTime = listJammerStation[i].DeltaTime;
                table.IsGnssUsed = listJammerStation[i].IsGnssUsed;
                table.Note = listJammerStation[i].Note;
               
                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Преобразование MyTableJammerStation к TableJammerStation
        /// </summary>
        /// <param name="listJammerStation"></param>
        /// <returns> List<TableJammerStation> </returns>
        private TableJammerStation MyTableJammerStationToTableJammerStation(MyTableJammerStation tableJammerStation)
        {
            TableJammerStation table = new TableJammerStation();

            table.Id = tableJammerStation.Id;
            table.Role = tableJammerStation.Role;
            table.CallSign = tableJammerStation.CallSign;
            table.DeltaTime = tableJammerStation.DeltaTime;
            table.IsGnssUsed = tableJammerStation.IsGnssUsed;
            table.Note = tableJammerStation.Note;

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    table.Coordinates.Altitude = tableJammerStation.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = tableJammerStation.CoordinatesDDMMSS.Latitude;
                    table.Coordinates.Longitude = tableJammerStation.CoordinatesDDMMSS.Longitude;
                    break;

                case 2: // format "DD MM.mmmm"
                    table.Coordinates.Altitude = tableJammerStation.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableJammerStation.CoordinatesDDMMSS.LatDegrees + ((double)tableJammerStation.CoordinatesDDMMSS.LatMinutesDDMM / 60), 6); 
                    table.Coordinates.Longitude = Math.Round(tableJammerStation.CoordinatesDDMMSS.LonDegrees + ((double)tableJammerStation.CoordinatesDDMMSS.LonMinutesDDMM / 60), 6);
                    break;

                case 3: // format "DD MM SS.ss"

                    table.Coordinates.Altitude = tableJammerStation.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableJammerStation.CoordinatesDDMMSS.LatDegrees + ((double)tableJammerStation.CoordinatesDDMMSS.LatMinutesDDMMSS / 60) + (tableJammerStation.CoordinatesDDMMSS.LatSeconds / 3600), 6);
                    table.Coordinates.Longitude = Math.Round(tableJammerStation.CoordinatesDDMMSS.LonDegrees + ((double)tableJammerStation.CoordinatesDDMMSS.LonMinutesDDMMSS / 60) + (tableJammerStation.CoordinatesDDMMSS.LonSeconds / 3600), 6);
                    break;
                default:
                    break;
            }

            table.Coordinates.Latitude = tableJammerStation.CoordinatesDDMMSS.LatSign == LatitudeSign.S ? table.Coordinates.Latitude * -1 : table.Coordinates.Latitude;
            table.Coordinates.Longitude = tableJammerStation.CoordinatesDDMMSS.LonSign == LongitudeSign.W ? table.Coordinates.Longitude * -1 : table.Coordinates.Longitude;

              
            return table;
        }


        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listJammerStation"></param>
        public void AddJammerStation(List<TableJammerStation> listJammerStation)
        {
            //try
            //{
            //    TempListJammerStation = listJammerStation;

            //    DeleteEmptyRows();

            //    for (int i = 0; i < listJammerStation.Count; i++)
            //    {
            //        int ind = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id == listJammerStation[i].Id);
            //        if (ind != -1)
            //        {
            //            ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation[ind] = listJammerStation[i];
            //        }
            //        else
            //        {
            //            ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Add(listJammerStation[i]);
            //        }
            //    }

            //    AddEmptyRows();
            //}
            //catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteJammerStation(TableJammerStation tableJammerStation)
        {
            try
            {
                int index = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id == tableJammerStation.Id);
                ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearJammerStation()
        {
            try
            {
                ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        //private void AddEmptyRows()
        //{
        //    try
        //    {
        //        int сountRowsAll = DgvJammerStation.Items.Count; // количество всех строк в таблице
        //        double hs = 23; // высота строки
        //        double ah = DgvJammerStation.ActualHeight; // визуализированная высота dataGrid
        //        double chh = DgvJammerStation.ColumnHeaderHeight; // высота заголовка

        //        int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

        //        int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
        //        int index = -1;
        //        for (int i = 0; i < count; i++)
        //        {
        //            // Удалить пустые строки в dgv
        //            index = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id < 0);
        //            if (index != -1)
        //            {
        //                ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.RemoveAt(index);
        //            }
        //        }

        //        List<TableJammerStation> list = new List<TableJammerStation>();
        //        for (int i = 0; i < countRows - сountRowsAll; i++)
        //        {
        //            TableJammerStation strJS = new TableJammerStation
        //            {
        //                Id = -2,
        //                CallSign = string.Empty,
        //                Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
        //                Role = StationRole.Complex,
        //                DeltaTime = string.Empty,
        //                Note = string.Empty
        //            };
        //            list.Add(strJS);
        //        }

        //        for (int i = 0; i < list.Count; i++)
        //        {
        //            ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Add(list[i]);
        //        }
        //    }
        //    catch { }
        //}

        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvJammerStation.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvJammerStation.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvJammerStation.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.RemoveAt(index);
                    }
                }

                List<MyTableJammerStation> list = new List<MyTableJammerStation>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    MyTableJammerStation strJS = new MyTableJammerStation
                    {
                        Id = -2,
                        CallSign = string.Empty,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        Role = StationRole.Complex,
                        DeltaTime = string.Empty,
                        Note = string.Empty
                    };
                    list.Add(strJS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Count(s => s.Id < 0);
                int countAllRows = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.RemoveAt(iCount);
                }
            }
            catch { }
        }

        //private bool IsSelectedRowEmpty()
        //{
        //    try
        //    {
        //        if (((TableJammerStation)DgvJammerStation.SelectedItem).Id == -2)
        //            return false;
        //    }
        //    catch { }

        //    return true;
        //}

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((MyTableJammerStation)DgvJammerStation.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Установить АСП
        /// </summary>
        /// <param name="coord"></param>
        public void SetJammerStationToPG(Coord coord)
        {
            try
            {
                if (JammerStationWindow != null)
                {
                    JammerStationWindow.JammerStation.CoordinatesDDMMSS.LatSign = coord.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                    JammerStationWindow.JammerStation.CoordinatesDDMMSS.LonSign = coord.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                    JammerStationWindow.JammerStation.CoordinatesDDMMSS.Altitude = coord.Altitude;

                    coord.Latitude = Math.Round(coord.Latitude, 6);
                    coord.Latitude = coord.Latitude < 0 ? coord.Latitude * -1 : coord.Latitude;
                    coord.Longitude = Math.Round(coord.Longitude, 6);
                    coord.Longitude = coord.Longitude < 0 ? coord.Longitude * -1 : coord.Longitude;

                    switch (PropViewCoords.ViewCoords)
                    {
                        case 1: // format "DD.dddddd"

                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.Latitude = coord.Latitude;
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.Longitude = coord.Longitude;
                            break;

                        case 2: // format "DD MM.mmmm"

                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(coord.Latitude);
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((coord.Latitude - Math.Truncate(coord.Latitude)) * 60, 4);

                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(coord.Longitude);
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((coord.Longitude - Math.Truncate(coord.Longitude)) * 60, 4);
                            break;

                        case 3: // format "DD MM SS.ss"

                            double latD = Math.Truncate(coord.Latitude);
                            double latM = Math.Truncate((coord.Latitude - latD) * 60);
                            double latS = Math.Round(((coord.Latitude - latD) * 60 - latM) * 60, 2);

                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LatDegrees = (int)latD;
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)latM;
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LatSeconds = latS;

                            double lonD = Math.Truncate(coord.Longitude);
                            double lonM = Math.Truncate((coord.Longitude - lonD) * 60);
                            double lonS = Math.Round(((coord.Longitude - lonD) * 60 - lonM) * 60, 2);

                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LonDegrees = (int)lonD;
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)lonM;
                            JammerStationWindow.JammerStation.CoordinatesDDMMSS.LonSeconds = lonS;
                            break;

                        default:
                            break;
                    }
                }
            }
            catch { }
        }

    }
}
