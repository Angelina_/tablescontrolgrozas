﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace JammerStationControl
{
    public partial class UserControlJammerStation : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion



        #region FormatViewCoord

        public static readonly DependencyProperty ViewCoordProperty = DependencyProperty.Register("ViewCoord", typeof(byte), typeof(UserControlJammerStation),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(ViewCoordChanged)));

        public byte ViewCoord
        {
            get { return (byte)GetValue(ViewCoordProperty); }
            set { SetValue(ViewCoordProperty, value); }
        }

        private static void ViewCoordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlJammerStation userControlJammerStation = (UserControlJammerStation)d;
                userControlJammerStation.UpdateFormatCoords();
            }
            catch
            { }

        }
        #endregion

        /// <summary>
        /// Обновить вид координат в таблице
        /// </summary>
        private void UpdateFormatCoords()
        {
            try
            {
                if (TempListJammerStation == null) return;

                PropViewCoords.ViewCoords = ViewCoord;

                ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Clear();

                for (int i = 0; i < TempListJammerStation.Count; i++)
                {
                    ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.Add(TempListJammerStation[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalJammerStation)DgvJammerStation.DataContext).CollectionJammerStation.ToList().FindIndex(x => x.Id == PropNumJammerStation.SelectedNumJammerStation);
                if (ind != -1)
                {
                    DgvJammerStation.SelectedIndex = ind;
                }
                else
                {
                    DgvJammerStation.SelectedIndex = 0;
                }
            }
            catch { }
        }

       
    }
}
