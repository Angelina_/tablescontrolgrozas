﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace JammerStationControl
{
    using TableEvents;

    public class GlobalJammerStation
    {
        public ObservableCollection<MyTableJammerStation> CollectionJammerStation { get; set; }

        public GlobalJammerStation()
        {
            try
            {
                CollectionJammerStation = new ObservableCollection<MyTableJammerStation> { };
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }

    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    public class MyTableJammerStation 
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), ReadOnly(false)]
        [Browsable(true)]
        [PropertyOrder(1)]
        public int Id { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(CallSign))]
        public string CallSign { get; set; } = String.Empty;

        [DataMember]
        [Category("Прочее")]
        [Browsable(false)]
        [DisplayName(nameof(DeltaTime))]
        public string DeltaTime { get; set; } = "";

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Role))]
        [PropertyOrder(3)]
        public StationRole Role { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(nameof(IsGnssUsed))]
        [PropertyOrder(2)]
        public bool IsGnssUsed { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CoordDDMMSS CoordinatesDDMMSS { get; set; } = new CoordDDMMSS();

        public MyTableJammerStation Clone()
        {
            return new MyTableJammerStation
            {
                Id = Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                CallSign = CallSign,
                DeltaTime = DeltaTime,
                Note = Note,
                Role = Role,
                IsGnssUsed = IsGnssUsed,
                CoordinatesDDMMSS = new CoordDDMMSS
                {
                    LatDegrees = CoordinatesDDMMSS.LatDegrees,
                    LonDegrees = CoordinatesDDMMSS.LonDegrees,
                    LatMinutesDDMM = CoordinatesDDMMSS.LatMinutesDDMM,
                    LonMinutesDDMM = CoordinatesDDMMSS.LonMinutesDDMM,
                    LatMinutesDDMMSS = CoordinatesDDMMSS.LatMinutesDDMMSS,
                    LonMinutesDDMMSS = CoordinatesDDMMSS.LonMinutesDDMMSS,
                    LatSeconds = CoordinatesDDMMSS.LatSeconds,
                    LonSeconds = CoordinatesDDMMSS.LonSeconds,
                    Altitude = CoordinatesDDMMSS.Altitude
                }
            };
        }
    }

    public class CoordDDMMSS : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private LatitudeSign latSign = LatitudeSign.N;
        [DataMember]
        [DisplayName(nameof(LatSign))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public LatitudeSign LatSign
        {
            get { return latSign; }
            set
            {
                if (latSign == value)
                    return;

                latSign = value;
                OnPropertyChanged(nameof(LatSign));
            }
        }

        private LongitudeSign lonSign = LongitudeSign.E;
        [DataMember]
        [DisplayName(nameof(LonSign))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public LongitudeSign LonSign
        {
            get { return lonSign; }
            set
            {
                if (lonSign == value)
                    return;

                lonSign = value;
                OnPropertyChanged(nameof(LonSign));
            }
        }

        private double latitude = 0;
        [DataMember]
        [DisplayName(nameof(Latitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;

                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private double longitude = 0;
        [DataMember]
        [DisplayName(nameof(Longitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;

                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private int latDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LatDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatDegrees
        {
            get { return latDegrees; }
            set
            {
                if (latDegrees == value)
                    return;

                latDegrees = value;
                OnPropertyChanged(nameof(LatDegrees));
            }
        }

        private int lonDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LonDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonDegrees
        {
            get { return lonDegrees; }
            set
            {
                if (lonDegrees == value)
                    return;

                lonDegrees = value;
                OnPropertyChanged(nameof(LonDegrees));
            }
        }

        private double latMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatMinutesDDMM
        {
            get { return latMinutesDDMM; }
            set
            {
                if (latMinutesDDMM == value)
                    return;

                latMinutesDDMM = value;
                OnPropertyChanged(nameof(LatMinutesDDMM));
            }
        }

        private double lonMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonMinutesDDMM
        {
            get { return lonMinutesDDMM; }
            set
            {
                if (lonMinutesDDMM == value)
                    return;

                lonMinutesDDMM = value;
                OnPropertyChanged(nameof(LonMinutesDDMM));
            }
        }

        private int latMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatMinutesDDMMSS
        {
            get { return latMinutesDDMMSS; }
            set
            {
                if (latMinutesDDMMSS == value)
                    return;

                latMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LatMinutesDDMMSS));
            }
        }

        private int lonMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonMinutesDDMMSS
        {
            get { return lonMinutesDDMMSS; }
            set
            {
                if (lonMinutesDDMMSS == value)
                    return;

                lonMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LonMinutesDDMMSS));
            }
        }

        private double latSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LatSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatSeconds
        {
            get { return latSeconds; }
            set
            {
                if (latSeconds == value)
                    return;

                latSeconds = value;
                OnPropertyChanged(nameof(LatSeconds));
            }
        }

        private double lonSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LonSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonSeconds
        {
            get { return lonSeconds; }
            set
            {
                if (lonSeconds == value)
                    return;

                lonSeconds = value;
                OnPropertyChanged(nameof(LonSeconds));
            }
        }

        private float altitude = -1;
        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;

                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }
    }
}
