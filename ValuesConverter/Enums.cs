﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesConverter
{
    public enum TypeCuirasseM
    {
        Unknown = 0,
        Mavic = 1,
        Mavic2 = 2,
        MavicAir = 3,
        MavicAir2 = 4,
        DJIPhantom3 = 5,
        DJIPhantom4 = 6
    }

    public enum Led : byte
    {
        Empty,
        Green,
        Red,
        Blue,
        Yellow,
        Gray,
        White,
    }
}
