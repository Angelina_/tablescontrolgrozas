﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    public class RoleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            string sRole = string.Empty;

            switch (value)
            {
                case StationRole.Own:
                    sRole = "*";
                    break;

                case StationRole.Linked:
                    sRole = "→";
                    break;

                case StationRole.Complex:
                    sRole = string.Empty;
                    break;

                default:
                    sRole = string.Empty;
                    break;
            }

            return sRole;
            //}
            //catch { }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static string RoleSign(object role)
        {
            string sRole = string.Empty;

            switch (role)
            {
                case StationRole.Own:
                    sRole = "*";
                    break;

                case StationRole.Linked:
                    sRole = "→";
                    break;

                case StationRole.Complex:
                    sRole = string.Empty;
                    break;

                default:
                    sRole = string.Empty;
                    break;
            }

            return sRole;
        }
    }

    public class RolePropGridConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bRole = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (StationRole)System.Convert.ToByte(value);
        }
    }
}
