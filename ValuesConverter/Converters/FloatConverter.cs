﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class FloatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if ((float)value == -1F)
                {
                    return "-";
                }
                if ((float)value == -2F)
                {
                    return string.Empty;
                }
            }

            catch { }

            return System.Convert.ToString(((float)value).ToString("0.0"), culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
