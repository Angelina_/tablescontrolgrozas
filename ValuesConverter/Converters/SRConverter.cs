﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(bool), targetType: typeof(Uri))]
    public class SRConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri;
            switch (System.Convert.ToBoolean(value))
            {
                case true:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/green.png", UriKind.Absolute);
                    break;

                case false:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/empty.png", UriKind.Absolute);
                    break;

                default:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/empty.png", UriKind.Absolute);
                    break;
            }

            return uri;
            //}

            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
