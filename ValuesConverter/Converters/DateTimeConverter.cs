﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(DateTime), targetType: typeof(string))]
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] strTime = System.Convert.ToString(value).Split(' ');

            //try
            //{
            if ((strTime[0] == "01.01.0001" || strTime[0] == "01/01/0001" || strTime[0] == "01-01-0001" ||
                strTime[0] == "01.01.01" || strTime[0] == "01/01/01" || strTime[0] == "01-01-01" && (strTime[1] == "0:00:00" || strTime[1] == "00:00:00")) ||
                (strTime[0] == "1.1.0001" || strTime[0] == "1/1/0001" || strTime[0] == "1-1-0001" ||
                strTime[0] == "1.1.01" || strTime[0] == "1/1/01" || strTime[0] == "1-1-01" && (strTime[1] == "0:00:00" || strTime[1] == "00:00:00")) ||
                (strTime[0] == "00.00.0000" || strTime[0] == "00/00/0000" || strTime[0] == "00-00-0000" ||
                strTime[0] == "00.00.00" || strTime[0] == "00/00/00" || strTime[0] == "00-00-00" && (strTime[1] == "0:00:00" || strTime[1] == "00:00:00")))
            {
                return string.Empty;
            }
            //}
            //catch { }

            string Time = ((DateTime)value).Hour.ToString("00") + ":" + ((DateTime)value).Minute.ToString("00") + ":" + ((DateTime)value).Second.ToString("00");
            return Time;
            //return strTime[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(DateTime), targetType: typeof(string))]
    public class DateTimeOwnUAVConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] strDateTime = System.Convert.ToString(value).Split(' ');

            //try
            //{
            if ((strDateTime[0] == "01.01.0001" || strDateTime[0] == "01/01/0001" || strDateTime[0] == "01-01-0001" ||
                strDateTime[0] == "01.01.01" || strDateTime[0] == "01/01/01" || strDateTime[0] == "01-01-01") ||
                (strDateTime[0] == "1.1.0001" || strDateTime[0] == "1/1/0001" || strDateTime[0] == "1-1-0001" ||
                strDateTime[0] == "1.1.01" || strDateTime[0] == "1/1/01" || strDateTime[0] == "1-1-01" && (strDateTime[1] == "0:00:00" || strDateTime[1] == "00:00:00")) ||
                (strDateTime[0] == "00.00.0000" || strDateTime[0] == "00/00/0000" || strDateTime[0] == "00-00-0000" ||
                strDateTime[0] == "00.00.00" || strDateTime[0] == "00/00/00" || strDateTime[0] == "00-00-00"))
            {
                return string.Empty;
            }
            //}
            //catch { }

            string Date = ((DateTime)value).ToShortDateString();
            string Time = ((DateTime)value).Hour.ToString("00") + ":" + ((DateTime)value).Minute.ToString("00") + ":" + ((DateTime)value).Second.ToString("00");
            return Date + "  " + Time;
            //return strTime[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
