﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(double), targetType: typeof(string))]
    public class LatLonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if ((double)value == -1) 
            {
                return "-";
            }
            if ((double)value == -2)
               
            {
                return string.Empty;
            }

            //double dValue = ((double)value) < 0 ? ((double)value) * -1 : ((double)value);
            //}
            //catch { }

            return (System.Convert.ToString(((double)value).ToString("0.000000")));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
