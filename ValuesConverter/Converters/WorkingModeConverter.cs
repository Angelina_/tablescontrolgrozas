﻿using System;
using System.Globalization;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    public class WorkingModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
             string sMode = string.Empty;

            switch (System.Convert.ToByte(value))
            {
                case 0: 
                    sMode = SMeaning.meaningNoMode;
                    break;

                case 1: 
                    sMode = SMeaning.meaningScanning;
                    break;
               
                default:
                    sMode = string.Empty;
                    break;
            }

            return sMode;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
