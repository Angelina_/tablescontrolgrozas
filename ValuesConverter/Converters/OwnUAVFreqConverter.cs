﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(double), targetType: typeof(string))]
    public class OwnUAVFreqConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
                bool bTemp = value.ToString().Contains(",");
                int Freq = 0;

                if (bTemp) { Freq = (int)((double)value * 10d); }
                else Freq = System.Convert.ToInt32((double)value * 10);

                string strFreq = System.Convert.ToString(Freq);
                int iLength = strFreq.Length;
                string strFreqResult = string.Empty;

                try
                {
                    if (strFreq == "0")
                        return string.Empty;

                    if (iLength == 5)
                    {
                    strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                    }
                    else
                    {
                    strFreqResult = strFreq.Substring(0, iLength - 1) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }

          
            return strFreqResult;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Int32 result;

            if (Int32.TryParse(value.ToString(), NumberStyles.Any, culture, out result))
            {
                return result;
            }
            return value;
        }
    }

    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class OwnUAVBandConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sDeltaFreq = string.Empty;

            //try
            //{
            if ((float)value == -1F)
            {
                return "-";
            }
            if ((float)value == -2F)
            {
                return string.Empty;
            }

            bool bTemp = value.ToString().Contains(",");
            int DFreq = 0;

            if (bTemp) { DFreq = (int)((float)(value) * 10d); }
            else DFreq = System.Convert.ToInt32((float)value * 10);
            string strDFreq = System.Convert.ToString(DFreq);

            if (strDFreq.Length == 1)
            {
                sDeltaFreq = strDFreq + ",0".ToString(culture); ;
            }
            else
            {
                if (strDFreq.Length == 8)
                {
                    sDeltaFreq = strDFreq.Substring(0, 1) + " " + strDFreq.Substring(1, 3) + " " + strDFreq.Substring(strDFreq.Length - 4, 3) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                }
                else
                {
                    if (strDFreq.Length < 5)
                    {
                        sDeltaFreq = strDFreq.Substring(0, strDFreq.Length - 1) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                    }
                    else
                    {
                        sDeltaFreq = strDFreq.Substring(0, strDFreq.Length - 4) + " " + strDFreq.Substring(strDFreq.Length - 4, 3) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                    }
                }
            }


            //}

            //catch { }

            return sDeltaFreq;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
