﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    public class TypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sType = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sType;

            switch (value)
            {
                case (byte)TypeCuirasseM.Unknown: // 1
                    sType = "Unknown";
                    break;

                case (byte)TypeCuirasseM.Mavic: // 2
                    sType = "Mavic";
                    break;

                case (byte)TypeCuirasseM.Mavic2: // 3
                    sType = "Mavic 2";
                    break;

                case (byte)TypeCuirasseM.MavicAir: // 4
                    sType = "Mavic Air";
                    break;

                case (byte)TypeCuirasseM.MavicAir2: // 5
                    sType = "Mavic Air 2";
                    break;

                case (byte)TypeCuirasseM.DJIPhantom3: // 6
                    sType = "DJI Phantom 3";
                    break;

                case (byte)TypeCuirasseM.DJIPhantom4: // 7
                    sType = "DJI Phantom 4";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

        //}
            //catch { }

            ////try
            ////{

            //if ((byte)value == 255)
            //    return sType;

            //switch (value)
            //{
            //    case TypeUAVRes.Ocusinc:
            //        sType = "Mavic 2";
            //        break;

            //    case TypeUAVRes.G3:
            //        sType = "3G";
            //        break;

            //    case TypeUAVRes.Lightbridge:
            //        sType = "Phantom 4";
            //        break;

            //    case TypeUAVRes.WiFi:
            //        sType = "Mavic Air";
            //        break;

            //    case TypeUAVRes.Unknown:
            //        sType = "Unknown";
            //        break;

            //    default:
            //        sType = string.Empty;
            //        break;
            //}

            //}
            //catch { }

            return sType;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
