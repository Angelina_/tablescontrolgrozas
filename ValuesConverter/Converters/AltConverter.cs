﻿using GrozaSModelsDBLib;
using System;
using System.Globalization;
using System.Windows.Data;
using TableEvents;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(CoordCuirasseM), targetType: typeof(string))]
    [ValueConversion(sourceType: typeof(CoordGlobus), targetType: typeof(string))]
    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class AltConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strAlt = string.Empty;

            if (value is CoordCuirasseM)
            {
                if (System.Convert.ToDouble(((CoordCuirasseM)value).Altitude) == -1)
                {
                    return "-";
                }
                if (System.Convert.ToDouble(((CoordCuirasseM)value).Altitude) == -2)
                {
                    return string.Empty;
                }

                strAlt = System.Convert.ToString(((CoordCuirasseM)value).Altitude.ToString("0.0"));
            }
            else if (value is CoordGlobus)
            {
                if (System.Convert.ToDouble(((CoordGlobus)value).Altitude) == -1)
                {
                    return "-";
                }
                if (System.Convert.ToDouble(((CoordGlobus)value).Altitude) == -2)
                {
                    return string.Empty;
                }

                strAlt = System.Convert.ToString(((CoordGlobus)value).Altitude.ToString("0.0"));
            }
            else
            {

                //try
                //{
                if (System.Convert.ToDouble(((Coord)value).Altitude) == -1)
                {
                    return "-";
                }
                if (System.Convert.ToDouble(((Coord)value).Altitude) == -2)
                {
                    return string.Empty;
                }

                strAlt = System.Convert.ToString(((Coord)value).Altitude.ToString("0.0"));
                //}
                //catch { }
            }

            return strAlt; // (System.Convert.ToString(((Coord)value).Altitude.ToString("0.0")));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
