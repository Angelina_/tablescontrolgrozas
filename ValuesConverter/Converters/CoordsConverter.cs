﻿using CoordFormatLib;
using GrozaSModelsDBLib;
using System;
using System.Globalization;
using System.Windows.Data;
using TableEvents;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(CoordCuirasseM), targetType: typeof(string))]
    [ValueConversion(sourceType: typeof(CoordGlobus), targetType: typeof(string))]
    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class CoordsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dLat = -2;
            double dLon = -2;

            if (value is CoordCuirasseM)
            {
                dLat = System.Convert.ToDouble(((CoordCuirasseM)value).Latitude);
                dLon = System.Convert.ToDouble(((CoordCuirasseM)value).Longitude);
            }
            else if (value is CoordGlobus)
            {
                dLat = System.Convert.ToDouble(((CoordGlobus)value).Latitude);
                dLon = System.Convert.ToDouble(((CoordGlobus)value).Longitude);
            }
            else
            {
                dLat = System.Convert.ToDouble(((Coord)value).Latitude);
                dLon = System.Convert.ToDouble(((Coord)value).Longitude);
            }

            //try
            //{

            if (dLat == -1 || dLon == -1) { return "-"; }
            if (dLat == -2 || dLon == -2) { return string.Empty; }

            //}
            //catch { }

            string[] sCoords = FormatCoords(dLat, dLon);
            string[] sSign = Sign(dLat, dLon);

            
            return sSign[0] + " " + sCoords[0] + "  " + sSign[1] + " " + sCoords[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private string[] FormatCoords(double dLat, double dLon)
        {
            VCFormat _coord = new VCFormat(dLat, dLon);

            dLat = dLat < 0 ? dLat * -1 : dLat;
            dLon = dLon < 0 ? dLon * -1 : dLon;

            string sLat = string.Empty;
            string sLon = string.Empty;

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"

                    sLat = dLat.ToString("0.000000") + "\u00B0";
                    sLon = dLon.ToString("0.000000") + "\u00B0";
                    break;

                case 2: // format "DD MM.mmmm"

                    sLat = ConvertToDM(dLat);
                    sLon = ConvertToDM(dLon);
                    break;

                case 3: // format "DD MM SS.ss"

                    sLat = ConvertToDMS(dLat);
                    sLon = ConvertToDMS(dLon);
                    break;

                default:
                    break;
            }

            string[] sCoords = new string[2];
            sCoords[0] = sLat;
            sCoords[1] = sLon;
           
            return sCoords;
        }

        string ConvertToDM(double dd)
        {
            try
            {
                return Math.Truncate(dd).ToString("00") + "\u00B0"
                        + ((dd - Math.Truncate(dd)) * 60).ToString("00.0000") + "\u0027";
            }
            catch
            {
                return string.Empty;
            }
        }

        string ConvertToDMS(double dd)
        {
            try
            {
                double d = Math.Truncate(dd);
                double m = Math.Truncate((dd - d) * 60);
                double s = ((dd - d) * 60 - m) * 60;

                return d.ToString("00") + "\u00B0"
                     + m.ToString("00") + "\u0027"
                     + s.ToString("00.00") + "\u0022";
            }
            catch
            {
                return string.Empty;
            }
        }

        private string[] Sign(double dLat, double dLon)
        {
            string[] sSign = new string[2];
            sSign[0] = dLat >= 0 ? "N" : "S";
            sSign[1] = dLon >= 0 ? "E" : "W";

            return sSign;
        }
    }

    [ValueConversion(sourceType: typeof(int), targetType: typeof(string))]
    public class SignLatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sSign = string.Empty;

            var type = value.GetType().Name;

            switch (type)
            {
                case "Double":
                    sSign = (double)value >= 0 ? "N" : "S";
                    break;

                case "Int32":
                    sSign = (int)value >= 0 ? "N" : "S";
                    break;
            }

            return sSign;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    [ValueConversion(sourceType: typeof(int), targetType: typeof(string))]
    public class SignLonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sSign = string.Empty;

            var type = value.GetType().Name;

            switch (type)
            {
                case "Double":
                    sSign = (double)value >= 0 ? "E" : "W";
                    break;

                case "Int32":
                    sSign = (int)value >= 0 ? "E" : "W";
                    break;
            }

            return sSign;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class ValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = System.Convert.ChangeType(value, targetType);
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var result = System.Convert.ChangeType(value, targetType);
                return result;
            }
            catch
            {
                return null;
            }
        }
    }

    public class LatitudeSignConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bSign = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (LatitudeSign)System.Convert.ToByte(value);
        }
    }

    public class LongitudeSignConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bSign = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (LongitudeSign)System.Convert.ToByte(value);
        }
    }
}
