﻿using System;
using GrozaSModelsDBLib;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
   public class TypeConnectGunConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sType = string.Empty;

            //try
            //{

            switch (value)
            {
                case TypeConnectGun.Modem3G: // 0
                    sType = "3G / 4G";
                    break;

                case TypeConnectGun.ModemUHF: // 1
                    sType = "УКВ приемник";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }
            //}
            //catch { }

        return sType;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

   public class TypeConnectGunPropGridConverter : IValueConverter
   {
       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           byte bTypeConnectGun = (byte)value;

           return (byte)value;
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return (TypeConnectGun)System.Convert.ToByte(value);
       }
   }

}
