﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(Uri))]
    public class StateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri;
            switch (System.Convert.ToByte(value))
            {
                case 0:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/gray.png", UriKind.Absolute);
                    break;

                case 1:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/blue.png", UriKind.Absolute);
                    break;

                case 2:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/empty.png", UriKind.Absolute);
                    break;

                default:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/empty.png", UriKind.Absolute);
                    break;
            }

            return uri;
            //}

            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
