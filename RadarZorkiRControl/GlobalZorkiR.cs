﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RadarZorkiRControl
{
    public class GlobalZorkiR : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<ZorkiRModel> CollectionZorkiR { get; set; }

        public GlobalZorkiR()
        {
            CollectionZorkiR = new ObservableCollection<ZorkiRModel> { };

            #region Test
            //CollectionZorkiR = new ObservableCollection<ZorkiRModel>
            //{
            //    new ZorkiRModel
            //    {
            //        Id = 1,
            //        Azimuth = 35.83F,
            //        Range = 125.5F,
            //        TimeReg = DateTime.Now,
            //        TimeControl = 345,
            //        Aspect = 24.7F,
            //        Speed = 250.9F,
            //        RadialSpeed = 35.77F,
            //        State = "Manual",
            //        TargetType = "Helicopter"
            //    },
            //    new ZorkiRModel
            //    {
            //        Id = 2,
            //        Azimuth = 44.43F,
            //        Range = 560.5F,
            //        TimeReg = DateTime.Now,
            //        TimeControl = 3665,
            //        Aspect = 78.66F,
            //        Speed = 560.9F,
            //        RadialSpeed = 89.45F,
            //        State = "Automatic",
            //        TargetType = "Tank"
            //    },
            //    new ZorkiRModel
            //    {
            //        Id = 3,
            //        Azimuth = 33.43F,
            //        Range = 270.5F,
            //        TimeReg = DateTime.Now,
            //        TimeControl = 75,
            //        Aspect = 22.66F,
            //        Speed = 222.9F,
            //        RadialSpeed = 234.45F,
            //        State = "Undefine",
            //        TargetType = "Car"
            //    }
            //};
            #endregion

        }
    }
}