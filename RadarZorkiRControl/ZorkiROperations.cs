﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RadarZorkiRControl
{
    public partial class UserControlRadarZorkiR : UserControl
    {
        #region RadarRodnik Update
        private List<ZorkiRModel> listZorkiRModel = new List<ZorkiRModel> { };
        public List<ZorkiRModel> ListZorkiRModel
        {
            get { return listZorkiRModel; }
            set
            {
                if (listZorkiRModel != null && listZorkiRModel.Equals(value)) return;
                listZorkiRModel = value;
                UpdateZorkiR();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateZorkiR()
        {
            try
            {
                if (ListZorkiRModel == null)
                    return;

                //((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Clear();

                //for (int i = 0; i < ListZorkiRModel.Count; i++)
                //{
                //    ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Add(ListZorkiRModel[i]);
                //}


                //DeleteEmptyRows();

                if (ListZorkiRModel.Count == 0)
                {
                    ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Clear();

                    AddEmptyRows();

                    return;
                }

                ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Clear();

                for (int i = 0; i < ListZorkiRModel.Count; i++)
                {
                    ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Add(ListZorkiRModel[i]);
                }


                //for (int i = 0; i < ListZorkiRModel.Count; i++)
                //{
                //    int index = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.ToList().FindIndex(x => x.Id == ListZorkiRModel[i].Id);
                //    if (index != -1)
                //    {
                //        ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR[index] = ListZorkiRModel[i];
                //    }
                //    else
                //    {
                //        ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Add(ListZorkiRModel[i]);
                //    }
                //}

                AddEmptyRows();

                int ind = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.ToList().FindIndex(x => x.Id == PropSelectedIdZorkiR.IdZorkiR);
                if (ind != -1)
                {
                    DgvZorkiR.SelectedIndex = ind;
                }
                else
                {
                    DgvZorkiR.SelectedIndex = 0;
                }

            }
            catch { }
        }
        #endregion

        #region RadarRodnik DeleteRec
        private short deleteRecZorkiR;
        public short DeleteRecZorkiR
        {
            get { return deleteRecZorkiR; }
            set
            {
                deleteRecZorkiR = value;
                DeleteRecZorkiRId();
            }
        }
        
        /// <summary>
        /// Удалить одну запись
        /// </summary>
        private void DeleteRecZorkiRId()
        {
            try
            {
                int index = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.ToList().FindIndex(x => x.Id == deleteRecZorkiR);
                ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.RemoveAt(index);

                int ind = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.ToList().FindIndex(x => x.Id == PropSelectedIdZorkiR.IdZorkiR);
                if (ind != -1)
                {
                    DgvZorkiR.SelectedIndex = ind;
                }
                else
                {
                    DgvZorkiR.SelectedIndex = 0;
                }
            }
            catch { }
        }
        #endregion

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvZorkiR.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvZorkiR.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvZorkiR.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.RemoveAt(index);
                    }
                }

                List<ZorkiRModel> list = new List<ZorkiRModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    ZorkiRModel strJS = new ZorkiRModel
                    {
                        Id = -2,
                        TimeReg = new DateTime(),
                        Azimuth = -2F,
                        Range = -2F,
                        Speed = -2F,
                        RadialSpeed = -2F,
                        State = string.Empty,
                        TargetType = string.Empty,
                        Aspect = -2F,
                        TimeControl = -2F
                    };
                    list.Add(strJS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Count(s => s.Id < 0);
                int countAllRows = ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalZorkiR)DgvZorkiR.DataContext).CollectionZorkiR.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((ZorkiRModel)DgvZorkiR.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
