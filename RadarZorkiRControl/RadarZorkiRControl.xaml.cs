﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace RadarZorkiRControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRadarZorkiR : UserControl
    {
        public event EventHandler<ZorkiRModel> OnDeleteRecord = (object sender, ZorkiRModel data) => { };
        public event EventHandler OnClearRecords;
        public event EventHandler<ZorkiRModel> OnCentering = (object sender, ZorkiRModel data) => { };

        public UserControlRadarZorkiR()
        {
            InitializeComponent();

            DgvZorkiR.DataContext = new GlobalZorkiR();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((ZorkiRModel)DgvZorkiR.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    ZorkiRModel radarZorkiRModel = new ZorkiRModel
                    {
                        Id = ((ZorkiRModel)DgvZorkiR.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, radarZorkiRModel);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, null);
            }
            catch { }
        }

        private void DgvRadarZorkiR_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvZorkiR_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if ((ZorkiRModel)DgvZorkiR.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnCentering(this, (ZorkiRModel)DgvZorkiR.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void DgvZorkiR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((ZorkiRModel)DgvZorkiR.SelectedItem == null) return;

            if (((ZorkiRModel)DgvZorkiR.SelectedItem).Id > -1)
            {
                if (((ZorkiRModel)DgvZorkiR.SelectedItem).Id != PropSelectedIdZorkiR.IdZorkiR)
                {
                    PropSelectedIdZorkiR.IdZorkiR = ((ZorkiRModel)DgvZorkiR.SelectedItem).Id;
                }
            }
            else
            {
                PropSelectedIdZorkiR.IdZorkiR = 0;
            }
        }
    }
}
