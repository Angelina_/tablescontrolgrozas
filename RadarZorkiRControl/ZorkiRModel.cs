﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadarZorkiRControl
{
    public class ZorkiRModel
    {
        public short Id { get; set; }
        public DateTime TimeReg { get; set; }
        public float TimeControl { get; set; }
        public float Azimuth { get; set; }
        public float Range { get; set; }
        public float Speed { get; set; }
        public float RadialSpeed { get; set; }
        public float Aspect { get; set; }
        public string State { get; set; } = String.Empty;
        public string TargetType { get; set; } = String.Empty;
    }
}
