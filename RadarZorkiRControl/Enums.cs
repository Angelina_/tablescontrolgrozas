﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadarZorkiRControl
{
    public enum State
    {
        Undefine = -1,
        Automatic,
        Manual
    }

    public enum Type
    {
        Human = 1,
        Car = 2,
        Helicopter = 3,
        Group = 4,
        UAV = 5,
        Tank = 6,
        IFV = 7,
        Unknown = 8
    }

}
