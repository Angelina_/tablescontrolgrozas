﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RadarRodnikControl
{
    public partial class UserControlRadarRodnik : UserControl
    {
        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRadarRodnik.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRadarRodnik.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRadarRodnik.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((RadarRodnikViewModel)DgvRadarRodnik.DataContext).CollectionRadarRodnik.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((RadarRodnikViewModel)DgvRadarRodnik.DataContext).CollectionRadarRodnik.RemoveAt(index);
                    }
                }

                List<RadarRodnikModel> list = new List<RadarRodnikModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    RadarRodnikModel strJS = new RadarRodnikModel
                    {
                        Id = -2,
                        Azimuth = -2F,
                        Range = -2F,
                        Class = string.Empty,
                        Velocity = -2F,
                        Aspect = -2F,
                        //Time = DateTime.Now,
                        Mode = string.Empty,
                        Flag = string.Empty

                    };
                    list.Add(strJS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((RadarRodnikViewModel)DgvRadarRodnik.DataContext).CollectionRadarRodnik.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((RadarRodnikViewModel)DgvRadarRodnik.DataContext).CollectionRadarRodnik.Count(s => s.Id < 0);
                int countAllRows = ((RadarRodnikViewModel)DgvRadarRodnik.DataContext).CollectionRadarRodnik.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((RadarRodnikViewModel)DgvRadarRodnik.DataContext).CollectionRadarRodnik.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((RadarRodnikModel)DgvRadarRodnik.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
