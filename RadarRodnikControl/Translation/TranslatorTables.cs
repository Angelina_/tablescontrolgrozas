﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace RadarRodnikControl
{
    public partial class UserControlRadarRodnik : UserControl
    {
        public void SetTranslation(Language language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case RadarRodnikControl.Language.RU:
                        dict.Source = new Uri("/RadarRodnikControl;component/Translation/TranslatorTables.RU.xaml",
                                      UriKind.Relative);
                        break;
                    case RadarRodnikControl.Language.EN:
                        dict.Source = new Uri("/RadarRodnikControl;component/Translation/TranslatorTables.EN.xaml",
                                           UriKind.Relative);
                        break;
                    case RadarRodnikControl.Language.AZ:
                        dict.Source = new Uri("/RadarRodnikControl;component/Translation/TranslatorTables.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/RadarRodnikControl;component/Translation/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
    }
}
