﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace RadarRodnikControl
{
    public partial class UserControlRadarRodnik : UserControl
    {
        #region RadarRodnik
        private List<RodnikModel> listRodnikModel = new List<RodnikModel> { };
        public List<RodnikModel> ListRodnikModel
        {
            get { return listRodnikModel; }
            set
            {
                //if (listRodnikModel != null && listRodnikModel.Equals(value)) return;
                listRodnikModel = value;
                UpdateRodnik();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateRodnik()
        {
            try
            {
                if (ListRodnikModel == null)
                    return;

                ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.Clear();

                for (int i = 0; i < ListRodnikModel.Count; i++)
                {
                    ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.Add(ListRodnikModel[i]);
                }

                if (ListRodnikModel.All(x => x.IsCheck == true))
                {
                    tbCheckAll.IsChecked = true;
                    IsCheckAll = true;
                }
                else
                {
                    tbCheckAll.IsChecked = false;
                    IsCheckAll = false;
                }

                AddEmptyRows();
                GC.Collect(1, GCCollectionMode.Optimized);

                int ind = ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.ToList().FindIndex(x => x.Id == PropSelectedIdRodnik.IdRodnik);
                if (ind != -1)
                {
                    DgvRodnik.SelectedIndex = ind;
                }
                else
                {
                    DgvRodnik.SelectedIndex = 0;
                }
            }
            catch { }
        }
        #endregion

        private bool isOptima = false;
        public bool IsOptima
        {
            get { return isOptima; }
            set
            {
                if (isOptima == value)
                    return;

                isOptima = value;
                UpdateTableForOptima(isOptima);
            }
        }

        private bool isCheckAll = false;
        public bool IsCheckAll
        {
            get { return isCheckAll; }
            set
            {
                if (isCheckAll == value)
                    return;

                isCheckAll = value;
            }
        }

        private void UpdateTableForOptima(bool IsVisible)
        { 
            if (IsVisible)
            {
                this.ColumnIsChecked.Visibility = Visibility.Visible;
                this.ColumnMode.Visibility = Visibility.Hidden;
                this.ColumnFlag.Visibility = Visibility.Hidden;
                this.tbCheckAll.Visibility = Visibility.Visible;
                this.lSeparator.Visibility = Visibility.Visible;
            }
            else
            {
                this.ColumnIsChecked.Visibility = Visibility.Hidden;
                this.ColumnMode.Visibility = Visibility.Visible;
                this.ColumnFlag.Visibility = Visibility.Visible;
                this.tbCheckAll.Visibility = Visibility.Hidden;
                this.lSeparator.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRodnik.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRodnik.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRodnik.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.RemoveAt(index);
                    }
                }

                List<RodnikModel> list = new List<RodnikModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    RodnikModel strJS = new RodnikModel
                    {
                        Id = -2,
                        Azimuth = -2F,
                        Range = -2F,
                        Class = string.Empty,
                        Velocity = -2F,
                        Aspect = -2F,
                        //Time = DateTime.Now,
                        Mode = string.Empty,
                        Flag = string.Empty,
                        Height = -2F,
                        Coordinates = new Coords() { Latitude = 501, Longitude = 501 }
                    };
                    list.Add(strJS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRodnik)DgvRodnik.DataContext).CollectionRodnik.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((RodnikModel)DgvRodnik.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }

    public class PropSelectedIdRodnik
    {
        private static short idRodnik;
        public static short IdRodnik
        {
            get { return idRodnik; }
            set
            {
                if (idRodnik == value)
                    return;

                idRodnik = value;
            }
        }
    }
}
