﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace RadarRodnikControl
{
    [ValueConversion(sourceType: typeof(Coords), targetType: typeof(string))]
    public class CoordsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dLat = 501;
            double dLon = 501;

            dLat = System.Convert.ToDouble(((Coords)value).Latitude);
            dLon = System.Convert.ToDouble(((Coords)value).Longitude);

            //try
            //{

            if (dLat == 500 || dLon == 500) { return "-"; }
            if (dLat == 501 || dLon == 501) { return string.Empty; }

            //}
            //catch { }

            string[] sCoords = FormatCoords(dLat, dLon);
            string[] sSign = Sign(dLat, dLon);


            return sSign[0] + " " + sCoords[0] + "  " + sSign[1] + " " + sCoords[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private string[] FormatCoords(double dLat, double dLon)
        {
            dLat = dLat < 0 ? dLat * -1 : dLat;
            dLon = dLon < 0 ? dLon * -1 : dLon;

            string sLat = string.Empty;
            string sLon = string.Empty;

            switch (UserControlRadarRodnik.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    sLat = ConvertToD(dLat);
                    sLon = ConvertToD(dLon);

                    break;

                case 2: // format "DD MM.mmmm"

                    sLat = ConvertToDM(dLat);
                    sLon = ConvertToDM(dLon);
                    break;

                case 3: // format "DD MM SS.ss"

                    sLat = ConvertToDMS(dLat);
                    sLon = ConvertToDMS(dLon);
                    break;

                default:
                    break;
            }

            string[] sCoords = new string[2];
            sCoords[0] = sLat;
            sCoords[1] = sLon;

            return sCoords;
        }

        string ConvertToD(double dd)
        {
            try
            {
                return dd.ToString("0.000000") + "\u00B0";
            }
            catch
            {
                return string.Empty;
            }
        }

        string ConvertToDM(double dd)
        {
            try
            {
                return Math.Truncate(dd).ToString("00") + "\u00B0"
                        + ((dd - Math.Truncate(dd)) * 60).ToString("00.0000") + "\u0027";
            }
            catch
            {
                return string.Empty;
            }
        }

        string ConvertToDMS(double dd)
        {
            try
            {
                double d = Math.Truncate(dd);
                double m = Math.Truncate((dd - d) * 60);
                double s = ((dd - d) * 60 - m) * 60;

                return d.ToString("00") + "\u00B0"
                     + m.ToString("00") + "\u0027"
                     + s.ToString("00.00") + "\u0022";
            }
            catch
            {
                return string.Empty;
            }
        }

        private string[] Sign(double dLat, double dLon)
        {
            string[] sSign = new string[2];
            sSign[0] = dLat >= 0 ? "N" : "S";
            sSign[1] = dLon >= 0 ? "E" : "W";

            return sSign;
        }
    }

    [ValueConversion(sourceType: typeof(int), targetType: typeof(string))]
    public class SignLatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sSign = string.Empty;

            var type = value.GetType().Name;

            switch (type)
            {
                case "Double":
                    sSign = (double)value >= 0 ? "N" : "S";
                    break;

                case "Int32":
                    sSign = (int)value >= 0 ? "N" : "S";
                    break;
            }

            return sSign;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    [ValueConversion(sourceType: typeof(int), targetType: typeof(string))]
    public class SignLonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sSign = string.Empty;

            var type = value.GetType().Name;

            switch (type)
            {
                case "Double":
                    sSign = (double)value >= 0 ? "E" : "W";
                    break;

                case "Int32":
                    sSign = (int)value >= 0 ? "E" : "W";
                    break;
            }

            return sSign;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class ValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = System.Convert.ChangeType(value, targetType);
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var result = System.Convert.ChangeType(value, targetType);
                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}
