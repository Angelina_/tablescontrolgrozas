﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace RadarRodnikControl
{
    [ValueConversion(sourceType: typeof(short), targetType: typeof(string))]
    public class ShortConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if (System.Convert.ToInt16(value) == -1)
            {
                return "-";
            }
            if (System.Convert.ToInt16(value) == -2)
            {
                return string.Empty;
            }
            //}
            //catch { }

            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
