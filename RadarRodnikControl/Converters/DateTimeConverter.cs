﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace RadarRodnikControl
{
    [ValueConversion(sourceType: typeof(DateTime), targetType: typeof(string))]
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] strTime = System.Convert.ToString(value).Split(' ');

            //try
            //{
            if ((strTime[0] == "01.01.0001" || strTime[0] == "01/01/0001" || strTime[0] == "01-01-0001" ||
                strTime[0] == "01.01.01" || strTime[0] == "01/01/01" || strTime[0] == "01-01-01") ||
               (strTime[0] == "00.00.0000" || strTime[0] == "00/00/0000" || strTime[0] == "00-00-0000" ||
                strTime[0] == "00.00.00" || strTime[0] == "00/00/00" || strTime[0] == "00-00-00") ||

               (strTime[0] == "0.0.0000" || strTime[0] == "0/0/0000" || strTime[0] == "0-0-0000" ||
                strTime[0] == "0.0.00" || strTime[0] == "0/0/00" || strTime[0] == "0-0-00") ||
               (strTime[0] == "1.1.0001" || strTime[0] == "1/1/0001" || strTime[0] == "1-1-0001" ||
                strTime[0] == "1.1.01" || strTime[0] == "1/1/01" || strTime[0] == "1-1-01"))
            {
                    return string.Empty;
            }
            //}
            //catch { }

            return strTime[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
