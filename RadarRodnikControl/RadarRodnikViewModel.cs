﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RadarRodnikControl
{
    public class RadarRodnikViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<RadarRodnikModel> CollectionRadarRodnik { get; set; }

        public RadarRodnikViewModel()
        {
            //RadarRodnikModel = new ObservableCollection<RadarRodnikModel> { };

            CollectionRadarRodnik = new ObservableCollection<RadarRodnikModel>
            {
                new RadarRodnikModel
                {
                    Id = 1,
                    Azimuth = 35.83F,
                    Range = 125.4F,
                    Class = "AAA",
                    Velocity = 500.3F,
                    Aspect = 24.7F,
                    Time = DateTime.Now,
                    Mode = "BBB",
                    Flag = "CCC"
                },
                new RadarRodnikModel
                {
                    Id = 2,
                    Azimuth = 44.43F,
                    Range = 58.8F,
                    Class = "MMM",
                    Velocity = 456.3F,
                    Aspect = 33.33F,
                    Time = DateTime.Now,
                    Mode = "VVV",
                    Flag = "XXX"
                },
            };

        }
    }
}
