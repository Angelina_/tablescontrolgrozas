﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadarRodnikControl
{
    public class RadarRodnikModel
    {
        public short Id { get; set; }
        public float Azimuth { get; set; }
        public float Range { get; set; }
        public string Class { get; set; }
        public float Velocity { get; set; }
        public float Aspect { get; set; }
        public DateTime Time { get; set; }
        public string Mode { get; set; }
        public string Flag { get; set; }
    }
}
