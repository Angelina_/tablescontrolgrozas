﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RadarRodnikControl
{
    public partial class UserControlRadarRodnik : UserControl
    {
        #region RadarRodnik
        private List<RadarRodnikModel> radarRodnikModel;
        public List<RadarRodnikModel> RadarRodnikModel
        {
            get => radarRodnikModel;
            set
            {
                if (radarRodnikModel != null && radarRodnikModel.Equals(value)) return;
                radarRodnikModel = value;
                UpdateRadarRodnik();
            }
        }

        private void UpdateRadarRodnik()
        { 

        }
        #endregion
    }
}
