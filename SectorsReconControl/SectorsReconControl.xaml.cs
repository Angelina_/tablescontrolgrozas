﻿using GrozaSModelsDBLib;
using System;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace SectorsReconControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlSectorsRecon : UserControl
    {
        public SectorsReconProperty SectorsReconWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<SectorsReconProperty> OnIsWindowPropertyOpen = (object sender, SectorsReconProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; set; } = NameTable.TableSectorsRecon;
        #endregion

        public UserControlSectorsRecon()
        {
            InitializeComponent();

            DgvSectorsRecon.DataContext = new GlobalSectorsRecon();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (PropNumJammerStation.OwnNumJammerStation <= 0)
                return;
            SectorsReconWindow = new SectorsReconProperty(((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon);
            SectorsReconWindow.SectorsRecon.NumberASP = PropNumJammerStation.OwnNumJammerStation;

            OnIsWindowPropertyOpen(this, SectorsReconWindow);

            if (SectorsReconWindow.ShowDialog() == true)
            {
                // Событие добавления одной записи
                OnAddRecord(this, new TableEvent(SectorsReconWindow.SectorsRecon));
            }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((TableSectorsRecon)DgvSectorsRecon.SelectedItem != null)
            {
                if (((TableSectorsRecon)DgvSectorsRecon.SelectedItem).Id > 0)
                {
                    var selected = (TableSectorsRecon)DgvSectorsRecon.SelectedItem;

                    SectorsReconWindow = new SectorsReconProperty(((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon, selected.Clone());

                    OnIsWindowPropertyOpen(this, SectorsReconWindow);

                    if (SectorsReconWindow.ShowDialog() == true)
                    {
                        // Событие изменения одной записи
                        OnChangeRecord(this, new TableEvent(SectorsReconWindow.SectorsRecon));
                    }
                }
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableSectorsRecon)DgvSectorsRecon.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    //TableSectorsRecon tableSectorsRecon = new TableSectorsRecon
                    //{
                    //    Id = ((TableSectorsRecon)DgvSectorsRecon.SelectedItem).Id
                    //};

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent((TableSectorsRecon)DgvSectorsRecon.SelectedItem));
                    //OnDeleteRecord(this, new TableEvent(tableSectorsRecon));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableSectorsRecon);
            }
            catch { }
        }

        private void DgvSectorsRecon_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableSectorsRecon)DgvSectorsRecon.SelectedItem).Id > 0)
                {
                    if (((TableSectorsRecon)DgvSectorsRecon.SelectedItem).IsActive)
                    {
                        ((TableSectorsRecon)DgvSectorsRecon.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((TableSectorsRecon)DgvSectorsRecon.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent((TableSectorsRecon)DgvSectorsRecon.SelectedItem));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }
    }
}

