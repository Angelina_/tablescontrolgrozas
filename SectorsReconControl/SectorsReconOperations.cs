﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SectorsReconControl
{
    public partial class UserControlSectorsRecon : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listSectorsRecon"></param>
        public void UpdateSectorsRecon(List<TableSectorsRecon> listSectorsRecon)
        {
            try
            {
                if (listSectorsRecon == null)
                    return;

                ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Clear();

                for (int i = 0; i < listSectorsRecon.Count; i++)
                {
                    ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Add(listSectorsRecon[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSectorsRecon"></param>
        public void AddSectorsRecon(List<TableSectorsRecon> listSectorsRecon)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listSectorsRecon.Count; i++)
                {
                    int ind = ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.ToList().FindIndex(x => x.Id == listSectorsRecon[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon[ind] = listSectorsRecon[i];
                    }
                    else
                    {
                        ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Add(listSectorsRecon[i]);
                    }
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteSectorsRecon(TableSectorsRecon tableSectorsRecon)
        {
            try
            {
                int index = ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.ToList().FindIndex(x => x.Id == tableSectorsRecon.Id);
                ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearSectorsRecon()
        {
            try
            {
                ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvSectorsRecon.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvSectorsRecon.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvSectorsRecon.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.RemoveAt(index);
                    }
                }

                List<TableSectorsRecon> list = new List<TableSectorsRecon>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableSectorsRecon strSR = new TableSectorsRecon
                    {
                        Id = -2,
                        IsActive = false,
                        AngleMin = -2,
                        AngleMax = -2,
                        Note = string.Empty
                    };
                    list.Add(strSR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Count(s => s.Id < 0);
                int countAllRows = ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalSectorsRecon)DgvSectorsRecon.DataContext).CollectionSectorsRecon.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableSectorsRecon)DgvSectorsRecon.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

    }
}
