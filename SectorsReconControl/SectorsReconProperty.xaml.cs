﻿using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ValuesCorrectLib;

namespace SectorsReconControl
{
    /// <summary>
    /// Логика взаимодействия для SectorsReconProperty.xaml
    /// </summary>
    public partial class SectorsReconProperty : Window
    {
        private ObservableCollection<TableSectorsRecon> collectionTemp;
        public TableSectorsRecon SectorsRecon { get; private set; }

        public SectorsReconProperty(ObservableCollection<TableSectorsRecon> collectionSectorsRecon)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSectorsRecon;
                SectorsRecon = new TableSectorsRecon();
                SectorsRecon.AngleMin = 0;
                SectorsRecon.AngleMax = 360;
                propertyGrid.SelectedObject = SectorsRecon;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                //ChangeCategories();
            }
            catch { }
        }

        public SectorsReconProperty(ObservableCollection<TableSectorsRecon> collectionSectorsRecon, TableSectorsRecon tableSectorsRecon)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSectorsRecon;
                SectorsRecon = tableSectorsRecon;
                propertyGrid.SelectedObject = SectorsRecon;

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public SectorsReconProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }


        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableSectorsRecon)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableSectorsRecon IsAddClick(TableSectorsRecon SectorsReconWindow)
        {
            CorrectParams.IsCorrectMinMax(SectorsReconWindow);
            if (CorrectParams.IsCorrectAngleMinMax(SectorsReconWindow.AngleMin, SectorsReconWindow.AngleMax))
            {
                SectorsReconWindow.IsActive = true;
                return SectorsReconWindow;
            }

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new SectorsReconQminEditor(nameof(SectorsRecon.AngleMin), typeof(TableSectorsRecon)));
            propertyGrid.Editors.Add(new SectorsReconQmaxEditor(nameof(SectorsRecon.AngleMax), typeof(TableSectorsRecon)));
            propertyGrid.Editors.Add(new SectorsReconNoteEditor(nameof(SectorsRecon.Note), typeof(TableSectorsRecon)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableSectorsRecon)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                            UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.SRB.xaml",
                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.FR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.FR.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllGrozaSProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllGrozaSProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
