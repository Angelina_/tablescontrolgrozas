﻿using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace SectorsReconControl
{
    public class GlobalSectorsRecon
    {
        public ObservableCollection<TableSectorsRecon> CollectionSectorsRecon { get; set; }

        public GlobalSectorsRecon()
        {
            try
            {
                CollectionSectorsRecon = new ObservableCollection<TableSectorsRecon> { };

                //CollectionSectorsRecon = new ObservableCollection<TableSectorsRecon>
                //{
                //    new TableSectorsRecon
                //    {
                //        Id = 1,
                //        IsActive = true,
                //        AngleMin = 10,
                //        AngleMax = 50,
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableSectorsRecon
                //    {
                //        Id = 2,
                //        IsActive = false,
                //        AngleMin = 230,
                //        AngleMax = 340,
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
