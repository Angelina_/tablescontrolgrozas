﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace CuirasseMPointsControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlCuirasseMPoints : UserControl
    {
        #region Events
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<TableEvent> OnCentering = (object sender, TableEvent data) => { };
        public event EventHandler OnUpdateCuirasseMPoints;
        #endregion

        public UserControlCuirasseMPoints()
        {
            InitializeComponent();

            DgvCuirasseMPoints.DataContext = new GlobalCuirasseMPoints();
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableCuirasseMPoints);
            }
            catch { }
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnUpdateCuirasseMPoints?.Invoke(sender, e);
            }
            catch { }
        }

        private void DgvCuirasseMPoints_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvCuirasseMPoints_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if ((TableCuirasseMPoints)DgvCuirasseMPoints.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному пункту 
                    OnCentering(this, new TableEvent((TableCuirasseMPoints)DgvCuirasseMPoints.SelectedItem));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
