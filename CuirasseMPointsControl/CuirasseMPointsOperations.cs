﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CuirasseMPointsControl
{
    public partial class UserControlCuirasseMPoints : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listCuirasseMPoints"></param>
        public void UpdateCuirasseMPoints(List<TableCuirasseMPoints> listCuirasseMPoints)
        {
            try
            {
                if (listCuirasseMPoints == null)
                    return;

                ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.Clear();

                for (int i = 0; i < listCuirasseMPoints.Count; i++)
                {
                    ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.Add(listCuirasseMPoints[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvCuirasseMPoints.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvCuirasseMPoints.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvCuirasseMPoints.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.RemoveAt(index);
                    }
                }

                List<TableCuirasseMPoints> list = new List<TableCuirasseMPoints>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableCuirasseMPoints strCMP = new TableCuirasseMPoints
                    {
                        Id = -2,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        Note = string.Empty
                    };
                    list.Add(strCMP);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.Count(s => s.Id < 0);
                int countAllRows = ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalCuirasseMPoints)DgvCuirasseMPoints.DataContext).CollectionCuirasseMPoints.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableCuirasseMPoints)DgvCuirasseMPoints.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
