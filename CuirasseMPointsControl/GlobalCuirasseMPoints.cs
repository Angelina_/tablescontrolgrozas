﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CuirasseMPointsControl
{
    public class GlobalCuirasseMPoints
    {
        public ObservableCollection<TableCuirasseMPoints> CollectionCuirasseMPoints { get; set; }

        public GlobalCuirasseMPoints()
        {
            try
            {
                CollectionCuirasseMPoints = new ObservableCollection<TableCuirasseMPoints> { };

                //CollectionCuirasseMPoints = new ObservableCollection<TableCuirasseMPoints>
                //{
                //    new TableCuirasseMPoints()
                //    {
                //        Id = 1,
                //        Coordinates = new Coord
                //        {
                //            Latitude = 53.7899777,
                //            Longitude = 27.77565,
                //            Altitude = 150
                //        },
                //        Note = "FHFFJGHJGV"
                //    },
                //    new TableCuirasseMPoints()
                //    {
                //        Id = 2,
                //        Coordinates = new Coord
                //        {
                //            Latitude = 54.643233,
                //            Longitude = 30.746425,
                //            Altitude = 245
                //        },
                //        Note = "HGHHK GHJHJ"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

    }
}
