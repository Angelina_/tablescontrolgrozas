﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrozaSModelsDBLib;
using TableEvents;

namespace EscopeControl
{
    public partial class EscopeControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="records"></param>
        public void UpdateSignalsUAV(List<EscopeUAVModel> records)
        {
            try
            {
                if (records == null)
                    return;

                tableViewModel.CollectionEscope.Clear();

                for (int i = 0; i < records.Count; i++)
                {
                    tableViewModel.CollectionEscope.Add(records[i]);
                }

                AddEmptyRows();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="records"></param>
        public void AddSignalsUAV(List<EscopeUAVModel> records)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < records.Count; i++)
                {
                    int ind = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == records[i].SerialNumber);
                    
                    if (ind != -1)
                    {
                        tableViewModel.CollectionEscope[ind] = records[i];
                    }
                    else
                    {
                        tableViewModel.CollectionEscope.Add(records[i]);
                    }
                }

                AddEmptyRows();

                DatagridgEscope.SelectedIndex = PropNumUAV.SelectedNumEscope;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Добавить запись в контрол
        /// </summary>
        /// <param name="records"></param>
        public void AddSignalUAV(EscopeUAVModel record)
        {
            try
            {
                if (record.SerialNumber == PropNumUAV.SelectedIdForTargeting)
                {
                    record.IsTracking = true;
                }
                else
                {
                    record.IsTracking = false;
                }

                DeleteEmptyRows();

                int ind = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == record.SerialNumber);
                record.ColorToBrush(record.Color);

                if (ind != -1)
                {
                    tableViewModel.CollectionEscope[ind] = record;
                }
                else
                {
                    tableViewModel.CollectionEscope.Add(record);
                }

                AddEmptyRows();

                DatagridgEscope.SelectedIndex = PropNumUAV.SelectedNumEscope;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteAeroscope(EscopeUAVModel tableAeroscope)
        {
            try
            {
                int index = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == tableAeroscope.SerialNumber);
                if (index != -1)
                {
                    tableViewModel.CollectionEscope.RemoveAt(index);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteAeroscope(string serialNumber)
        {
            try
            {
                int index = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == serialNumber);
                if (index != -1)
                {
                    tableViewModel.CollectionEscope.RemoveAt(index);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearAeroscope()
        {
            try
            {
                tableViewModel.CollectionEscope.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавление пустых строк в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                //paramSatellite.Items.Refresh(); //надо ли?
                int сountRowsAll = DatagridgEscope.Items.Count; // количество имеющихся строк в таблице
                double hs = 23; // высота строки
                double ah = DatagridgEscope.ActualHeight; // визуализированная высота dataGrid
                double chh = DatagridgEscope.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++) //мб переставить циклы местами
                {
                    index = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == string.Empty);
                    //index = DgvSignalsUAV.CollectionSignalsUAV.ToList().FindIndex(x => x.PRN < 0);
                    if (index != -1)
                    {
                        tableViewModel.CollectionEscope.RemoveAt(index);
                        //DgvSignalsUAV.FullParams.RemoveAt(index);
                    }
                }
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    tableViewModel.CollectionEscope.Add(new EscopeUAVModel()
                    {
                        //UAVCoordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        //HomePointCoordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        //PilotCoordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        //Elevation = -2, 
                        //Speed = -2
                    });
                    //DgvSignalsUAV.FullParams.Add(new Satellite());
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = tableViewModel.CollectionEscope.Count(s => s.SerialNumber == string.Empty);
                int countAllRows = tableViewModel.CollectionEscope.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    tableViewModel.CollectionEscope.RemoveAt(iCount);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (this.tableViewModel.SelectedItem.SerialNumber == string.Empty)
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return true;
        }
    }
}
