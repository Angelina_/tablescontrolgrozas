﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using EscopeControl.Annotations;
using  GrozaSModelsDBLib;

namespace EscopeControl
{
    using System.Windows.Media;

    public class EscopeUAVModel : INotifyPropertyChanged
    {
        private bool isTracking;
        private bool isOwn = false;
        private string serialNumber = string.Empty;
        private DateTime time;
        private float elevation = -2;
        private int speed = -2;
        private string type = string.Empty;
        private Coord uavCoordinates = new Coord(-2, -2, -2);
        private Coord pilotCoordinates = new Coord(-2, -2, -2);
        private Coord homePointCoordinates = new Coord(-2, -2, -2);
        private float distance = -2;
        private float azimuth = -2;
        private Color color = Colors.Magenta;

        private Brush brush = Brushes.Magenta;

        public bool IsTracking
        {
            get => isTracking;
            set
            {
                isTracking = value;
                OnPropertyChanged(nameof(IsTracking));
            }
        }

        public bool IsOwn
        {
            get => isOwn;
            set
            {
                isOwn = value;
                OnPropertyChanged(nameof(IsOwn));
            }
        }

        public string SerialNumber
        {
            get => serialNumber;
            set
            {
                serialNumber = value;
                OnPropertyChanged(nameof(SerialNumber));
            }
        }

        public DateTime Time
        {
            get => time;
            set
            {
                time = value;
                OnPropertyChanged(nameof(Time));
            }
        }

        public float Elevation
        {
            get => elevation;
            set
            {
                elevation = value;
                OnPropertyChanged(nameof(Elevation));
            }
        }

        public int Speed
        {
            get => speed;
            set
            {
                speed = value;
                OnPropertyChanged(nameof(Speed));
            }
        }

        public string Type
        {
            get => type;
            set
            {
                type = value;
                OnPropertyChanged(nameof(Type));
            }
        }

        public Coord UAVCoordinates
        {
            get => uavCoordinates;
            set
            {
                uavCoordinates = value;
                OnPropertyChanged(nameof(UAVCoordinates));
            }
        }

        public Coord PilotCoordinates
        {
            get => pilotCoordinates;
            set
            {
                pilotCoordinates = value;
                OnPropertyChanged(nameof(PilotCoordinates));
            }
        }

        public Coord HomePointCoordinates
        {
            get => homePointCoordinates;
            set
            {
                homePointCoordinates = value;
                OnPropertyChanged(nameof(HomePointCoordinates));
            }
        }

        public float Distance
        {
            get => this.distance;
            set
            {
                distance = value;
                OnPropertyChanged(nameof(Distance));
            }
        }

        public float Azimuth
        {
            get => this.azimuth;
            set
            {
                azimuth = value;
                OnPropertyChanged(nameof(Azimuth));
            }
        }

        public Color Color
        {
            get => this.color;
            set
            {
                color = value;
                //ColorToBrush(this.color);
                OnPropertyChanged(nameof(Color));
            }
        }

        public Brush Brush
        {
            get => this.brush;
            set
            {
                brush = value;
                OnPropertyChanged(nameof(Brush));
            }
        }

        public EscopeUAVModel Clone()
        {
            return new EscopeUAVModel()
            {
                IsTracking = IsTracking,
                IsOwn = IsOwn,
                Azimuth = Azimuth,
                Brush = Brush,
                Color = Color, 
                Distance = Distance, 
                SerialNumber = SerialNumber,
                UAVCoordinates = UAVCoordinates.Clone(),
                PilotCoordinates = PilotCoordinates.Clone(), 
                HomePointCoordinates = HomePointCoordinates.Clone(), 
                Elevation = Elevation,
                Speed = Speed, 
                Time = Time, 
                Type = Type
            };
        }

        public void ColorToBrush(Color color)
        {
            Brush = new SolidColorBrush(this.color);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
