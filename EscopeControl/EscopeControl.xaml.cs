﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace EscopeControl
{
    using GrozaSModelsDBLib;
    using System.Collections.Generic;

    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class EscopeControl : UserControl
    {
        #region Events
        public event EventHandler<string> OnDeleteRecord;
        public event EventHandler OnClearRecords;
        public event EventHandler<List<EscopeUAVModel>> OnSaveTracks;
        public event EventHandler<EscopeUAVModel> OnCentering;
        public event EventHandler<EscopeUAVModel> OnAddOwn;
        public event EventHandler<EscopeUAVModel> OnSetAsSpoofingPosition;
        public event EventHandler<bool> OnTargetingMode;
        public event EventHandler<string> OnChangedTarget;
        public event EventHandler<EscopeUAVModel> OnSendCoord;
        #endregion

        private GlobalEscope tableViewModel = new GlobalEscope();

        public EscopeControl()
        {
            InitializeComponent();

            this.DataContext = tableViewModel;
        }


        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (tableViewModel.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    //TableAeroscope tableAeroscope = new TableAeroscope
                    //{
                    var serial = tableViewModel.SelectedItem.SerialNumber;
                    //};

                    // Событие удаления одной записи
                    //OnDeleteRecord(this, new TableEvent(tableAeroscope));
                    OnDeleteRecord?.Invoke(this, serial);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords?.Invoke(this, null);
            }
            catch { }
        }

        private void DgvAeroscope_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvAeroscope_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tableViewModel.SelectedItem == null) return;

            if (tableViewModel.SelectedItem.SerialNumber != string.Empty)
            {
                if (tableViewModel.SelectedItem.SerialNumber != PropNumUAV.SelectedSerialNumEscope)
                {
                    int ind = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == tableViewModel.SelectedItem.SerialNumber);
                    if (ind != -1)
                    {
                        PropNumUAV.SelectedNumEscope = ind;
                        PropNumUAV.SelectedSerialNumEscope = tableViewModel.SelectedItem.SerialNumber;
                        PropNumUAV.IsSelectedNumEscope = true;
                    }
                }
            }
            else
            {
                PropNumUAV.SelectedNumEscope = 0;
                PropNumUAV.SelectedSerialNumEscope = string.Empty;
                PropNumUAV.IsSelectedNumEscope = false;
            }
        }

        private void DgvAeroscope_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (tableViewModel.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnCentering?.Invoke(this, tableViewModel.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonSaveTracks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSaveTracks?.Invoke(sender, tableViewModel.CollectionEscope.ToList());
            }
            catch { }
        }

        private void AddAsOwnMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnAddOwn?.Invoke(sender, tableViewModel.SelectedItem);
            }
            catch { }
        }

        private void SetAsSpoofingPosMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSetAsSpoofingPosition?.Invoke(sender, tableViewModel.SelectedItem);
            }
            catch { }
        }

        private void IsTrackedCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = tableViewModel.SelectedItem;
                var all = tableViewModel.CollectionEscope;

                if (selected == null) return;

                if (tableViewModel.SelectedItem.SerialNumber != string.Empty)
                {
                    var zapis = tableViewModel.CollectionEscope.First(x => x.SerialNumber == tableViewModel.SelectedItem.SerialNumber);
                    if (zapis.IsTracking)
                    {
                        zapis.IsTracking = false;
                        PropNumUAV.SelectedIdForTargeting = String.Empty;
                    }
                    else
                    {
                        foreach (var target in all)
                        {
                            target.IsTracking = false;
                        }
                        zapis.IsTracking = true;
                        PropNumUAV.SelectedIdForTargeting = zapis.SerialNumber;
                    }

                    OnChangedTarget?.Invoke(this, PropNumUAV.SelectedIdForTargeting);
                    
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void ButtonTargeting_Click(object sender, RoutedEventArgs e)
        {
            OnTargetingMode?.Invoke(this, bTargeting.IsChecked ?? false);
        }

        private void SendCoordsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnSendCoord?.Invoke(this, tableViewModel.SelectedItem.Clone());
        }

        private void ButtonTargetingOnce_Click(object sender, RoutedEventArgs e)
        {
            if(PropNumUAV.SelectedIdForTargeting != string.Empty)
            {
                var zapis = tableViewModel.CollectionEscope.First(x => x.SerialNumber == PropNumUAV.SelectedIdForTargeting);
                if(zapis != null)
                {
                    OnSendCoord?.Invoke(this, zapis.Clone());
                }

            }
            
            
        }
    }
}
