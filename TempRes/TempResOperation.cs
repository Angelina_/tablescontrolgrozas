﻿using System;
using System.Collections.Generic;
using System.Linq;
using ValuesConverter;

namespace TempResControl
{
    public partial class TempResControl
    {
        //public List<ResModel> TempListResModel { get; set; }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="records"></param>
        public void UpdateSignalsUAV(IList<ResModel> records)
        {
            try
            {
                if (records == null)
                    return;
                var selectedRec = tableViewModel.CollectionTempFWS.FirstOrDefault(t => t.IsSelected == true);
                if (selectedRec != null)
                {
                    var rec = records.FirstOrDefault(t => t.Id == selectedRec.Id);
                    if ( rec != null)
                    {
                        rec.IsSelected = true;
                    }
                }
                tableViewModel.CollectionTempFWS.Clear();
                tableViewModel.CollectionSelected.Clear();
                if (isActiveFirst)
                {
                    var active = records.Where(t => t.Control == Led.Green).OrderBy(x => x.FrequencyMHz).ToList();
                    var old = records.Where(t => t.Control == Led.Empty).OrderBy(x => x.FrequencyMHz).ToList();
                    foreach (var rec in active)
                    {
                        tableViewModel.CollectionTempFWS.Add(rec);
                    }
                    foreach (var rec in old)
                    {
                        tableViewModel.CollectionTempFWS.Add(rec);
                    }
                }
                else
                {
                    for (int i = 0; i < records.Count; i++)
                    {
                        tableViewModel.CollectionTempFWS.Add(records[i]);
                    }
                }

                if (selectedRec == null)
                {
                    tableViewModel.CollectionSelected.Add(new ResModel(-2, 0, -2F, "", -2F, -2F, default, default, default, Led.Empty));
                }
                else
                {
                    tableViewModel.CollectionSelected.Add(
                        tableViewModel.CollectionTempFWS.First(t => t.IsSelected == true));
                }
                AddEmptyRows();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Добавление пустых строк в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                //paramSatellite.Items.Refresh(); //надо ли?
                int сountRowsAll = dgvTempFWS.Items.Count; // количество имеющихся строк в таблице
                double hs = 23; // высота строки
                double ah = dgvTempFWS.ActualHeight; // визуализированная высота dataGrid
                double chh = dgvTempFWS.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++) //мб переставить циклы местами
                {
                    index = tableViewModel.CollectionTempFWS.ToList().FindIndex(x => x.Id <= 0);
                    //index = DgvSignalsUAV.CollectionSignalsUAV.ToList().FindIndex(x => x.PRN < 0);
                    if (index != -1)
                    {
                        tableViewModel.CollectionTempFWS.RemoveAt(index);
                        //DgvSignalsUAV.FullParams.RemoveAt(index);
                    }
                }
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    tableViewModel.CollectionTempFWS.Add(new ResModel()
                    {
                        Id = -2,
                        FrequencyMHz = 0,
                        Control = Led.Empty,
                        BandMHz = -2F,
                        BearingOwn = -2F,
                        BearingAnother = -2F
                        //UAVCoordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        //HomePointCoordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        //PilotCoordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        //Elevation = -2, 
                        //Speed = -2
                    });
                    //DgvSignalsUAV.FullParams.Add(new Satellite());
                }
            }
            catch { }
        }


        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteAeroscope(ResModel tableAeroscope)
        {
            try
            {
                int index = tableViewModel.CollectionTempFWS.ToList().FindIndex(x => x.Id == tableAeroscope.Id);
                if (index != -1)
                {
                    tableViewModel.CollectionTempFWS.RemoveAt(index);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearAeroscope()
        {
            try
            {
                tableViewModel.CollectionTempFWS.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (tableViewModel.CollectionSelected != null)
                {
                    if (this.tableViewModel.CollectionSelected[0].Id > 0)
                        return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return true;
        }

        ///// <summary>
        ///// Обновить записи в контроле
        ///// </summary>
        //public void UpdateResModel(List<ResModel> listResModel)
        //{
        //    try
        //    {
        //        TempListResModel = listResModel;

        //        if (listResModel.Count == 0)
        //        {
        //            ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionTempFWSCurRow.Clear();
        //            ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Clear();

        //            //------------------------------------------------------------------------
        //            // Отображение в StatusBar
        //            ((PropertiesModel)DataContext).CountFreqAll = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count;
        //            ((PropertiesModel)DataContext).CountFreqActive = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count(ctrlG => ctrlG.Control == Led.Green);
        //            //------------------------------------------------------------------------
        //        }
        //        else
        //        {
        //            // -----------------------------------------
        //            DeleteEmptyRows();

        //            for (int i = 0; i < listResModel.Count; i++)
        //            {
        //                listResModel[i].IsSelected = false;
        //            }
        //            int ind = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList().FindIndex(x => x.IsSelected == true);
        //            if (ind != -1)
        //            {
        //                int indId = listResModel.FindIndex(x => x.Id == ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[ind].Id);
        //                if (indId != -1)
        //                {
        //                    listResModel[indId].IsSelected = true;
        //                }
        //            }
        //            //------------------------------
        //            ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.Clear();
        //            ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Clear();

        //            //------------------------------------------------------------------------
        //            // Отображение в StatusBar
        //            ((PropertiesModel)DataContext).CountFreqAll = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count;
        //            ((PropertiesModel)DataContext).CountFreqActive = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count(ctrlG => ctrlG.Control == Led.Green);
        //            //------------------------------------------------------------------------

        //            if (bSortActive.IsChecked.Value)
        //            {
        //                //Сортировка по частоте(сначала с Control == Led.Green, потом с Control == Led.Empty)
        //                var listAll = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList();

        //                var listControlGreen = listAll.Where(ctrlG => ctrlG.Control == Led.Green); // записали в лист значения Control == Led.Green
        //                IEnumerable<ResModelRow> SortFreqGreen = listControlGreen.OrderBy(x => x.FreqKHz); // сортировка по частоте

        //                var listControlEmpty = listAll.Where(ctrlE => ctrlE.Control == Led.Empty); // записали в лист значения Control == Led.Empty
        //                IEnumerable<ResModelRow> SortFreqEmpty = listControlEmpty.OrderBy(x => x.FreqKHz); // сортировка по частоте

        //                List<ResModelRow> list = new List<ResModelRow>();
        //                list.AddRange(SortFreqGreen.ToList());
        //                list.AddRange(SortFreqEmpty.ToList());

        //                for (int i = 0; i < list.ToList().Count; i++)
        //                {
        //                    ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i] = list[i];
        //                }
        //            }
        //            else
        //            {
        //                var listSort = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList();
        //                IEnumerable<ResModelRow> SortFreq = listSort.OrderBy(x => x.FreqKHz); // сортировка по частоте

        //                // Важные сверху ---------------------------------------------------------------------------
        //                if (listFreqImportant.Count > 0)
        //                {
        //                    List<ResModelRow> tempFreqImportant = new List<ResModelRow>();
        //                    List<ResModelRow> tempFreq = new List<ResModelRow>();
        //                    List<ResModelRow> listFirstFreqImp = new List<ResModelRow>();
        //                    bool f = false;

        //                    for (int i = 0; i < SortFreq.ToList().Count; i++)
        //                    {
        //                        for (int j = 0; j < listFreqImportant.Count; j++)
        //                        {
        //                            if (SortFreq.ToList()[i].FreqKHz >= listFreqImportant[j].FreqMinKHz && SortFreq.ToList()[i].FreqKHz <= listFreqImportant[j].FreqMaxKHz)
        //                            {
        //                                f = true;
        //                            }
        //                        }
        //                        if (f)
        //                        {
        //                            tempFreqImportant.Add(SortFreq.ToList()[i]);
        //                            f = false;
        //                        }
        //                        else
        //                        {
        //                            tempFreq.Add(SortFreq.ToList()[i]);
        //                        }
        //                    }

        //                    listFirstFreqImp.AddRange(tempFreqImportant);
        //                    listFirstFreqImp.AddRange(tempFreq);

        //                    for (int i = 0; i < ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count; i++)
        //                    {
        //                        ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i] = listFirstFreqImp[i];
        //                    }
        //                }
        //                else
        //                {
        //                    for (int i = 0; i < ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count; i++)
        //                    {
        //                        ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i] = SortFreq.ToList()[i];
        //                    }
        //                }
        //            }

        //            int indIsSelectedRow = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList().FindIndex(x => (x.IsSelected.HasValue && x.IsSelected.Value));
        //            if (indIsSelectedRow != -1)
        //            {
        //                if (((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.Count == 0)
        //                {
        //                    AddEmptyRows(dgvResModelCurRow);
        //                    ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[indIsSelectedRow];
        //                }
        //                else
        //                {
        //                    ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[indIsSelectedRow];
        //                }
        //            }
        //        }

        //        GC.Collect(1, GCCollectionMode.Optimized);

        //        AddEmptyRows(dgvResModel);
        //        AddEmptyRows(dgvResModelCurRow);
        //    }
        //    catch { }
        //}


        ///// <summary>
        ///// Удалить одну запись из контрола
        ///// </summary>
        //public void DeleteResModel(ResModel ResModel)
        //{
        //    try
        //    {
        //        int index = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList().FindIndex(x => x.Id == ResModel.Id);
        //        ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.RemoveAt(index);

        //        ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.RemoveAt(0);
        //    }
        //    catch { }
        //}

        ///// <summary>
        ///// Удалить все записи из контрола
        ///// </summary>
        //public void ClearResModel()
        //{
        //    try
        //    {
        //        ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Clear();
        //        AddEmptyRows(dgvResModel);

        //        ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.Clear();
        //        AddEmptyRows(dgvResModelCurRow);
        //    }
        //    catch { }
        //}

        ///// <summary>
        ///// Добавить / удалить пустую строку в dgvResModelCurRow или dgvResModel
        ///// </summary>
        //private void AddEmptyRows(DataGrid dataGrid)
        //{
        //    try
        //    {
        //        int сountRowsAll = dataGrid.Items.Count; // количество всех строк в таблице
        //        double hs = 23; // высота строки
        //        double ah = dataGrid.ActualHeight; // визуализированная высота dataGrid
        //        double chh = dataGrid.ColumnHeaderHeight; // высота заголовка
        //        int countRows = -1;
        //        int index = -1;
        //        int count = -1;

        //        List<ResModelRow> list;

        //        switch (dataGrid.Name)
        //        {
        //            case "dgvResModel":

        //                countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
        //                count = сountRowsAll - countRows; // сколько пустых строк нужно удалить

        //                for (int i = 0; i < count; i++)
        //                {
        //                    // Удалить пустые строки в dgv
        //                    index = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList().FindIndex(x => x.Id < 0);
        //                    if (index != -1)
        //                    {
        //                        ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.RemoveAt(index);
        //                    }
        //                }

        //                list = new List<ResModelRow>();

        //                for (int i = 0; i < (countRows - сountRowsAll) + 1; i++)
        //                {
        //                    ResModelRow strResModel = new ResModelRow
        //                    {
        //                        Id = -2,
        //                        FreqKHz = -2F,
        //                        Control = Led.Empty,
        //                        Deviation = -2F,
        //                        Coordinates = new Coord
        //                        {
        //                            Latitude = -2,
        //                            Longitude = -2,
        //                            Altitude = -2
        //                        },
        //                        ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>
        //                        {
        //                            new JamDirect
        //                            {
        //                                NumberASP = -2,
        //                                Bearing = -2F,
        //                                Level = -2,
        //                                Std = -2,
        //                                DistanceKM = -2
        //                            }
        //                        },
        //                        Type = 33 // 33 - пустое значение 
        //                    };

        //                    list.Add(strResModel);
        //                }

        //                for (int i = 0; i < list.Count; i++)
        //                {
        //                    ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Add(list[i]);
        //                }
        //                break;

        //            case "dgvResModelCurRow":

        //                countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

        //                list = new List<ResModelRow>();
        //                for (int i = 0; i < countRows; i++)
        //                {
        //                    ResModelRow strResModel = new ResModelRow
        //                    {
        //                        Id = -2,
        //                        FreqKHz = -2F,
        //                        Control = Led.Empty,
        //                        Deviation = -2F,
        //                        Coordinates = new Coord
        //                        {
        //                            Latitude = -2,
        //                            Longitude = -2,
        //                            Altitude = -2
        //                        },
        //                        ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>
        //                        {
        //                            new JamDirect
        //                            {
        //                                NumberASP = -2,
        //                                Bearing = -2F,
        //                                Level = -2,
        //                                Std = -2,
        //                                DistanceKM = -2
        //                            }
        //                        },
        //                        Type = 33 // 33 - пустое значение
        //                    };

        //                    list.Add(strResModel);
        //                }

        //                for (int i = 0; i < list.Count; i++)
        //                {
        //                    ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.Add(list[i]);
        //                }
        //                break;

        //            default:
        //                break;
        //        }
        //    }
        //    catch { }
        //}

        ///// <summary>
        ///// Удалить пустые строки из таблицы
        ///// </summary>
        //private void DeleteEmptyRows()
        //{
        //    try
        //    {
        //        int countEmptyRows = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count(s => s.Id < 0);
        //        int countAllRows = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count;
        //        int iCount = countAllRows - countEmptyRows;
        //        for (int i = iCount; i < countAllRows; i++)
        //        {
        //            ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.RemoveAt(iCount);
        //        }

        //        int countEmptyRowsCur = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.Count(s => s.Id < 0);
        //        int countAllRowsCur = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.Count;
        //        int iCountCur = countAllRowsCur - countEmptyRowsCur;
        //        for (int i = iCountCur; i < countAllRowsCur; i++)
        //        {
        //            ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow.RemoveAt(iCountCur);
        //        }
        //    }
        //    catch { }
        //}

        //private bool IsSelectedRowEmpty()
        //{
        //    try
        //    {
        //        //if (((ResModelRow)dgvResModel.SelectedItem).Id == -2)
        //        if (((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow != null)
        //        {
        //            if (((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Id == -2)
        //                return false;
        //        }
        //    }
        //    catch { }

        //    return true;
        //}

        ///// <summary>
        ///// Записать в ResModel выделенную строку 
        ///// </summary>
        ///// <returns></returns>
        //private ResModel SelectedRowForSend()
        //{
        //    int index = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel.ToList().FindIndex(x => x.FreqKHz == ((ResModelRow)dgvResModel.SelectedItem).FreqKHz);

        //    ObservableCollection<JamDirect> listJamDirect = new ObservableCollection<JamDirect>();
        //    for (int i = 0; i < ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ.Count; i++)
        //    {
        //        JamDirect jamDirect = new JamDirect
        //        {
        //            NumberASP = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ[i].NumberASP,
        //            Bearing = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ[i].Bearing,
        //            Level = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ[i].Level,
        //            Std = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ[i].Std,
        //            DistanceKM = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ[i].DistanceKM,
        //            IsOwn = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[index].ListQ[i].IsOwn
        //        };

        //        listJamDirect.Add(jamDirect);
        //    }

        //    ResModel ResModel = new ResModel
        //    {
        //        Id = ((ResModelRow)dgvResModel.SelectedItem).Id,
        //        FreqKHz = ((ResModelRow)dgvResModel.SelectedItem).FreqKHz,
        //        Deviation = ((ResModelRow)dgvResModel.SelectedItem).Deviation,
        //        Coordinates = new Coord
        //        {
        //            Latitude = ((ResModelRow)dgvResModel.SelectedItem).Coordinates.Latitude,
        //            Longitude = ((ResModelRow)dgvResModel.SelectedItem).Coordinates.Longitude
        //        },
        //        Time = ((ResModelRow)dgvResModel.SelectedItem).Time,
        //        Type = ((ResModelRow)dgvResModel.SelectedItem).Type,
        //        IsSelected = true,
        //        ListQ = listJamDirect
        //    };

        //    return ResModel;
        //}

        ///// <summary>
        ///// Записать в ResModel выделенную строку 
        ///// </summary>
        ///// <returns></returns>
        //private ResModel SelectedCurRowForSend()
        //{
        //    ObservableCollection<JamDirect> listJamDirect = new ObservableCollection<JamDirect>();
        //    for (int i = 0; i < ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ.Count; i++)
        //    {
        //        JamDirect jamDirect = new JamDirect
        //        {
        //            NumberASP = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ[i].NumberASP,
        //            Bearing = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ[i].Bearing,
        //            Level = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ[i].Level,
        //            Std = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ[i].Std,
        //            DistanceKM = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ[i].DistanceKM,
        //            IsOwn = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].ListQ[i].IsOwn
        //        };

        //        listJamDirect.Add(jamDirect);
        //    }

        //    ResModel ResModel = new ResModel
        //    {
        //        Id = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Id,
        //        FreqKHz = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].FreqKHz,
        //        Deviation = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Deviation,
        //        Coordinates = new Coord
        //        {
        //            Latitude = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Coordinates.Latitude,
        //            Longitude = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Coordinates.Longitude,
        //            Altitude = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Coordinates.Altitude
        //        },
        //        Time = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Time,
        //        Type = ((GlobalResModelCurRow)dgvResModelCurRow.DataContext).CollectionResModelCurRow[0].Type,
        //        IsSelected = true,
        //        ListQ = listJamDirect
        //    };

        //    return ResModel;

        //}

        //SolidColorBrush solidBrushRed = new SolidColorBrush(Colors.Red);
        //SolidColorBrush solidBrushKhaki = new SolidColorBrush(Colors.Khaki);
        //SolidColorBrush solidBrushWhite = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD9F9E3"));

        ///// <summary>
        ///// Добавить таблицу для отчета
        ///// </summary>
        ///// <returns></returns>
        //private string[,] AddResModelToTable()
        //{
        //    int Columns = 10;
        //    int Rows = (((GlobalResModel)dgvResModel.DataContext).CollectionResModel.Count(x => x.Id > 0)) + 1;
        //    string[,] Table = new string[Rows, Columns];

        //    try
        //    {
        //        Table[0, 0] = SHeaders.headerFreq;
        //        Table[0, 1] = SHeaders.headerBearing;
        //        Table[0, 2] = SHeaders.headerDeltaF;
        //        Table[0, 3] = SHeaders.headerRMSE;
        //        Table[0, 4] = SHeaders.headerLevel;
        //        Table[0, 5] = SHeaders.headerTime;
        //        Table[0, 6] = SHeaders.headerLatLon;
        //        Table[0, 7] = SHeaders.headerAlt;
        //        Table[0, 8] = SHeaders.headerDistance;
        //        Table[0, 9] = SHeaders.headerAJS;

        //        for (int i = 1; i < Rows; i++)
        //        {
        //            Table[i, 0] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].FreqKHz.ToString();
        //            Table[i, 1] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].SelectedJamDirect.Bearing.ToString();
        //            Table[i, 2] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].Deviation.ToString();
        //            Table[i, 3] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].SelectedJamDirect.Std.ToString();
        //            Table[i, 4] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].SelectedJamDirect.Level.ToString();
        //            Table[i, 5] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].Time.ToString();
        //            Table[i, 6] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].Coordinates.Latitude.ToString() + "   " + ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].Coordinates.Longitude.ToString();
        //            Table[i, 7] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].Coordinates.Altitude.ToString();
        //            Table[i, 8] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].SelectedJamDirect.DistanceKM.ToString();
        //            Table[i, 9] = ((GlobalResModel)dgvResModel.DataContext).CollectionResModel[i - 1].SelectedASP.ToString();
        //        }

        //    }
        //    catch { }

        //    return Table;
        //}
    }


}
