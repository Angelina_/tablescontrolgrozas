﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ValuesConverter;

namespace TempResControl
{
    public class ResModel : INotifyPropertyChanged//, IEquatable<ResModel>
    {
        private int _id;
        private double _frequencyMHz;
        private float _bandMHz;
        private string _type = string.Empty;
        private float _distanceOwn;
        private float _distanceAnother;
        private double _latitude;
        private double _longitude;
        private float _bearingOwn;
        private float _bearingAnother;
        private DateTime _timeCreation;
        private DateTime _timeLastUpdate;

        private bool? _isSelected;
        public Led _control;


        //[Category("JammingParams")]
        public int Id
        {
            get => this._id;
            set => this.SetField(ref this._id, value);
        }

        public double FrequencyMHz
        {
            get => this._frequencyMHz;
            set => this.SetField(ref this._frequencyMHz, value);
        }

        public float BandMHz
        {
            get => this._bandMHz;
            set => this.SetField(ref this._bandMHz, value);
        }

        public string Type
        {
            get => this._type;
            set => this.SetField(ref this._type, value);
        }

        //public float DistanceOwn
        //{
        //    get => this._distanceOwn;
        //    set => this.SetField(ref this._distanceOwn, value);
        //}

        //public float DistanceAnother
        //{
        //    get => this._distanceAnother;
        //    set => this.SetField(ref this._distanceAnother, value);
        //}

        //public double Latitude
        //{
        //    get => this._latitude;
        //    set => this.SetField(ref this._latitude, value);
        //}

        //public double Longitude
        //{
        //    get => this._longitude;
        //    set => this.SetField(ref this._longitude, value);
        //}

        public float BearingOwn
        {
            get => this._bearingOwn;
            set => this.SetField(ref this._bearingOwn, value);
        }

        public float BearingAnother
        {
            get => this._bearingAnother;
            set => this.SetField(ref this._bearingAnother, value);
        }

        public DateTime TimeCreation
        {
            get => this._timeCreation;
            set => this.SetField(ref this._timeCreation, value);
        }

        public DateTime TimeLastUpdate
        {
            get => this._timeLastUpdate;
            set => this.SetField(ref this._timeLastUpdate, value);
        }

        public bool? IsSelected
        {
            get => this._isSelected;
            set => this.SetField(ref this._isSelected, value);
        }

        public Led Control
        {
            get => this._control;
            set => this.SetField(ref this._control, value);
        }

        public ResModel()
        {}

        public ResModel(int id, double frequencyMHz, float bandMHz, string type, float bearingOwn, float bearingAnother, DateTime timeCreation, DateTime timeLastUpdate, bool? isSelected, Led control)
        {
            Id = id;
            FrequencyMHz = frequencyMHz;
            BandMHz = bandMHz;
            Type = type;
            BearingOwn = bearingOwn;
            BearingAnother = bearingAnother;
            TimeCreation = timeCreation;
            TimeLastUpdate = timeLastUpdate;
            IsSelected = isSelected;
            Control = control;

        }

        public ResModel Copy() => new ResModel(Id, FrequencyMHz, BandMHz, Type, BearingOwn, BearingAnother,
            TimeCreation, TimeLastUpdate, IsSelected, Control);


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        #endregion


        //public bool Equals(ResModel other)
        //{
        //    if (ReferenceEquals(null, other)) return false;
        //    if (ReferenceEquals(this, other)) return true;
        //    return _id == other._id && _frequencyMHz.Equals(other._frequencyMHz) && _bandMHz.Equals(other._bandMHz) && _type == other._type && _distanceOwn.Equals(other._distanceOwn) && _distanceAnother.Equals(other._distanceAnother) && _latitude.Equals(other._latitude) && _longitude.Equals(other._longitude) && _bearingOwn.Equals(other._bearingOwn) && _bearingAnother.Equals(other._bearingAnother) && _timeCreation.Equals(other._timeCreation) && _timeLastUpdate.Equals(other._timeLastUpdate) && _isSelected == other._isSelected && _control == other._control;
        //}

        //public override bool Equals(object obj)
        //{
        //    if (ReferenceEquals(null, obj)) return false;
        //    if (ReferenceEquals(this, obj)) return true;
        //    if (obj.GetType() != this.GetType()) return false;
        //    return Equals((ResModel)obj);
        //}

        //public override int GetHashCode()
        //{
        //    unchecked
        //    {
        //        var hashCode = _id;
        //        hashCode = (hashCode * 397) ^ _frequencyMHz.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _bandMHz.GetHashCode();
        //        hashCode = (hashCode * 397) ^ (_type != null ? _type.GetHashCode() : 0);
        //        hashCode = (hashCode * 397) ^ _distanceOwn.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _distanceAnother.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _latitude.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _longitude.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _bearingOwn.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _bearingAnother.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _timeCreation.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _timeLastUpdate.GetHashCode();
        //        hashCode = (hashCode * 397) ^ _isSelected.GetHashCode();
        //        hashCode = (hashCode * 397) ^ (int)_control;
        //        return hashCode;
        //    }
        //}
    }
}
