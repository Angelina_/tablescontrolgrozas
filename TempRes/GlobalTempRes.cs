﻿
using System.Collections.ObjectModel;

namespace TempResControl
{
    public class GlobalTempRes
    {
        public ObservableCollection<ResModel> CollectionTempFWS { get; set; }
        public ObservableCollection<ResModel> CollectionSelected { get; set; }

        public ResModel SelectedItem { get; set; }

        public GlobalTempRes()
        {
            CollectionTempFWS = new ObservableCollection<ResModel> { };
            CollectionSelected = new ObservableCollection<ResModel>();
        }
    }

    //public class TempFWSRow : ResModel, INotifyPropertyChanged
    //{
    //    private JamDirect selectedJamDirect = new JamDirect();
    //    private int selectedASP = -2;

    //    #region INotifyPropertyChanged

    //    public event PropertyChangedEventHandler PropertyChanged;

    //    public void OnPropertyChanged([CallerMemberName] string prop = "")
    //    {
    //        try
    //        {
    //            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
    //        }
    //        catch
    //        {
    //        }
    //    }

    //    #endregion

    //    public int SelectedASP
    //    {
    //        get
    //        {
    //            if (selectedASP < 0 && ListQ != null && ListQ.Count != 0)
    //                selectedASP = ListQ.First().NumberASP;
    //            return selectedASP;
    //        }
    //        set
    //        {
    //            if (selectedASP == value) return;
    //            selectedASP = value;
    //            //OnPropertyChanged(nameof(SelectedASP));
    //        }
    //    }

    //    public JamDirect SelectedJamDirect
    //    {
    //        get
    //        {
    //            if (!CompareJams(selectedJamDirect, ListQ.FirstOrDefault(rec => rec.NumberASP == SelectedASP)))
    //            {
    //                SelectedJamDirect = ListQ.FirstOrDefault(rec => rec.NumberASP == SelectedASP);
    //            }

    //            return selectedJamDirect;
    //        }
    //        set
    //        {
    //            if (value == null) return;
    //            if (selectedJamDirect == value) return;

    //            selectedJamDirect.Bearing = value.Bearing;
    //            selectedJamDirect.DistanceKM = value.DistanceKM;
    //            selectedJamDirect.IsOwn = value.IsOwn;
    //            selectedJamDirect.Level = value.Level;
    //            selectedJamDirect.NumberASP = value.NumberASP;
    //            selectedJamDirect.Std = value.Std;
    //            OnPropertyChanged(nameof(SelectedJamDirect));
    //        }
    //    }

    //    private bool CompareJams(JamDirect jam1, JamDirect jam2)
    //    {
    //        if (jam1 == null || jam2 == null)
    //            return true;
    //        if (jam1.Bearing != jam2.Bearing ||
    //            jam1.DistanceKM != jam2.DistanceKM ||
    //            jam1.IsOwn != jam2.IsOwn ||
    //            jam1.Level != jam2.Level ||
    //            jam1.NumberASP != jam2.NumberASP ||
    //            jam1.Std != jam2.Std)
    //            return false;
    //        return true;
    //    }

    //    public TempFWSRow()
    //    {
    //        if (ListQ != null && ListQ.Count != 0)
    //            selectedJamDirect = ListQ.First();
    //    }

    //    public TempFWSRow(ResModel tempFWS)
    //    {
    //        Id = tempFWS.Id;
    //        Control = tempFWS.Control;
    //        FrequencyMHz = tempFWS.FrequencyMHz;
    //        BandMHz = tempFWS.BandMHz;
    //        BearingOwn = tempFWS.BearingOwn;
    //        BearingAnother = tempFWS.BearingAnother;
    //        Type = tempFWS.Type;
    //        TimeCreation = tempFWS.TimeCreation;
    //        TimeLastUpdate = tempFWS.TimeLastUpdate;
    //        IsSelected = tempFWS.IsSelected;
    //    }

    //    public class GlobalTempFWSCurRow
    //    {
    //        public ObservableCollection<TempFWSRow> CollectionTempFWSCurRow { get; set; }

    //        public GlobalTempFWSCurRow()
    //        {
    //            CollectionTempFWSCurRow = new ObservableCollection<TempFWSRow> { };
    //        }
    //    }
    //}
}
