﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using TableEvents;

namespace TempResControl
{
    /// <summary>
    /// Interaction logic for TempResControl.xaml
    /// </summary>
    public partial class TempResControl : UserControl
    {
        #region Events
        // Удалить запись из таблицы
        public event EventHandler<int> OnDeleteRecord;
        // Удалить все записи в таблице
        public event EventHandler OnClearRecords;
        // Добавить запись в таблицу ИИ ФРЧ РП
        public event EventHandler<ResModel> OnAddForJamming;
        // Добавить запись в таблицу ИИ ФРЧ ЦР
        public event EventHandler<ResModel> OnAddToRes;
        // // Отправить запрос на исполнительное пеленгование
        //public event EventHandler<TempFWS> OnGetExecBear = (object sender, TempFWS data) => { };
        //// Отправить запрос на квазиодновременное пеленгование
        //public event EventHandler<TempFWS> OnGetKvBear = (object sender, TempFWS data) => { };

        public event EventHandler<TableEvent> OnAddRecord;

        // Изменить статус выделенной строки (поле IsSelected = true)
        public event EventHandler<TableEvent> OnSelectedRow;

        public event EventHandler<TableEventReport> OnAddTableToReport;
        #endregion

        private GlobalTempRes tableViewModel = new GlobalTempRes();
        private bool isActiveFirst;

        public TempResControl()
        {
            InitializeComponent();
            this.DataContext = tableViewModel;

            //dgvTempFWS.DataContext = new GlobalTempRes();
            //dgvTempFWSCurRow.DataContext = new GlobalTempFWSCurRow();

            ////DataContext = new PropertiesModel();
        }

        private void grid_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.Key == System.Windows.Input.Key.Space)
                //    ButtonAddFWS_CRRD_Click(this, null);

                //if (e.Key == System.Windows.Input.Key.Q)
                //    ButtonGetKvBear_Click(this, null);

                //if (e.Key == System.Windows.Input.Key.E)
                //    ButtonGetExecBear_Click(this, null);

                if (e.Key == System.Windows.Input.Key.R)
                    ButtonAddToRes_Click(this, null);

                if (e.Key == System.Windows.Input.Key.J)
                    ButtonAddForJamming_Click(this, null);
            }
            catch { }
        }

        private void ButtonAddToRes_Click(object sender, RoutedEventArgs e)
        {
            if (tableViewModel.CollectionSelected == null)
                return;
            
            if (IsSelectedRowEmpty())
                return;

            var selected = tableViewModel.CollectionSelected[0].Copy();
            OnAddToRes?.Invoke(this, selected);
        }

        private void ButtonAddForJamming_Click(object sender, RoutedEventArgs e)
        {
            if (tableViewModel.CollectionSelected == null)
                return;

            if (IsSelectedRowEmpty())
                return;

            var selected = tableViewModel.CollectionSelected[0].Copy();
            OnAddForJamming?.Invoke(this, selected);
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (tableViewModel.CollectionSelected != null)
                {
                    if (IsSelectedRowEmpty())
                        return;
                    
                    var serial = tableViewModel.CollectionSelected[0].Id;

                    OnDeleteRecord?.Invoke(this, serial);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            OnClearRecords?.Invoke(this, null);
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            //string[,] Table = AddTempFWSToTable();

            //OnAddTableToReport(this, new TableEventReport(Table, NameTable.TempFWS));
        }

        private void dgvTempFWSCurRow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //AddEmptyRows(dgvTempFWSCurRow);
        }

        private void dgvTempFWSCurRow_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                // Отменяет выделение всех ячеек в элементе управления
                dgvTempFWSCurRow.UnselectAllCells();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void DgvTempFWS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
            //AddEmptyRows(dgvTempFWS);
        }

        private async void dgvTempFWS_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                if (tableViewModel.SelectedItem == null) return;

                if (tableViewModel.SelectedItem.Id != PropSelectedIdTempFWS.SelectedIdTempFWS)
                {
                    PropSelectedIdTempFWS.SelectedIdTempFWS = tableViewModel.SelectedItem.Id;

                    if (tableViewModel.SelectedItem.Id > 0)
                    {
                        for (int i = 0; i < tableViewModel.CollectionTempFWS.Count; i++)
                        {
                            tableViewModel.CollectionTempFWS[i].IsSelected = false;
                        }
                        int indIsSelectedRow = tableViewModel.CollectionTempFWS.ToList().FindIndex(x => x.Id == tableViewModel.SelectedItem.Id);
                        if (indIsSelectedRow != -1)
                        {
                            tableViewModel.CollectionTempFWS[indIsSelectedRow].IsSelected = true;
                        }
                        
                        tableViewModel.CollectionSelected.Clear();
                        tableViewModel.CollectionSelected.Add(tableViewModel.SelectedItem.Copy());

                        dgvTempFWS.UnselectAllCells();
                    }
                }
            }
            catch
            { }
           
            
            //if (tableViewModel.SelectedItem.Id > 0)
            //{
            //    if (tableViewModel.SelectedItem.Id != PropSelectedIdTempFWS.SelectedIdTempFWS)
            //    {
            //        PropSelectedIdTempFWS.SelectedIdTempFWS = tableViewModel.SelectedItem.Id;
            //    }
            //}
            //else
            //{
            //    PropSelectedIdTempFWS.SelectedIdTempFWS = 0;
            //}
        }


        private void BSortActive_OnClick(object sender, RoutedEventArgs e)
        {
            isActiveFirst = (sender as ToggleButton).IsChecked ?? false;
        }

        /// <summary>
        /// Для синхронного растягивания колонок в двух таблицах
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvTempFWSCurRow_LayoutUpdated(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvTempFWS.Columns.Count && i < dgvTempFWSCurRow.Columns.Count; ++i)
                dgvTempFWS.Columns[i].Width = dgvTempFWSCurRow.Columns[i].ActualWidth;
        }
    }
}
