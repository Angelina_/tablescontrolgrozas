﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace FreqRangesSControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlFreqRangesS : UserControl, ITableEvent
    {
        public FreqRangesSProperty FreqRangesSWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<FreqRangesSProperty> OnIsWindowPropertyOpen = (object sender, FreqRangesSProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public FreqRangesS NameTable { get; set; } = FreqRangesS.FreqRangesRecon;
        #endregion

        public UserControlFreqRangesS()
        {
            InitializeComponent();

            DgvFreqRangesS.DataContext = new GlobalFreqRangesS();
        }

        //private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (PropNumJammerStation.SelectedNumJammerStation != 0 && PropNumJammerStation.IsSelectedNumJammerStation)
        //        {
        //            FreqRangesSWindow = new FreqRangesSProperty(((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS);
        //            FreqRangesSWindow.FreqRangesS.NumberASP = PropNumJammerStation.SelectedNumJammerStation;

        //            OnIsWindowPropertyOpen(this, FreqRangesSWindow);

        //            if (FreqRangesSWindow.ShowDialog() == true)
        //            {
        //                FreqRangesSWindow.FreqRangesS.FreqMinKHz = FreqRangesSWindow.FreqRangesS.FreqMinKHz * 1000;
        //                FreqRangesSWindow.FreqRangesS.FreqMaxKHz = FreqRangesSWindow.FreqRangesS.FreqMaxKHz * 1000;

        //                // Событие добавления одной записи
        //                OnAddRecord(this, new TableEvent(FindTypeFreqRanges(FreqRangesSWindow.FreqRangesS)));
        //            }
        //        }
        //    }
        //    catch { }
        //}
        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PropNumJammerStation.OwnNumJammerStation <= 0)
                {
                    return;
                }

                FreqRangesSWindow = new FreqRangesSProperty(((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS);
                FreqRangesSWindow.FreqRangesS.NumberASP = PropNumJammerStation.OwnNumJammerStation;

                OnIsWindowPropertyOpen(this, FreqRangesSWindow);

                if (FreqRangesSWindow.ShowDialog() == true)
                {
                    FreqRangesSWindow.FreqRangesS.FreqMinKHz = FreqRangesSWindow.FreqRangesS.FreqMinKHz * 1000;
                    FreqRangesSWindow.FreqRangesS.FreqMaxKHz = FreqRangesSWindow.FreqRangesS.FreqMaxKHz * 1000;

                    // Событие добавления одной записи
                    OnAddRecord(this, new TableEvent(FindTypeFreqRanges(FreqRangesSWindow.FreqRangesS)));
                }
                
            }
            catch { }
        }

      

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((FreqRanges)DgvFreqRangesS.SelectedItem != null)
                {
                    if (((FreqRanges)DgvFreqRangesS.SelectedItem).Id > 0)
                    {
                        var selected = (FreqRanges)DgvFreqRangesS.SelectedItem;

                        FreqRangesSWindow = new FreqRangesSProperty(((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS, selected.Clone());

                        OnIsWindowPropertyOpen(this, FreqRangesSWindow);

                        if (FreqRangesSWindow.ShowDialog() == true)
                        {
                            FreqRangesSWindow.FreqRangesS.FreqMinKHz = FreqRangesSWindow.FreqRangesS.FreqMinKHz * 1000;
                            FreqRangesSWindow.FreqRangesS.FreqMaxKHz = FreqRangesSWindow.FreqRangesS.FreqMaxKHz * 1000;

                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvent(FindTypeFreqRanges(FreqRangesSWindow.FreqRangesS)));
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((FreqRanges)DgvFreqRangesS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    //FreqRanges tableFreqRanges = new FreqRanges
                    //{
                    //    Id = ((FreqRanges)DgvFreqRangesS.SelectedItem).Id
                    //};

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(FindTypeFreqRanges((FreqRanges)DgvFreqRangesS.SelectedItem)));
                    //OnDeleteRecord(this, new TableEvent(FindTypeFreqRanges(tableFreqRanges)));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableFreqRanges());
            }
            catch { }
        }

        private void DgvFreqRangesS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((FreqRanges)DgvFreqRangesS.SelectedItem).Id > 0)
                {
                    if (((FreqRanges)DgvFreqRangesS.SelectedItem).IsActive)
                    {
                        ((FreqRanges)DgvFreqRangesS.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((FreqRanges)DgvFreqRangesS.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(FindTypeFreqRanges((FreqRanges)DgvFreqRangesS.SelectedItem)));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }
    }
}
