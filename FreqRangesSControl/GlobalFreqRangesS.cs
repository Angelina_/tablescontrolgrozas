﻿using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace FreqRangesSControl
{
    public class GlobalFreqRangesS
    {
        public ObservableCollection<FreqRanges> CollectionFreqRangesS { get; set; }

        public GlobalFreqRangesS()
        {
            try
            {
                CollectionFreqRangesS = new ObservableCollection<FreqRanges> { };

                //CollectionFreqRanges = new ObservableCollection<FreqRanges>
                //{
                //    new FreqRanges
                //    {
                //        Id = 1,
                //        FreqMinKHz = 56999.98,
                //        FreqMaxKHz = 78900.88,
                //        Note = "Jk8 dflk okl"
                //    },
                //    new FreqRanges
                //    {
                //        Id = 2,
                //        FreqMinKHz = 455999.58,
                //        FreqMaxKHz = 788900.78,
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
