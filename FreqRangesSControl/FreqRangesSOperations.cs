﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FreqRangesSControl
{
    public partial class UserControlFreqRangesS : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listFreqRangesS"></param>
        public void UpdateFreqRangesS(List<FreqRanges> listFreqRangesS)
        {
            try
            {
                if (listFreqRangesS == null)
                    return;

                ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Clear();

                for (int i = 0; i < listFreqRangesS.Count; i++)
                {
                    ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Add(listFreqRangesS[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listFreqRangesS"></param>
        public void AddFreqRangesS(List<FreqRanges> listFreqRangesS)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listFreqRangesS.Count; i++)
                {
                    int ind = ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.ToList().FindIndex(x => x.Id == listFreqRangesS[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS[ind] = listFreqRangesS[i];
                    }
                    else
                    {
                        ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Add(listFreqRangesS[i]);
                    }
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteFreqRangesS(FreqRanges tableFreqRangesS)
        {
            try
            {
                int index = ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.ToList().FindIndex(x => x.Id == tableFreqRangesS.Id);
                ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearFreqRangesS()
        {
            try
            {
                ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvFreqRangesS.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvFreqRangesS.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvFreqRangesS.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.RemoveAt(index);
                    }
                }

                List<FreqRanges> list = new List<FreqRanges>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    FreqRanges strFR = new FreqRanges
                    {
                        Id = -2,
                        FreqMinKHz = 0,
                        FreqMaxKHz = 0,
                        Note = string.Empty
                    };
                    list.Add(strFR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Count(s => s.Id < 0);
                int countAllRows = ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalFreqRangesS)DgvFreqRangesS.DataContext).CollectionFreqRangesS.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((FreqRanges)DgvFreqRangesS.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableFreqRanges()
        {
            switch (NameTable)
            {
                case FreqRangesS.FreqKnown:
                    return GrozaSModelsDBLib.NameTable.TableFreqKnown;
                case FreqRangesS.FreqRangesRecon:
                    return GrozaSModelsDBLib.NameTable.TableFreqRangesRecon;
                case FreqRangesS.FreqForbidden:
                    return GrozaSModelsDBLib.NameTable.TableFreqForbidden;
                default:
                    break;
            }
            return GrozaSModelsDBLib.NameTable.TableFreqKnown;
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeFreqRanges(FreqRanges table)
        {
            switch (NameTable)
            {
                case FreqRangesS.FreqKnown:
                    return table.ToFreqKnown();
                case FreqRangesS.FreqRangesRecon:
                    return table.ToRangesRecon();
                case FreqRangesS.FreqForbidden:
                    return table.ToFreqForbidden();
                default:
                    break;
            }
            return null;
        }
    }
}
