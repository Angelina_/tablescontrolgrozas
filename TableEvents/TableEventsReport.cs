﻿using GrozaSModelsDBLib;
using System;

namespace TableEvents
{
    public class TableEventReport : EventArgs
    {
        public string[,] Table { get; }

        public NameTable NameTable { get; private set; }

        public TableEventReport(string[,] table, NameTable nameTable)
        {
            Table = table;
            NameTable = nameTable;
           
        }
    }
}
