﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public interface ISelectedRowEvents
    {
        event EventHandler<SelectedRowEvents> OnSelectedRow;
        event EventHandler<SelectedRowEvents> OnDoubleClickStation;
    }
}
