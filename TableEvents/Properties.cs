﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class PropShowDialog
    {
        private static bool isShowDialogOpen = false;
        public static bool IsShowDialogOpen
        {
            get { return isShowDialogOpen; }
            set
            {
                if (isShowDialogOpen == value)
                    return;

                isShowDialogOpen = value;
            }
        }
    }

    public class PropNumJammerStation
    {
        //private static int selectedNumJammerStation = 2;// для отладки
        private static int selectedNumJammerStation = 0;
        public static int SelectedNumJammerStation
        {
            get { return selectedNumJammerStation; }
            set
            {
                if (selectedNumJammerStation == value)
                    return;

                selectedNumJammerStation = value;
            }
        }

        //private static bool isSelectedNumJammerStation = true; // для отладки
        private static bool isSelectedNumJammerStation = false;
        public static bool IsSelectedNumJammerStation
        {
            get { return isSelectedNumJammerStation; }
            set
            {
                if (isSelectedNumJammerStation == value)
                    return;

                isSelectedNumJammerStation = value;
            }
        }

        private static int ownNumJammerStation = -1;
        public static int OwnNumJammerStation
        {
            get { return ownNumJammerStation; }
            set
            {
                if (ownNumJammerStation == value)
                    return;

                ownNumJammerStation = value;
            }
        }
    }

    public class PropIsRecJammerStation
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }

    public class PropSelectedIdRodnik
    {
        private static short idRodnik;
        public static short IdRodnik
        {
            get { return idRodnik; }
            set
            {
                if (idRodnik == value)
                    return;

                idRodnik = value;
            }
        }
    }

    public class PropSelectedIdZorkiR
    {
        private static short idZorkiR;
        public static short IdZorkiR
        {
            get { return idZorkiR; }
            set
            {
                if (idZorkiR == value)
                    return;

                idZorkiR = value;
            }
        }
    }

    public class PropSelectedIdRESCuirasseM
    {
        private static int idRESCuirasseM;
        public static int IdRESCuirasseM
        {
            get { return idRESCuirasseM; }
            set
            {
                if (idRESCuirasseM == value)
                    return;

                idRESCuirasseM = value;
            }
        }
    }

    public class PropSelectedIdGlobus
    {
        private static int idGlobus;
        public static int IdGlobus
        {
            get { return idGlobus; }
            set
            {
                if (idGlobus == value)
                    return;

                idGlobus = value;
            }
        }

        private static int selectedIdForTargeting = -1;
        public static int SelectedIdForTargeting
        {
            get { return selectedIdForTargeting; }
            set
            {
                if (selectedIdForTargeting == value)
                    return;

                selectedIdForTargeting = value;
            }
        }
    }

    public class PropSelectedIdOperatorGun
    {
        private static int idOperatorGun;
        public static int IdOperatorGun
        {
            get { return idOperatorGun; }
            set
            {
                if (idOperatorGun == value)
                    return;

                idOperatorGun = value;
            }
        }
    }
    
    public class PropViewCoords
    { 
        private static byte viewCoords = 1;
        public static byte ViewCoords
        {
            get { return viewCoords; }
            set
            {
                if (viewCoords == value)
                    return;

                viewCoords = value;
            }
        }
    }

    public class PropNumUAV
    {
        private static int selectedIdOwnUAV = 0;
        public static int SelectedIdOwnUAV
        {
            get { return selectedIdOwnUAV; }
            set
            {
                if (selectedIdOwnUAV == value)
                    return;

                selectedIdOwnUAV = value;
            }
        }

        private static int selectedNumOwnUAV = 0;
        public static int SelectedNumOwnUAV
        {
            get { return selectedNumOwnUAV; }
            set
            {
                if (selectedNumOwnUAV == value)
                    return;

                selectedNumOwnUAV = value;
            }
        }

        private static int selectedNumAeroscope = 0;
        public static int SelectedNumAeroscope
        {
            get { return selectedNumAeroscope; }
            set
            {
                if (selectedNumAeroscope == value)
                    return;

                selectedNumAeroscope = value;
            }
        }

        private static string selectedSerialNumAeroscope = string.Empty;
        public static string SelectedSerialNumAeroscope
        {
            get { return selectedSerialNumAeroscope; }
            set
            {
                if (selectedSerialNumAeroscope == value)
                    return;

                selectedSerialNumAeroscope = value;
            }
        }

        private static bool isSelectedNumAeroscope = false;
        public static bool IsSelectedNumAeroscope
        {
            get { return isSelectedNumAeroscope; }
            set
            {
                if (isSelectedNumAeroscope == value)
                    return;

                isSelectedNumAeroscope = value;
            }
        }

        private static int selectedNumEscope = 0;
        public static int SelectedNumEscope
        {
            get { return selectedNumEscope; }
            set
            {
                if (selectedNumEscope == value)
                    return;

                selectedNumEscope = value;
            }
        }

        private static string selectedSerialNumEscope = string.Empty;
        public static string SelectedSerialNumEscope
        {
            get { return selectedSerialNumEscope; }
            set
            {
                if (selectedSerialNumEscope == value)
                    return;

                selectedSerialNumEscope = value;
            }
        }

        private static bool isSelectedNumEscope = false;
        public static bool IsSelectedNumEscope
        {
            get { return isSelectedNumEscope; }
            set
            {
                if (isSelectedNumEscope == value)
                    return;

                isSelectedNumEscope = value;
            }
        }

        private static string selectedIdForTargeting = String.Empty;
        public static string SelectedIdForTargeting
        {
            get { return selectedIdForTargeting; }
            set
            {
                if (selectedIdForTargeting == value)
                    return;

                selectedIdForTargeting = value;
            }
        }
    }

    public class PropFrequencies
    {
        private static int selectedIndFrequencies = -1;
        public static int SelectedIndFrequencies
        {
            get { return selectedIndFrequencies; }
            set
            {
                if (selectedIndFrequencies == value)
                    return;

                selectedIndFrequencies = value;
            }
        }
    }

    public class CoordCuirasseM
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public float Altitude { get; set; }
    }

    public class CoordGlobus
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
    }

    public class PropSelectedIdTempFWS
    {
        private static int selectedIdTempFWS = 0;
        public static int SelectedIdTempFWS
        {
            get { return selectedIdTempFWS; }
            set
            {
                if (selectedIdTempFWS == value)
                    return;

                selectedIdTempFWS = value;
            }
        }
    }

}
