﻿namespace TableEvents
{
    public enum LatitudeSign : byte
    {
        N,
        S
    }

    public enum LongitudeSign : byte
    {
        E,
        W
    }
}
