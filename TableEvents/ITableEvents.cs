﻿using GrozaSModelsDBLib;
using System;

namespace TableEvents
{
    public interface ITableEvent
    {
        event EventHandler<TableEvent> OnAddRecord;
        event EventHandler<TableEvent> OnChangeRecord;
        event EventHandler<TableEvent> OnDeleteRecord;
        event EventHandler<NameTable> OnClearRecords;
       
    }
}
