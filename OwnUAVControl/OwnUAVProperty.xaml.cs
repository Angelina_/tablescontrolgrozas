﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ValuesCorrectLib;

namespace OwnUAVControl
{
    /// <summary>
    /// Логика взаимодействия для OwnUAVProperty.xaml
    /// </summary>
    public partial class OwnUAVProperty : Window
    {
        private ObservableCollection<TableOwnUAV> collectionTemp;
        public TableOwnUAV OwnUAV { get; private set; }

        private Random random = new Random();
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public OwnUAVProperty(ObservableCollection<TableOwnUAV> collectionOwnUAV)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionOwnUAV;
                OwnUAV = new TableOwnUAV();

                var rndSerialNumber = chars.Select(c => chars[random.Next(chars.Length)]).Take(14).ToArray();
                OwnUAV.SerialNumber = string.Join("", rndSerialNumber);
                // OwnUAV.Frequencies = new ObservableCollection<TableOwnUAVFreq>();
                // OwnUAV.Frequencies[0].Time = DateTime.Now;
                propertyGrid.SelectedObject = OwnUAV;

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();

                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Frequencies").HeaderCategoryName = SMeaning.meaningFreqUAV;
                //ChangeCategories();
            }
            catch { }
        }

        public OwnUAVProperty(ObservableCollection<TableOwnUAV> collectionOwnUAV, TableOwnUAV tableOwnUAV)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionOwnUAV;
                OwnUAV = tableOwnUAV;
                propertyGrid.SelectedObject = OwnUAV;

                propertyGrid.Properties[nameof(TableOwnUAV.SerialNumber)].IsReadOnly = true;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();

                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Frequencies").HeaderCategoryName = SMeaning.meaningFreqUAV;
            }
            catch { }
        }


        public OwnUAVProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableOwnUAV)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableOwnUAV IsAddClick(TableOwnUAV OwnUAVWindow)
        {
            if (CorrectParams.IsCorrectSerialNumber(OwnUAVWindow.SerialNumber))
            {
                if (OwnUAVWindow.Frequencies.Count == 1 && OwnUAVWindow.Frequencies[0].FrequencyMHz == 0)
                {
                    OwnUAVWindow.Frequencies.Clear();
                    return OwnUAVWindow;
                }

                for (int i = 0; i < OwnUAVWindow.Frequencies.Count; i++)
                {
                    if (!CorrectParams.IsCorrectFreq(OwnUAVWindow.Frequencies[i].FrequencyMHz))
                    {
                        return null;
                    }
                }

                return OwnUAVWindow;
            }

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new OwnUAVSerialNumberEditor(nameof(OwnUAV.SerialNumber), typeof(TableOwnUAV)));
            propertyGrid.Editors.Add(new OwnUAVNameEditor(nameof(OwnUAV.Name), typeof(TableOwnUAV)));
            propertyGrid.Editors.Add(new OwnUAVNoteEditor(nameof(OwnUAV.Note), typeof(TableOwnUAV)));
            propertyGrid.Editors.Add(new OwnUAVFrequenciesEditor(nameof(OwnUAV.Frequencies), typeof(TableOwnUAV)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableOwnUAV)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                            UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.SRB.xaml",
                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.FR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.FR.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllGrozaSProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllGrozaSProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
