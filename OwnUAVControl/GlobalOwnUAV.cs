﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OwnUAVControl
{
    public class GlobalOwnUAV
    {
        public ObservableCollection<TableOwnUAV> CollectionOwnUAV { get; set; }

        public GlobalOwnUAV()
        {
            try
            {
                CollectionOwnUAV = new ObservableCollection<TableOwnUAV> { };

                #region Test
                //CollectionOwnUAV = new ObservableCollection<TableOwnUAV>
                //{
                //    new TableOwnUAV
                //    {
                //        Id = 1,
                //        SerialNumber = "123456789fghjn",
                //        Name = "Mavic Air",
                //        Note = "Jk8 dflk okl",
                //        Frequencies = new ObservableCollection<TableOwnUAVFreq>
                //        {
                //            new TableOwnUAVFreq
                //            {
                //                Id = 1,
                //                IdSR = 1,
                //                SR = true,
                //                TableOwnUAVId = 1,
                //                FrequencyMHz = 1500.33,
                //                BandMHz = 100.5F,
                //                Time = DateTime.Now
                //            },
                //            new TableOwnUAVFreq
                //            {
                //                Id = 2,
                //                IdSR = 2,
                //                SR = true,
                //                TableOwnUAVId = 1,
                //                FrequencyMHz = 2300.7,
                //                BandMHz = 60.67F,
                //                Time = DateTime.Now
                //            },
                //            new TableOwnUAVFreq
                //            {
                //                Id = 3,
                //                IdSR = 3,
                //                SR = false,
                //                TableOwnUAVId = 1,
                //                FrequencyMHz = 600.786,
                //                BandMHz = 80.88F,
                //                Time = DateTime.Now
                //            }
                //        }
                //    },

                //    new TableOwnUAV
                //    {
                //        Id = 2,
                //        SerialNumber = "NHJIP5689236KX",
                //        Name = "Phantom 4",
                //        Note = "HJG uhhkj",
                //        Frequencies = new ObservableCollection<TableOwnUAVFreq>
                //        {
                //            new TableOwnUAVFreq
                //            {
                //                Id = 4,
                //                IdSR = 4,
                //                SR = false,
                //                TableOwnUAVId = 2,
                //                FrequencyMHz = 1540.33,
                //                BandMHz = 104.5F,
                //                Time = DateTime.Now
                //            },
                //            new TableOwnUAVFreq
                //            {
                //                Id = 5,
                //                IdSR = 5,
                //                SR = true,
                //                TableOwnUAVId = 2,
                //                FrequencyMHz = 2780.8,
                //                BandMHz = 160.67F,
                //                Time = DateTime.Now
                //            }
                //        }
                //    }
                //};

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
