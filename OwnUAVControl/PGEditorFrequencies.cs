﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace OwnUAVControl
{
    public partial class PGEditorFrequencies
    {
        public void LBFrequencies_SelectionChanged(object obj, SelectionChangedEventArgs e)
        {
            try
            {
                var parent = (obj as ListBox).Parent;
                if (parent is Grid)
                {
                    foreach (var child in (parent as Grid).Children)
                    {
                        if (child is ListBox)
                        {
                            switch ((child as ListBox).Name)
                            {
                                case "LBFrequencies":

                                    PropFrequencies.SelectedIndFrequencies = (child as ListBox).SelectedIndex;

                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch { }
        }
    }
}
