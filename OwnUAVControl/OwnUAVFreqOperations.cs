﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OwnUAVControl
{
    public partial class UserControlOwnUAV : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listOwnUAVFreq"></param>
        private void UpdateOwnUAVFreq(List<TableOwnUAVFreq> listOwnUAVFreq)
        {
            try
            {
                if (listOwnUAVFreq == null)
                    return;

                ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.Clear();

                for (int i = 0; i < listOwnUAVFreq.Count; i++)
                {
                    ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.Add(listOwnUAVFreq[i]);
                }

                AddEmptyRowsFreq();

                DgvOwnUAVFreq.SelectedIndex = 0;

            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRowsFreq()
        {
            try
            {
                int сountRowsAll = DgvOwnUAVFreq.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvOwnUAVFreq.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvOwnUAVFreq.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.RemoveAt(index);
                    }
                }

                List<TableOwnUAVFreq> list = new List<TableOwnUAVFreq>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableOwnUAVFreq strTable = new TableOwnUAVFreq
                    {
                        Id = -2,
                        BandMHz = -2
                      };

                    list.Add(strTable);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.Add(list[i]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRowsFreq()
        {
            try
            {
                int countEmptyRows = ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.Count(s => s.Id < 0);
                int countAllRows = ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalOwnUAVFreq)DgvOwnUAVFreq.DataContext).CollectionOwnUAVFreq.RemoveAt(iCount);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
