﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace OwnUAVControl
{
    public partial class UserControlOwnUAV : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listOwnUAV"></param>
        public void UpdateOwnUAV(List<TableOwnUAV> listOwnUAV)
        {
            try
            {
                if (listOwnUAV == null)
                    return;

                ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.Clear();

                for (int i = 0; i < listOwnUAV.Count; i++)
                {
                    ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.Add(listOwnUAV[i]);
                }

                AddEmptyRows();

                DgvOwnUAV.SelectedIndex = PropNumUAV.SelectedNumOwnUAV;

                if (listOwnUAV.Count == 0)
                    UpdateOwnUAVFreq(new List<TableOwnUAVFreq>());
                else
                    UpdateOwnUAVFreq((listOwnUAV[PropNumUAV.SelectedNumOwnUAV].Frequencies).ToList());

            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvOwnUAV.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvOwnUAV.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvOwnUAV.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.RemoveAt(index);
                    }
                }

                List<TableOwnUAV> list = new List<TableOwnUAV>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableOwnUAV strTable = new TableOwnUAV
                    {
                        Id = -2,
                        SerialNumber = string.Empty,
                        Name = String.Empty,
                        Note = string.Empty
                    };

                    list.Add(strTable);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.Add(list[i]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.Count(s => s.Id < 0);
                int countAllRows = ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.RemoveAt(iCount);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableOwnUAV)DgvOwnUAV.SelectedItem).Id == -2)
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return true;
        }
    }
}
