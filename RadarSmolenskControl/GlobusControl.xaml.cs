﻿using System;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace GlobusControl
{
    using System.Linq;

    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlGlobus : UserControl
    {
        public event EventHandler<GlobusModel> OnDeleteRecord = (object sender, GlobusModel data) => { };
        public event EventHandler OnClearRecords;
        public event EventHandler<bool> OnTargetingMode;

        public event EventHandler OnRequestCurrentMode;
        public event EventHandler OnActivate;
        public event EventHandler OnDeactivate;
        public event EventHandler<GlobusModel> OnDoubleClick = (object sender, GlobusModel data) => { };
        public event EventHandler<GlobusModel> OnSendCoord;

        public UserControlGlobus()
        {
            InitializeComponent();

            DgvGlobus.DataContext = new GlobalGlobus();
            StatusBar.DataContext = new StatusModel();

          
        }

        public GlobusModel TrackedTarget
        {
            get { return (GlobusModel)GetValue(TrackedTargetProperty); }
            set { SetValue(TrackedTargetProperty, value); }
        }

        public static readonly DependencyProperty TrackedTargetProperty =
            DependencyProperty.Register("TrackedTarget", typeof(GlobusModel),
                typeof(UserControlGlobus), new FrameworkPropertyMetadata(new GlobusModel() { Id = -2}));

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((GlobusModel)DgvGlobus.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    GlobusModel radarSmolenskModel = new GlobusModel
                    {
                        Id = ((GlobusModel)DgvGlobus.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, radarSmolenskModel);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, null);
            }
            catch { }
        }

        private void DgvGlobus_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvGlobus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((GlobusModel)DgvGlobus.SelectedItem == null) return;

            if (((GlobusModel)DgvGlobus.SelectedItem).Id > -1)
            {
                if (((GlobusModel)DgvGlobus.SelectedItem).Id != PropSelectedIdGlobus.IdGlobus)
                {
                    PropSelectedIdGlobus.IdGlobus = ((GlobusModel)DgvGlobus.SelectedItem).Id;
                }
            }
            else
            {
                PropSelectedIdGlobus.IdGlobus = 0;
            }
        }

        private void ButtonRequestCurrentMode_Click(object sender, RoutedEventArgs e)
        {
            //((StatusModel)StatusBar.DataContext).PeriodicMessage = 1;
            //((StatusModel)StatusBar.DataContext).WorkingMode = 0;
            try
            {
                // Событие удаления записей
                OnRequestCurrentMode(this, null);
            }
            catch { }

        }

        private void ButtonActivate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnActivate(this, null);
            }
            catch { }
        }

        private void ButtonDeactivate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnDeactivate(this, null);
            }
            catch { }
        }


        private void DgvGlobus_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if ((GlobusModel)DgvGlobus.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnDoubleClick(this, (GlobusModel)DgvGlobus.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }


        private void ButtonTargeting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if ((GlobusModel)DgvGlobus.SelectedItem != null)
                //{
                //    if (!IsSelectedRowEmpty())
                //        return;

                //    GlobusModel radarSmolenskModel = ((GlobusModel)DgvGlobus.SelectedItem).Clone();

                //OnTargeting?.Invoke(this, radarSmolenskModel);
                //}
                OnTargetingMode?.Invoke(this, (bool)(bTargeting.IsChecked ?? false));
            }
            catch { }
        }

        public event EventHandler<int> OnChangedTarget;
        private void IsTrackedCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (GlobusModel)DgvGlobus.SelectedItem;
                var all = ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus;
                if (selected.Id > 0)
                {
                    //var hasTracked = (record as TableJammerStation).Role == StationRole.Own && rec.Role != StationRole.Own;
                    //selected.IsTracking = !selected.IsTracking; //check
                    var zapis = ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.First(t => t.Id == selected.Id);
                    if (zapis.IsTracking)
                    {
                        zapis.IsTracking = false;
                        TrackedTarget = new GlobusModel() { Id = -2 };
                        PropSelectedIdGlobus.SelectedIdForTargeting = -1;
                    }
                    else
                    {
                        foreach (var target in all)
                        {
                            target.IsTracking = false;
                        }
                        zapis.IsTracking = true;
                        PropSelectedIdGlobus.SelectedIdForTargeting = zapis.Id;
                        TrackedTarget = zapis.Clone(); //check

                    }

                    OnChangedTarget?.Invoke(this, TrackedTarget.Id);
                    //if (selected.IsTracking)
                    //{
                    //    selected.IsTracking = false;
                    //    TrackedTarget = new GlobusModel() { Id = -2 };
                    //}
                    //else
                    //{
                    //    foreach (var target in all)
                    //    {
                    //        target.IsTracking = false;
                    //    }
                    //    selected.IsTracking = true;
                    //    TrackedTarget = selected.Clone(); //check

                    //}
                    //if (selected.IsTracking)
                    //{
                    //    ((GlobusModel)DgvGlobus.SelectedItem).IsTracking = false;
                    //}
                    //else
                    //{
                    //    ((GlobusModel)DgvGlobus.SelectedItem).IsTracking = true;
                    //}



                    //// Событие изменения одной записи
                    //OnChangeRecord(this, new TableEvent(FindTypeFreqRanges((FreqRanges)DgvFreqRangesS.SelectedItem)));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void SendCoordsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GlobusModel radarSmolenskModel = ((GlobusModel)DgvGlobus.SelectedItem).Clone();

            OnSendCoord?.Invoke(this, radarSmolenskModel);
        }
    }
}
