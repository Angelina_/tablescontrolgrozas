﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using TableEvents;

namespace GlobusControl
{
    public partial class UserControlGlobus : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        #region Globus
        
        private List<GlobusModel> listGlobusModel = new List<GlobusModel> { };
        public List<GlobusModel> ListGlobusModel
        {
            get { return listGlobusModel; }
            set
            {
                if (listGlobusModel != null && listGlobusModel.Equals(value)) return;
                listGlobusModel = value;
                UpdateGlobus();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateGlobus()
        {
            try
            {
                if (ListGlobusModel == null)
                    return;

                ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.Clear();

                foreach (var target in ListGlobusModel)
                {
                    if (target.Id == PropSelectedIdGlobus.SelectedIdForTargeting)
                    {
                        target.IsTracking = true;
                    }
                    else
                    {
                        target.IsTracking = false;
                    }
                }

                for (int i = 0; i < ListGlobusModel.Count; i++)
                {
                    ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.Add(ListGlobusModel[i]);
                }
                //foreach (var target in ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus)
                //{
                //        target.IsTracking = true;
                //}

                AddEmptyRows();
                //GC.Collect(1, GCCollectionMode.Optimized);

                int ind = ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.ToList().FindIndex(x => x.Id == PropSelectedIdGlobus.IdGlobus);
                if (ind != -1)
                {
                    DgvGlobus.SelectedIndex = ind;
                }
                else
                {
                    DgvGlobus.SelectedIndex = 0;
                }

            }
            catch { }
        }
        #endregion

        private byte periodicMessage = 0;
        public byte PeriodicMessage
        {
            get => periodicMessage;
            set
            {
                if (periodicMessage == value)
                    return;

                periodicMessage = value;
                UpdatePeriodicMessage();
            }
        }

        private void UpdatePeriodicMessage()
        {
            ((StatusModel)StatusBar.DataContext).PeriodicMessage = PeriodicMessage;
        }

        private byte workingMode = 0;
        public byte WorkingMode
        {
            get => workingMode;
            set
            {
                if (workingMode == value)
                    return;

                workingMode = value;
                UpdateWorkingMode();
            }
        }

        private void UpdateWorkingMode()
        {
            ((StatusModel)StatusBar.DataContext).WorkingMode = WorkingMode;
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvGlobus.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvGlobus.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvGlobus.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.RemoveAt(index);
                    }
                }

                List<GlobusModel> list = new List<GlobusModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    GlobusModel strJS = new GlobusModel
                    {
                        Id = -2,
                        Coord = new CoordGlobus() { Latitude = -2, Longitude = -2, Altitude = -2 },
                        Azimuth = -2,
                        Elevation = -2,
                        Velocity = -2,
                    };
                    list.Add(strJS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.Count(s => s.Id < 0);
                int countAllRows = ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalGlobus)DgvGlobus.DataContext).CollectionGlobus.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((GlobusModel)DgvGlobus.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}