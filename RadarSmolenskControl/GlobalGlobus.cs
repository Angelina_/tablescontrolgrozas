﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TableEvents;

namespace GlobusControl
{
    public class GlobalGlobus : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<GlobusModel> CollectionGlobus { get; set; }
        //public GlobusModel TrackedTarget { get; set; } 

        public GlobalGlobus()
        {
            CollectionGlobus = new ObservableCollection<GlobusModel> { };
            //TrackedTarget  = new GlobusModel() { Id = -2 };
            //CollectionGlobus = new ObservableCollection<GlobusModel>
            //{
            //    new GlobusModel
            //    {
            //        Id = 1,
            //        Coord = new CoordGlobus { Latitude = 53.343432, Longitude = 27.67567, Altitude = 245.7 },
            //        Azimuth = 35.83,
            //        Elevation = 45,
            //        Velocity = 500.3
            //    },
            //    new GlobusModel
            //    {
            //        Id = 2,
            //        Coord = new CoordGlobus { Latitude = 56.6466, Longitude = 29.61117, Altitude = 555.5 },
            //        Azimuth = 44.43,
            //        Elevation = 255,
            //        Velocity = 670.9
            //    },
            //    new GlobusModel
            //    {
            //        Id = 3,
            //        Coord = new CoordGlobus { Latitude = 54.4422566, Longitude = 30.976674, Altitude = 365.83 },
            //        Azimuth = 46.44,
            //        Elevation = 15,
            //        Velocity = 2540.2
            //    }
            //};

        }

    }
}
