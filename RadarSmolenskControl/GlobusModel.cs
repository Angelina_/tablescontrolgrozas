﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TableEvents;


namespace GlobusControl
{
    public class GlobusModel 
    {
        public int Id { get; set; }
        public CoordGlobus Coord { get; set; } = new CoordGlobus();
        public double Velocity { get; set; }
        public double Elevation { get; set; }
        public double Azimuth { get; set; }
        public bool IsTracking { get; set; }

        public GlobusModel Clone()
        {
            return new GlobusModel()
                       {
                           Id = this.Id,
                           Coord = new CoordGlobus()
                                       {
                                           Latitude = this.Coord.Latitude,
                                           Longitude = this.Coord.Longitude,
                                           Altitude = this.Coord.Altitude
                                       },
                           Velocity = this.Velocity,
                           Elevation = this.Elevation,
                           Azimuth = this.Azimuth,
                           IsTracking = this.IsTracking
                       };
        }
    }

    public class StatusModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private byte periodicMessage = 0;
        public byte PeriodicMessage
        {
            get => periodicMessage;
            set
            {
                if (periodicMessage == value)
                    return;

                periodicMessage = value;
                OnPropertyChanged();
            }
        }

        private byte workingMode = 0;
        public byte WorkingMode
        {
            get => workingMode;
            set
            {
                if (workingMode == value)
                    return;

                workingMode = value;
                OnPropertyChanged();
            }
        }
    }
}
