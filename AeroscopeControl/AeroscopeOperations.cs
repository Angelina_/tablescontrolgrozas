﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using TableEvents;

namespace AeroscopeControl
{
    public partial class AeroscopeControl : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listAeroscope"></param>
        public void UpdateAeroscope(List<TableAeroscope> listAeroscope)
        {
            try
            {
                if (listAeroscope == null)
                    return;

                ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Clear();

                List<TableAeroscopeAll> list = TableAeroscopeToTableAeroscopeAll(listAeroscope);

                for (int i = 0; i < listAeroscope.Count; i++)
                {
                    ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Add(list[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listAeroscope"></param>
        public void AddAeroscope(List<TableAeroscope> listAeroscope)
        {
            try
            {
                DeleteEmptyRows();

                List<TableAeroscopeAll> list = TableAeroscopeToTableAeroscopeAll(listAeroscope);

                for (int i = 0; i < listAeroscope.Count; i++)
                {
                    int ind = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.ToList().FindIndex(x => x.TableAeroscope.SerialNumber == list[i].TableAeroscope.SerialNumber);
                    if (ind != -1)
                    {
                        ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll[ind].TableAeroscope = list[i].TableAeroscope;
                    }
                    else
                    {
                        ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Add(list[i]);
                    }
                }

                AddEmptyRows();

                DgvAeroscopeAll.SelectedIndex = PropNumUAV.SelectedNumAeroscope;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteAeroscope(TableAeroscope tableAeroscope)
        {
            try
            {
                int index = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.ToList().FindIndex(x => x.TableAeroscope.SerialNumber == tableAeroscope.SerialNumber);
                if (index != -1)
                {
                    ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.RemoveAt(index);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearAeroscope()
        {
            try
            {
                ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvAeroscopeAll.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvAeroscopeAll.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvAeroscopeAll.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.ToList().FindIndex(x => x.TableAeroscope.SerialNumber == string.Empty);
                    if (index != -1)
                    {
                        ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.RemoveAt(index);
                    }
                }

                List<TableAeroscopeAll> list = new List<TableAeroscopeAll>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableAeroscopeAll strTableA = new TableAeroscopeAll
                    { 
                        TableAeroscope = new TableAeroscope
                        {
                            Id = -2,
                            IsActive = false,
                            Type = string.Empty,
                            SerialNumber = string.Empty,
                            UUIDLength = -2,
                            UUID = string.Empty,
                            HomeLatitude = -2,
                            HomeLongitude = -2
                        },
                        TableAeroscopeTrajectory = new TableAeroscopeTrajectory
                        {
                            Id = -2,
                            Num = -2,
                            Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                            Elevation = -2,
                            Roll = -2,
                            Pitch = -2,
                            Yaw = -2,
                            V_up = -2,
                            V_east = -2,
                            V_north = -2,
                        }
                    };

                    list.Add(strTableA);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Count(s => s.TableAeroscope.SerialNumber == string.Empty);
                int countAllRows = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber == string.Empty)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Преобразование TableAeroscope к TableAeroscopeAll
        /// </summary>
        /// <param name="listAeroscope"></param>
        /// <returns> List<TableAeroscopeAll> </returns>
        private List<TableAeroscopeAll> TableAeroscopeToTableAeroscopeAll(List<TableAeroscope> listAeroscope)
        {
            List<TableAeroscopeAll> list = new List<TableAeroscopeAll>();

            try
            {

                for (int i = 0; i < listAeroscope.Count; i++)
                {
                    TableAeroscopeAll table = new TableAeroscopeAll();
                    table.TableAeroscope.Id = listAeroscope[i].Id;
                    table.TableAeroscope.SerialNumber = listAeroscope[i].SerialNumber;
                    table.TableAeroscope.IsActive = listAeroscope[i].IsActive;
                    table.TableAeroscope.Type = listAeroscope[i].Type;
                    table.TableAeroscope.UUIDLength = listAeroscope[i].UUIDLength;
                    table.TableAeroscope.UUID = listAeroscope[i].UUID;
                    table.TableAeroscope.HomeLatitude = listAeroscope[i].HomeLatitude;
                    table.TableAeroscope.HomeLongitude = listAeroscope[i].HomeLongitude;
                   
                    list.Add(table);
                }
            }
            catch (Exception ex)
            {
                 Console.WriteLine(ex.Message.ToString());
            }
            return list;
        }
    }
}
