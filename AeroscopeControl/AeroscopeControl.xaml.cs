﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace AeroscopeControl
{
    /// <summary>
    /// Interaction logic for AeroscopeControl.xaml
    /// </summary>
    public partial class AeroscopeControl : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };

        public event EventHandler OnSaveTracks;
        public event EventHandler<TableAeroscopeAll> OnCentering;
        #endregion

        #region Properties
        public NameTable NameTableАeroscope { get; } = NameTable.TableAeroscope;
        public NameTable NameTableАeroscopeTrajectory { get; } = NameTable.TableAeroscopeTrajectory;
        #endregion

        #region struct STBChecked
        public struct STBChecked
        {
            public int indCollection;
            public bool IsTBChecked;
            public string SerialNumber;
            public string Type;
            public string Time;
        }

        private List<STBChecked> listTBChecked = new List<STBChecked>();
        #endregion

        public AeroscopeControl()
        {
            InitializeComponent();

            DgvAeroscopeAll.DataContext = new GlobalAeroscopeAll();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableAeroscope tableAeroscope = new TableAeroscope
                    {
                        Id = ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.Id,
                        SerialNumber = ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableAeroscope));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableAeroscope);
            }
            catch { }
        }

        private void DgvAeroscope_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvAeroscope_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem == null) return;

            if (((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber != string.Empty)
            {
                if (((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber != PropNumUAV.SelectedSerialNumAeroscope)
                {
                    int ind = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.ToList().FindIndex(x => x.TableAeroscope.SerialNumber == ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber);
                    if (ind != -1)
                    {
                        PropNumUAV.SelectedNumAeroscope = ind;
                        PropNumUAV.SelectedSerialNumAeroscope = ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber;
                        PropNumUAV.IsSelectedNumAeroscope = true;
                    }
                }
            }
            else
            {
                PropNumUAV.SelectedNumAeroscope = 0;
                PropNumUAV.SelectedSerialNumAeroscope = string.Empty;
                PropNumUAV.IsSelectedNumAeroscope = false;
            }
        }

        private void DgvAeroscope_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnCentering?.Invoke(this, (TableAeroscopeAll)DgvAeroscopeAll.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }


        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber != String.Empty)
                {
                    if (((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.IsActive)
                    {
                        ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.IsActive = false;
                    }
                    else
                    {
                        ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        //private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        //{
        //    STBChecked TBChecked = new STBChecked();
        //    TBChecked.IsTBChecked = true;
        //    TBChecked.SerialNumber = ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber;
        //    TBChecked.Type = ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.Type;
        //    TBChecked.Time = DateTime.Now.ToLongTimeString().Replace(':', '-');
        //    int indCollection = ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.ToList().FindIndex(x => x.TableAeroscope.SerialNumber == ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber);
        //    if (indCollection != -1)
        //    {
        //        TBChecked.indCollection = indCollection;
        //    }

        //    listTBChecked.Add(TBChecked);
        //}

        //private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    int indCollection = listTBChecked.FindIndex(x => x.SerialNumber == ((TableAeroscopeAll)DgvAeroscopeAll.SelectedItem).TableAeroscope.SerialNumber);
        //    if (indCollection != -1)
        //    {
        //        listTBChecked.RemoveAt(indCollection);
        //    }
        //}

        private void ButtonSaveTracks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSaveTracks?.Invoke(sender, e);
            }
            catch { }
        }
    }
}
