﻿using GrozaSModelsDBLib;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace AeroscopeControl
{
    public partial class AeroscopeControl : UserControl
    {
        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listATrajectory"></param>
        public void AddATrajectory(List<TableAeroscopeTrajectory> listATrajectory)
        {
            try
            {
                if (listATrajectory.Count > 0)
                {
                    for (int i = 0; i < ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll.Count; i++)
                    {
                        if (((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll[i].TableAeroscope.SerialNumber != string.Empty)
                        {
                            List<TableAeroscopeTrajectory> list = listATrajectory.Where(x => x.SerialNumber == ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll[i].TableAeroscope.SerialNumber).ToList();
                            if (list.Count != 0)
                            {
                                IEnumerable<TableAeroscopeTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки
                                TableAeroscopeTrajectory trackMaxNum = SortNum.ToList().Last();

                                ((GlobalAeroscopeAll)DgvAeroscopeAll.DataContext).CollectionAeroscopeAll[i].TableAeroscopeTrajectory = trackMaxNum;
                            }
                        }


                        //else
                        //{
                        //    //((GlobalATrajectory)DgvATrajectory.DataContext).CollectionATrajectory.Add(listATrajectory[i]);

                        //    // Save trajectory
                        //    int index = listTBChecked.FindIndex(x => x.SerialNumber == listATrajectory[i].SerialNumber);
                        //    if (index != -1)
                        //    {
                        //        if (listTBChecked[index].IsTBChecked)
                        //        {
                        //            SaveTrajectory.SaveTrajectoryAeroscope(listTBChecked[index].Type, listTBChecked[index].SerialNumber, listTBChecked[index].Time, listATrajectory[i]);
                        //        }
                        //    }
                        //}
                    }
                }
            }
            catch { }
        }
    }
}
