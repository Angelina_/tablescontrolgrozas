﻿using ValuesCorrectLib;

namespace TableOperations
{
    public class DefinitionParams
    {
        /// <summary>
        /// Получить литеру
        /// </summary>
        /// <param name="dFreq"></param>
        /// <returns></returns>
        private static byte GetLetter(double dFreq)
        {
            byte bLetter = 0;

            if (dFreq >= RangesLetters.FREQ_START_LETTER_1 & dFreq < RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_2 & dFreq < RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_3 & dFreq < RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_4 & dFreq < RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_5 & dFreq < RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_6 & dFreq < RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_7 & dFreq < RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_8 & dFreq < RangesLetters.FREQ_START_LETTER_9)
                bLetter = 8;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_9 & dFreq <= RangesLetters.FREQ_START_LETTER_10)
                bLetter = 9;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_10 & dFreq <= RangesLetters.FREQ_STOP_LETTER_10)
                bLetter = 10;

            return bLetter;
        }

    }
}
