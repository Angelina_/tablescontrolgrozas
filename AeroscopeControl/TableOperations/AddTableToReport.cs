﻿using System;
using System.IO;
//using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using GrozaSModelsDBLib;    
using System.Windows;

namespace TableOperations
{


    public class AddTableToReport
    {
        /// <summary>
        /// Записать таблицу в Word
        /// </summary>
        /// <param name="nameMissing"></param>
        public static void AddToWord(string nameMissing, string[,] Table, NameTable nameTable)
        {
            try
            {
                //Create an instance for word app
                Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();

                //Set animation status for word application
                winword.ShowAnimation = false;

                //Set status for word application is to be visible or not.
                winword.Visible = false;

                //Create a missing variable for missing value
                object missing = System.Reflection.Missing.Value;

                //Create a new document
                Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

                //Add header into the document
                string header = "Обстановка";
                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {

                    //Get the header range and add the header details.
                    Microsoft.Office.Interop.Word.Range headerRange = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;//.wdAlignParagraphCenter;
                    headerRange.Font.ColorIndex = WdColorIndex.wdBlue;
                    headerRange.Font.Size = 14;
                    headerRange.Text = header + "     " + nameMissing;
                }

                //Add paragraph with Heading 1 style
                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                para1.Range.Font.Size = 14;
                //string curTime = "Время               " + DateTime.Now.ToString(@"HH:mm:ss");
                string curTime = DateTime.Today.ToLongDateString() + "   " + DateTime.Now.ToString(@"HH:mm:ss");
                para1.Range.Text = curTime;
                para1.Range.InsertParagraphAfter();

                //Create table and insert some dummy record
                Microsoft.Office.Interop.Word.Table firstTable = document.Tables.Add(para1.Range, Table.GetLength(0), Table.GetLength(1), ref missing, ref missing);
                firstTable.Borders.Enable = 1;

                foreach (Row row in firstTable.Rows)
                {
                    foreach (Cell cell in row.Cells)
                    {
                        //Header row
                        if (cell.RowIndex == 1)
                        {
                            cell.Range.Text = Table[0, cell.ColumnIndex - 1];
                            cell.Range.Font.Bold = 1;
                            //other format properties goes here
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 10;
                            cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                            //Center alignment for the Header cells
                            cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        }
                        //Data row
                        else
                        {
                            cell.Range.Text = Table[cell.RowIndex - 1, cell.ColumnIndex - 1];
                        }
                    }
                }

                string path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Documents\\");
                string time = DateTime.Now.ToLongTimeString().Replace(':', '-');
                switch (nameTable)
                {
                    //case NameTable.TableASP:

                    //    path += "AJS\\";
                    //    break;

                    //case NameTable.TableSectorsRangesRecon:

                    //    path += "SB RI\\";
                    //    break;

                    //case NameTable.TableSectorsRangesSuppr:

                    //    path += "SB J\\";
                    //    break;

                    //case NameTable.TableFreqForbidden:

                    //    path += "RF\\";
                    //    break;

                    //case NameTable.TableFreqKnown:

                    //    path += "KF\\";
                    //    break;

                    //case NameTable.TableFreqImportant:

                    //    path += "IF\\";
                    //    break;

                    //case NameTable.TempFWS:

                    //    path += "RES FF\\";
                    //    break;

                    //case NameTable.TableReconFWS:

                    //    path += "RES FF TD\\";
                    //    break;

                    //case NameTable.TableSuppressFWS:

                    //    path += "RES FF J\\";
                    //    break;

                    //case NameTable.TableReconFHSS:

                    //    path += "RES FHSS\\";
                    //    break;

                    //case NameTable.TableSuppressFHSS:

                    //    path += "RES FHSS J\\";
                    //    break;

                }

                path += DateTime.Today.ToShortDateString().Replace('.', '-') + "\\";

                if (!Directory.Exists(path))
                {
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                }

                // Сохранить документ
                object filename = @path + time + ".docx";
                document.SaveAs2(ref filename);
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        /// <summary>
        /// Записать таблицу в Word
        /// </summary>
        /// <param name="nameMissing"></param>
        public static void AddToExcel(string nameMissing, string[,] Table, NameTable nameTable)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                //excel.Visible = true;
                Workbook workBook = excel.Workbooks.Add(System.Reflection.Missing.Value);
                Worksheet sheet1 = (Worksheet)workBook.Sheets[1];

                //Записать заголовки
                for (int j = 0; j < Table.GetLength(1); j++)
                {
                    Microsoft.Office.Interop.Excel.Range myRange = (Microsoft.Office.Interop.Excel.Range)sheet1.Cells[1, j + 1];
                    sheet1.Cells[1, j + 1].Font.Bold = true;
                    sheet1.Columns[j + 1].ColumnWidth = 10;
                    myRange.Value2 = Table[0, j];
                }
                // Записать данные
                for (int i = 1; i < Table.GetLength(0); i++)
                {
                    for (int j = 0; j < Table.GetLength(1); j++)
                    {
                        Microsoft.Office.Interop.Excel.Range myRange = (Microsoft.Office.Interop.Excel.Range)sheet1.Cells[i + 1, j + 1];
                        myRange.Value2 = Table[i, j];
                    }
                }

                string path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Documents\\");
                string time = DateTime.Now.ToLongTimeString().Replace(':', '-');
                //switch (nameTable)
                //{
                //    case NameTable.TableASP:

                //        path += "AJS\\";
                //        break;

                //    case NameTable.TableSectorsRangesRecon:

                //        path += "SB RI\\";
                //        break;

                //    case NameTable.TableSectorsRangesSuppr:

                //        path += "SB J\\";
                //        break;

                //    case NameTable.TableFreqForbidden:

                //        path += "RF\\";
                //        break;

                //    case NameTable.TableFreqKnown:

                //        path += "KF\\";
                //        break;

                //    case NameTable.TableFreqImportant:

                //        path += "IF\\";
                //        break;

                //    case NameTable.TempFWS:

                //        path += "RES FF\\";
                //        break;

                //    case NameTable.TableReconFWS:

                //        path += "RES FF TD\\";
                //        break;

                //    case NameTable.TableSuppressFWS:

                //        path += "RES FF J\\";
                //        break;

                //    case NameTable.TableReconFHSS:

                //        path += "RES FHSS\\";
                //        break;

                //    case NameTable.TableSuppressFHSS:

                //        path += "RES FHSS J\\";
                //        break;

                //}

                path += DateTime.Today.ToShortDateString().Replace('.', '-') + "\\";

                if (!Directory.Exists(path))
                {
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                }

                // Сохранить документ
                object filename = @path + time + ".xlsx";

                workBook.SaveAs(filename, Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value,
                        Missing.Value, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlUserResolution, true,
                        Missing.Value, Missing.Value, Missing.Value);

                workBook.Close();
                //excel.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public static void AddToTxt(string nameMissing, string[,] Table, NameTable nameTable)
        {
            try
            {
                string path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Documents\\");
                string time = DateTime.Now.ToLongTimeString().Replace(':', '-');
                //switch (nameTable)
                //{
                //    case NameTable.TableASP:

                //        path += "AJS\\";
                //        break;

                //    case NameTable.TableSectorsRangesRecon:

                //        path += "SB RI\\";
                //        break;

                //    case NameTable.TableSectorsRangesSuppr:

                //        path += "SB J\\";
                //        break;

                //    case NameTable.TableFreqForbidden:

                //        path += "RF\\";
                //        break;

                //    case NameTable.TableFreqKnown:

                //        path += "KF\\";
                //        break;

                //    case NameTable.TableFreqImportant:

                //        path += "IF\\";
                //        break;

                //    case NameTable.TempFWS:

                //        path += "RES FF\\";
                //        break;

                //    case NameTable.TableReconFWS:

                //        path += "RES FF TD\\";
                //        break;

                //    case NameTable.TableSuppressFWS:

                //        path += "RES FF J\\";
                //        break;

                //    case NameTable.TableReconFHSS:

                //        path += "RES FHSS\\";
                //        break;

                //    case NameTable.TableSuppressFHSS:

                //        path += "RES FHSS J\\";
                //        break;

                //}

                path += DateTime.Today.ToShortDateString().Replace('.', '-') + "\\";

                if (!Directory.Exists(path))
                {
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                }

                // Сохранить документ
                object filename = @path + time + ".txt";

                int countRow = Table.GetLength(0);
                using (StreamWriter w = new StreamWriter(filename.ToString(), false))
                {

                    string title = string.Empty;

                    for (int j = 0; j < Table.GetLength(1); j++)
                    {
                        title += (Table[0, j] + "     ").ToString();
                    }

                    w.WriteLine(String.Format("{0, -20} ", title));


                    for (int i = 1; i < Table.GetLength(0); i++)
                    {
                        string lines = string.Empty;
                        for (int j = 0; j < Table.GetLength(1); j++)
                        {
                            lines += (Table[i, j] + "         ").ToString();
                        }
                        w.WriteLine(lines);
                        //w.WriteLine(lines);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
