﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using System.Windows.Forms;

namespace TableOperations
{
    using System.Windows;

    public class SaveTrajectory
    {
        /// <summary>
        /// Сохранить траектории Aeroscope
        /// </summary>
        /// <param name="type"> Тип </param>
        /// <param name="serialNumber"> Серийный номер </param>
        /// <param name="time"> Время начала сохранения </param>
        /// <param name="table"></param>
        public static void SaveTrajectoryAeroscope(string type, string serialNumber, string time, TableAeroscopeTrajectory table)
        {
            try
            {
                string path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Trajectories\\Aeroscope\\");

                if (!Directory.Exists(path))
                {
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                }

                // Создать файл
                object filename = @path + "\\" + type + "_" + serialNumber + "_" + time + ".txt";

                using (StreamWriter w = new StreamWriter(filename.ToString(), true))
                {
                    w.WriteLine(String.Format("{0, -20} ",
                        table.Time.Hour.ToString("00") + ":" + table.Time.Minute.ToString("00") + ":" + table.Time.Second.ToString("00") + "." + table.Time.Millisecond.ToString("000") + " " +
                        //table.Time.TimeOfDay.TotalMilliseconds.ToString("F0") + " " +
                        //table.Time.ToLongTimeString() + " " +
                        table.Coordinates.Latitude.ToString("0.000000") + " " +
                        table.Coordinates.Longitude.ToString("0.000000") + " " +
                        table.Elevation.ToString("0.0")));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Сохранить траектории Aeroscope
        /// </summary>
        /// <param name="listAeroscope"> Источники </param>
        /// <param name="listATrajectory"> Траектории </param>
        public static void SaveTrajectoryAeroscope(List<TableAeroscope> listAeroscope, List<TableAeroscopeTrajectory> listATrajectory)
        {
            try
            {
                string path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Trajectories\\Aeroscope\\");
                string date = DateTime.Now.ToShortDateString();
                path = path + date;
                if (!Directory.Exists(path))
                {
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                }

                for (int i = 0; i < listAeroscope.Count; i++)
                {
                    if (listAeroscope[i].IsActive)
                    {
                        List<TableAeroscopeTrajectory> lATrajectory = listATrajectory.Where(x => x.SerialNumber == listAeroscope[i].SerialNumber).ToList();
                        if (lATrajectory.Count > 0)
                        {
                            IEnumerable<TableAeroscopeTrajectory> SortNum = lATrajectory.OrderBy(x => x.Num); // сортировка по номеру точки
                            // Создать файл
                            object filename = @path + "\\" + listAeroscope[i].Type + "_" + listAeroscope[i].SerialNumber.ToString() + "_" + DateTime.Now.Hour.ToString() + "." + DateTime.Now.Minute.ToString() + ".txt";

                            using (StreamWriter w = new StreamWriter(filename.ToString(), true))
                            {
                                for (int j = 0; j < lATrajectory.Count; j++)
                                {
                                    w.WriteLine(String.Format("{0, -20} ",
                                        SortNum.ToList()[j].Time.Hour.ToString("00") + ":" + SortNum.ToList()[j].Time.Minute.ToString("00") + ":" + SortNum.ToList()[j].Time.Second.ToString("00") + "." + SortNum.ToList()[j].Time.Millisecond.ToString("000") + " " +
                                        SortNum.ToList()[j].Coordinates.Latitude.ToString("0.000000") + " " +
                                        SortNum.ToList()[j].Coordinates.Longitude.ToString("0.000000") + " " +
                                        SortNum.ToList()[j].Elevation.ToString("0.0")));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
