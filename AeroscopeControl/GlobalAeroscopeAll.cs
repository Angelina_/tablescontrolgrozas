﻿using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace AeroscopeControl
{
    public class TableAeroscopeAll : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
        private TableAeroscope tableAeroscope = new TableAeroscope();
        private TableAeroscopeTrajectory tableAeroscopeTrajectory = new TableAeroscopeTrajectory();
        public TableAeroscope TableAeroscope
        {
            get => tableAeroscope;
            set
            {
                if (tableAeroscope == value) return;
                tableAeroscope = value;
                OnPropertyChanged();
            }
        }
        public TableAeroscopeTrajectory TableAeroscopeTrajectory
        {
            get => tableAeroscopeTrajectory;
            set
            {
                if (tableAeroscopeTrajectory == value) return;
                tableAeroscopeTrajectory = value;
                OnPropertyChanged();
            }
        }
    }

    public class GlobalAeroscopeAll 
    {
        public ObservableCollection<TableAeroscopeAll> CollectionAeroscopeAll { get; set; }

        public GlobalAeroscopeAll()
        {
            try
            {
                CollectionAeroscopeAll = new ObservableCollection<TableAeroscopeAll> { };

                #region Test
                //CollectionAeroscopeAll = new ObservableCollection<TableAeroscopeAll>
                //{
                //    new TableAeroscopeAll
                //    {
                //        TableAeroscope = new TableAeroscope
                //        {
                //            Id = 1,
                //            SerialNumber = "3434s",
                //            IsActive = false,
                //            Type = "hkhjkh",
                //            UUIDLength = 33,
                //            UUID = "7dfkl",
                //            HomeLatitude = 45,
                //            HomeLongitude = 26
                //        },
                //        TableAeroscopeTrajectory = new TableAeroscopeTrajectory
                //        {
                //            Id = 2,
                //            SerialNumber = "5678g",
                //            Num = 1,
                //            Coordinates = new Coord() { Altitude = 211, Latitude = 56.14577777884, Longitude = -27.845869 },
                //            Elevation = 35,
                //            Time = DateTime.Now,
                //            Roll = 135,
                //            Pitch = 85,
                //            Yaw = 65,
                //            V_up = 345,
                //            V_east = 48,
                //            V_north = 15
                //        }
                //    },
                //    new TableAeroscopeAll
                //    {
                //        TableAeroscope = new TableAeroscope
                //        {
                //            Id = 2,
                //            SerialNumber = "5678g",
                //            IsActive = true,
                //            Type = "sfDF",
                //            UUIDLength = 44,
                //            UUID = "5li9kl",
                //            HomeLatitude = 55,
                //            HomeLongitude = 28
                //        },
                //        TableAeroscopeTrajectory = new TableAeroscopeTrajectory
                //        {
                //            Id = 3,
                //            SerialNumber = "3434s",
                //            Num = 2,
                //            Coordinates = new Coord() { Altitude = 550, Latitude = 52.14577777884, Longitude = -26.845869 },
                //            Elevation = 50,
                //            Time = DateTime.Now,
                //            Roll = 175,
                //            Pitch = 185,
                //            Yaw = 165,
                //            V_up = 37,
                //            V_east = 38,
                //            V_north = 35
                //        }
                //    }
                //};
                #endregion
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
