﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorGunControl
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows;

    using GrozaSModelsDBLib;

    public class GlobalOperatorGun : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<OperatorGunModel> CollectionOperatorGun { get; set; }

        public GlobalOperatorGun()
        {
            try
            {
                CollectionOperatorGun = new ObservableCollection<OperatorGunModel> { };

                //CollectionOperatorGun = new ObservableCollection<OperatorGunModel>
                //                            {
                //                                new OperatorGunModel()
                //                                    {
                //                                        Id = 1,
                //                                        TypeConnection = TypeConnectGun.Modem3G,
                //                                        Azimuth = 45,
                //                                        Coordinates = new Coord()
                //                                                          {
                //                                                              Latitude = 53.456788,
                //                                                              Longitude = 27.563455,
                //                                                              Altitude = -1
                //                                                          },
                //                                        Note = "fsdfb fhf"
                //                                    },
                //                                new OperatorGunModel()
                //                                    {
                //                                        Id = 2,
                //                                        TypeConnection = TypeConnectGun.ModemUHF,
                //                                        Azimuth = 180,
                //                                        Coordinates = new Coord()
                //                                                          {
                //                                                              Latitude = 57.211434,
                //                                                              Longitude = 30.768646,
                //                                                              Altitude = -1
                //                                                          },
                //                                        Note = "gfh 64 dtgf"
                //                                    }
                //                            };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
