﻿namespace OperatorGunControl
{
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using GrozaSModelsDBLib;

    public class OperatorGunModel : TableOperatorGun
    {
        [DataMember]
        [Browsable(false)]
        public Coord Coordinates { get; set; }

        [DataMember]
        [Browsable(false)]
        public short Azimuth { get; set; }

        public OperatorGunModel Clone()
        {
            return new OperatorGunModel
                       {
                           Id = Id,                
                           Note = Note,
                           TypeConnection = TypeConnection
                       };
        }
      
    }
}
