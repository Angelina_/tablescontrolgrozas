﻿using System.Windows.Controls;

namespace OperatorGunControl
{
    using System;

    using GrozaSModelsDBLib;

    using TableEvents;

    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlOperatorGun : UserControl
    {
        public OperatorGunProperty OperatorGunWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object srnder, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<OperatorGunProperty> OnIsWindowPropertyOpen = (object sender, OperatorGunProperty data) => { };
        #endregion

        public UserControlOperatorGun()
        {
            InitializeComponent();

            this.DgvOperatorGun.DataContext = new GlobalOperatorGun();
        }

        private void ButtonAdd_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                OperatorGunWindow = new OperatorGunProperty(((GlobalOperatorGun)DgvOperatorGun.DataContext).CollectionOperatorGun);

                OnIsWindowPropertyOpen(this, OperatorGunWindow);

                if (OperatorGunWindow.ShowDialog() == true)
                {
                    // Событие добавления одной записи
                    OnAddRecord(this, new TableEvent(OperatorGunModelToTableOperatorGun(this.OperatorGunWindow.OperatorGun)));
                }
            }
            catch { }
        }

        private void ButtonChange_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if ((OperatorGunModel)this.DgvOperatorGun.SelectedItem != null)
                {
                    if (((OperatorGunModel)this.DgvOperatorGun.SelectedItem).Id > 0)
                    {
                        var selected = (OperatorGunModel)this.DgvOperatorGun.SelectedItem;

                        OperatorGunWindow = new OperatorGunProperty(((GlobalOperatorGun)DgvOperatorGun.DataContext).CollectionOperatorGun, selected.Clone());

                        OnIsWindowPropertyOpen(this, OperatorGunWindow);

                        if (OperatorGunWindow.ShowDialog() == true)
                        {
                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvent(OperatorGunModelToTableOperatorGun(this.OperatorGunWindow.OperatorGun)));
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if ((OperatorGunModel)this.DgvOperatorGun.SelectedItem != null)
                {
                    if (((OperatorGunModel)this.DgvOperatorGun.SelectedItem).Id > 0)
                    {
                        // Событие удаления одной записи
                        OnDeleteRecord(this, new TableEvent(OperatorGunModelToTableOperatorGun((OperatorGunModel)this.DgvOperatorGun.SelectedItem)));
                    }
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableOperatorGun);
            }
            catch { }
        }

        private void DgvOperatorGun_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            this.AddEmptyRows();
        }
    }
}
