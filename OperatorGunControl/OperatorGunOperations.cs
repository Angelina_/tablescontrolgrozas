﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OperatorGunControl
{
    using GrozaSModelsDBLib;
    using System.Windows.Controls;

    using TableEvents;

    public partial class UserControlOperatorGun : UserControl
    {
        #region OperatorGun
        private List<OperatorGunModel> ListTempOperatorGunModel = new List<OperatorGunModel>();

        private List<TableOperatorGun> listOperatorGun = new List<TableOperatorGun> { };
        public List<TableOperatorGun> ListOperatorGun
        {
            get { return listOperatorGun; }
            set
            {
                listOperatorGun = value;
                this.ListTempOperatorGunModel = TableOperatorGunToOperatorGunModel(listOperatorGun);
                UpdateOperatorGun();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateOperatorGun()
        {
            try
            {
                ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.Clear();

                for (int i = 0; i < this.ListTempOperatorGunModel.Count; i++)
                {
                    ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.Add(this.ListTempOperatorGunModel[i]);
                }

                AddEmptyRows();
                GC.Collect(1, GCCollectionMode.Optimized);

                int ind = ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.ToList().FindIndex(x => x.Id == PropSelectedIdOperatorGun.IdOperatorGun);
                if (ind != -1)
                {
                    this.DgvOperatorGun.SelectedIndex = ind;
                }
                else
                {
                    this.DgvOperatorGun.SelectedIndex = 0;
                }
            }
            catch { }
        }
        #endregion

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = this.DgvOperatorGun.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = this.DgvOperatorGun.ActualHeight; // визуализированная высота dataGrid
                double chh = this.DgvOperatorGun.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.RemoveAt(index);
                    }
                }

                List<OperatorGunModel> list = new List<OperatorGunModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    OperatorGunModel strOpGun = new OperatorGunModel
                                                 {
                                                     Id = -2,
                                                     TypeConnection = (TypeConnectGun)255,
                                                     Azimuth = -2,
                                                     Coordinates = new Coord() { Latitude = -2, Longitude = -2 },
                                                     Note = string.Empty
                                                 };
                    list.Add(strOpGun);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.Add(list[i]);
                }
            }
            catch { }
        }

        private TableOperatorGun OperatorGunModelToTableOperatorGun(OperatorGunModel model)
        {
            TableOperatorGun table = new TableOperatorGun();

            table.Id = model.Id;
            table.TypeConnection = model.TypeConnection;
            table.Note = model.Note;

            return table;
        }

        private List<OperatorGunModel> TableOperatorGunToOperatorGunModel(List<TableOperatorGun> listOperatorGun)
        {
            List<OperatorGunModel> list = new List<OperatorGunModel>();

            for (int i = 0; i < listOperatorGun.Count; i++)
            {
                OperatorGunModel table = new OperatorGunModel();

                table.Id = listOperatorGun[i].Id;
                table.TypeConnection = listOperatorGun[i].TypeConnection;
                table.Note = listOperatorGun[i].Note;
                table.Coordinates = new Coord() { Latitude = -1, Longitude = -1 };
                table.Azimuth = -1;

                list.Add(table);
            }
           
            return list;
        }

        /// <summary>
        /// Обновить координаты, азимут
        /// </summary>
        /// <param name="id"> id оператора </param>
        /// <param name="coord"> широта, долгота </param>
        /// <param name="azimuth"> азимут </param>
        public void UpdateCoordinates(int id, Coord coord, short azimuth)
        {
            int ind = this.ListTempOperatorGunModel.FindIndex(x => x.Id == id);
            if (ind != -1)
            {
                this.ListTempOperatorGunModel[ind].Coordinates = coord;
                this.ListTempOperatorGunModel[ind].Azimuth = azimuth;

                ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.Clear();

                for (int i = 0; i < this.ListTempOperatorGunModel.Count; i++)
                {
                    ((GlobalOperatorGun)this.DgvOperatorGun.DataContext).CollectionOperatorGun.Add(this.ListTempOperatorGunModel[i]);
                }

                this.AddEmptyRows();
                GC.Collect(1, GCCollectionMode.Optimized);
            }
        }
    }
}
