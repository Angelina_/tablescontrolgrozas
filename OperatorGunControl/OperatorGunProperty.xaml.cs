﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OperatorGunControl
{
    using System.Collections.ObjectModel;
    using System.Windows.Controls.WpfPropertyGrid;

    using GrozaSModelsDBLib;

    using ValuesCorrectLib;

    /// <summary>
    /// Логика взаимодействия для OperatorGunProperty.xaml
    /// </summary>
    public partial class OperatorGunProperty : Window
    {
        private ObservableCollection<OperatorGunModel> collectionTemp;
        public OperatorGunModel OperatorGun { get; private set; }

        public OperatorGunProperty(ObservableCollection<OperatorGunModel> collectionOperatorGun)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionOperatorGun;
                OperatorGun = new OperatorGunModel();
                propertyGrid.SelectedObject = OperatorGun;

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
            }
            catch { }
        }

        public OperatorGunProperty(ObservableCollection<OperatorGunModel> collectionOperatorGun, OperatorGunModel tableOperatorGun)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionOperatorGun;
                OperatorGun = tableOperatorGun;
                propertyGrid.SelectedObject = OperatorGun;
                propertyGrid.Properties[nameof(OperatorGunModel.Id)].IsReadOnly = true;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                InitProperty();
            }
            catch { }
        }


        public OperatorGunProperty()
        {
            InitializeComponent();

            InitEditors();
            InitProperty();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((OperatorGunModel)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public OperatorGunModel IsAddClick(OperatorGunModel OperatorGunWindow)
        {
            return OperatorGunWindow;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new TypeConnectGunEditor(nameof(this.OperatorGun.TypeConnection), typeof(OperatorGunModel)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((OperatorGunModel)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                            UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.SRB.xaml",
                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.FR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.FR.xaml",
                            UriKind.Relative);
                        break;

                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllGrozaSProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllGrozaSProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
